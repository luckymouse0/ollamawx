/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogModelInformation.h"
#include "Network.h"

#include <stdexcept>
#include <vector>

#include <wx/log.h>
#include <wx/font.h>

DialogModelInformation::DialogModelInformation(wxWindow* parent, const wxString& host, const wxString& modelName) : DialogModelInformationView(parent)
{
    this->SetTitle(this->GetTitle() + ": " + modelName);

    nlohmann::json requestPayload = {
        {"name", std::string(modelName.mb_str())},
        {"stream", false}
    };

    try
    {
        wxLogDebug("Updating model information...");

        wxLogDebug("Sending HTTP POST - Host: %s Request: /api/show Model: %s.", std::string(host.mb_str()), std::string(modelName.mb_str()));
        Network& network = Network::GetInstance();
        std::string response = network.HTTPPost(std::string(host.mb_str()), "/api/show", requestPayload.dump(), "application/json");
        wxLogDebug("HTTP POST Response: %s", response);

        modelInfo = nlohmann::json::parse(response);

        if (modelInfo.contains("details") && !modelInfo["details"].is_null())
        {
            textCtrlFormat->ChangeValue(modelInfo["details"].contains("format") && !modelInfo["details"]["format"].is_null() ? modelInfo["details"]["format"].get<std::string>() : "");
            textCtrlFamily->ChangeValue(modelInfo["details"].contains("family") && !modelInfo["details"]["family"].is_null() ? modelInfo["details"]["family"].get<std::string>() : "");

            if (modelInfo["details"].contains("families") && !modelInfo["details"]["families"].is_null())
            {
                std::string str;
                for (const auto& family : modelInfo["details"]["families"].get<std::vector<std::string>>())
                {
                    str += family + ", ";
                }

                if (str.size() > 2)
                {
                    str.erase(str.size() - 2);
                }

                textCtrlFamilies->ChangeValue(str);
            }

            textCtrlParameterSize->ChangeValue(modelInfo["details"].contains("parameter_size") && !modelInfo["details"]["parameter_size"].is_null() ? modelInfo["details"]["parameter_size"].get<std::string>() : "");
            textCtrlQuantizationLevel->ChangeValue(modelInfo["details"].contains("quantization_level") && !modelInfo["details"]["quantization_level"].is_null() ? modelInfo["details"]["quantization_level"].get<std::string>() : "");
        }

        wxLogDebug("Model information updated.");
    }
    catch (const std::exception& e)
    {
        wxLogError("HTTP POST Error: %s", e.what());
    }
}

void DialogModelInformation::OnModelfileClick(wxCommandEvent& event)
{
    richTextModelInformation->ChangeValue(modelInfo.contains("modelfile") && !modelInfo["modelfile"].is_null() ? modelInfo["modelfile"].get<std::string>() : "");

    BoldClickedButton(buttonModelfile->GetLabel());
}

void DialogModelInformation::OnLicenseClick(wxCommandEvent& event)
{
    richTextModelInformation->ChangeValue(modelInfo.contains("license") && !modelInfo["license"].is_null() ? modelInfo["license"].get<std::string>() : "");

    BoldClickedButton(buttonLicense->GetLabel());
}

void DialogModelInformation::OnParametersClick(wxCommandEvent& event)
{
    richTextModelInformation->ChangeValue(modelInfo.contains("parameters") && !modelInfo["parameters"].is_null() ? modelInfo["parameters"].get<std::string>() : "");

    BoldClickedButton(buttonParameters->GetLabel());
}

void DialogModelInformation::OnSystemMessageClick(wxCommandEvent& event)
{
    richTextModelInformation->ChangeValue(modelInfo.contains("system") && !modelInfo["system"].is_null() ? modelInfo["system"].get<std::string>() : "");

    BoldClickedButton(buttonSystemMessage->GetLabel());
}

void DialogModelInformation::OnPromptTemplateClick(wxCommandEvent& event)
{
    richTextModelInformation->ChangeValue(modelInfo.contains("template") && !modelInfo["template"].is_null() ? modelInfo["template"].get<std::string>() : "");

    BoldClickedButton(buttonPromptTemplate->GetLabel());
}

void DialogModelInformation::BoldClickedButton(const wxString& label)
{
    for (auto button : { buttonModelfile, buttonLicense, buttonParameters, buttonSystemMessage, buttonPromptTemplate })
    {
        wxFont font = button->GetFont();
        font.SetWeight(button->GetLabel() == label ? wxFONTWEIGHT_BOLD : wxFONTWEIGHT_NORMAL);
        button->SetFont(font);
    }
}
