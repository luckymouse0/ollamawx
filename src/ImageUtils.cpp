/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ImageUtils.h"

namespace ImageUtils
{
    wxIcon LoadIcon(const wxString& imagePath)
    {
        return wxIcon(imagePath, wxBITMAP_TYPE_GIF);
    }

    wxBitmap LoadBitmap(const wxString& imagePath)
    {
        return wxBitmap(imagePath, wxBITMAP_TYPE_PNG);
    }

    wxBitmap LoadBitmap(const wxString& imagePath, int width, int height)
    {
        wxImage image(imagePath, wxBITMAP_TYPE_PNG);
        image.Rescale(width, height);

        return wxBitmap(image);
    }

    wxImage LoadImage(const wxString& imagePath)
    {
        return wxImage(imagePath, wxBITMAP_TYPE_ANY);
    }

    wxImage ResizeImage(wxImage image, int width, int height)
    {
        return image.Rescale(width, height);
    }

    wxBitmap ResizeBitmap(const wxImage& image, int width, int height)
    {
        wxImage imageCopy(image);
        imageCopy.Rescale(width, height);

        return wxBitmap(imageCopy);
    }
};
