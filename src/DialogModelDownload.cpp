/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogModelDownload.h"

#include <string>
#include <stdexcept>

#include <wx/log.h>

DialogModelDownload::DialogModelDownload(wxWindow* parent, const wxString& host, const wxString& modelName) : DialogModelDownloadView(parent), host(host), modelName(modelName), timeoutSeconds(600), downloadInProgress(true)
{
    timerModelDownload.Start(100);

    threadDownload = std::make_unique<std::thread>(&DialogModelDownload::DownloadModel, this);
}

DialogModelDownload::~DialogModelDownload()
{
    downloadInProgress = false;
    if (threadDownload && threadDownload->joinable())
    {
        threadDownload->join();
    }
}

void DialogModelDownload::OnTimerModelDownload(wxTimerEvent& event)
{
    static int progress = 0;
    progress = (progress + 1) % 100;
    gaugeModelDownload->SetValue(progress);

    if (!downloadInProgress)
    {
        timerModelDownload.Stop();
    }
}

void DialogModelDownload::DownloadModel()
{
    nlohmann::json pullRequestPayload = {
        {"name", modelName},
        {"stream", false}
    };

    try
    {
        wxLogDebug("Downloading model...");

        wxLogDebug("Sending HTTP POST - Host: %s Request: /api/pull Model: %s.", std::string(host.mb_str()), std::string(modelName.mb_str()));

        Network& network = Network::GetInstance();
        std::string response = network.HTTPPost(std::string(host.mb_str()), "/api/pull", pullRequestPayload.dump(), "application/json", timeoutSeconds);
        wxLogDebug("HTTP POST Response: %s", response);

        nlohmann::json j = nlohmann::json::parse(response);
        if (j.contains("status") && !j["status"].is_null() && j["status"].get<std::string>() == "success")
        {
            wxLogDebug("Model downloaded successfully.");
            wxQueueEvent(GetEventHandler(), new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, wxID_OK));
        }
        else
        {
            wxLogError("Error downloading the model.");
            wxQueueEvent(GetEventHandler(), new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, wxID_CANCEL));
        }
    }
    catch (const std::exception& e)
    {
        wxLogError("HTTP POST Error: %s", e.what());
        wxQueueEvent(GetEventHandler(), new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, wxID_CANCEL));
    }
}
