/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ServerSettings.h"
#include "FileUtils.h"

#include <fstream>
#include <stdexcept>

ServerSettings::ServerSettings(std::string configJsonPath)
{
    if (!configJsonPath.empty())
    {
        Load(configJsonPath);
    }
    else
    {
        SetDefaultValues();
    }
}

bool ServerSettings::operator==(const ServerSettings& other) const
{
    return (configJson == other.configJson);
}

bool ServerSettings::operator!=(const ServerSettings& other) const
{
    return (configJson != other.configJson);
}

void ServerSettings::Load(std::string newPath)
{
    if (!newPath.empty() && newPath != configJsonPath)
    {
        configJsonPath = newPath;
    }

    if (FileUtils::CheckFileExists(configJsonPath))
    {
        std::ifstream file(configJsonPath);
        if (file.is_open())
        {
            file >> configJson;
            file.close();
        }
        else
        {
            throw std::runtime_error("Unable to open file for loading server settings: " + configJsonPath);
        }
    }
    else
    {
        SetDefaultValues();
    }
}

void ServerSettings::Save(std::string newJsonPath)
{
    if (!newJsonPath.empty() && newJsonPath != configJsonPath)
    {
        configJsonPath = newJsonPath;
    }

    if (!configJsonPath.empty())
    {
        FileUtils::CreateDirectory(FileUtils::GetFilePathDirectory(configJsonPath));

        std::ofstream file(configJsonPath);
        if (file.is_open())
        {
            file << configJson.dump(4);
            file.close();
        }
        else
        {
            throw std::runtime_error("Unable to open file for loading server settings: " + configJsonPath);
        }
    }
}

void ServerSettings::SetDefaultValues()
{
    configJson["Path"] = "ollama";
    configJson["Host"] = "http://localhost:11434";
    configJson["KeepAliveMinutes"] = 20;
    configJson["TimeoutSeconds"] = 120;
    configJson["Streaming"] = false;
}

std::string ServerSettings::GetConfigPath() const
{
    return configJsonPath;
}

std::string ServerSettings::GetPath() const
{
    return configJson["Path"].get<std::string>();
}

void ServerSettings::SetPath(std::string path)
{
    configJson["Path"] = path;
}

std::string ServerSettings::GetHost() const
{
    return configJson["Host"].get<std::string>();
}

void ServerSettings::SetHost(std::string host)
{
    configJson["Host"] = host;
}

int ServerSettings::GetKeepAliveMinutes() const
{
    return configJson["KeepAliveMinutes"].get<int>();
}

std::string ServerSettings::GetKeepAliveMinutesString() const
{
    int minutes = GetKeepAliveMinutes();

    return minutes > 0 ? std::to_string(minutes) + "m" : std::to_string(minutes);
}

void ServerSettings::SetKeepAliveMinutes(int keepAliveMinutes)
{
    configJson["KeepAliveMinutes"] = keepAliveMinutes;
}

int ServerSettings::GetTimeoutSeconds() const
{
    return configJson["TimeoutSeconds"].get<int>();
}

void ServerSettings::SetTimeoutSeconds(int timeoutSeconds)
{
    configJson["TimeoutSeconds"] = timeoutSeconds;
}

bool ServerSettings::GetStreaming() const
{
    return configJson["Streaming"].get<bool>();
}

void ServerSettings::SetStreaming(bool streaming)
{
    configJson["Streaming"] = streaming;
}
