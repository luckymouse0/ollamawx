/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogAbout.h"
#include "FileUtils.h"

#include <wx/string.h>
#include <wx/utils.h>
#include <wx/log.h>

DialogAbout::DialogAbout(wxWindow* parent) : DialogAboutView(parent)
{
    notebookAbout->AddPage(new wxPanel(notebookAbout, wxID_ANY), "Description");
    notebookAbout->AddPage(new wxPanel(notebookAbout, wxID_ANY), "Copyright");
    notebookAbout->AddPage(new wxPanel(notebookAbout, wxID_ANY), "License");

    SetDescription();
}

void DialogAbout::OnNotebookAboutPageChanged(wxNotebookEvent& event)
{
    switch (event.GetSelection())
    {
        case 0: SetDescription(); break;
        case 1: SetCopyright(); break;
        case 2: SetLicense(); break;
    }

    event.Skip();
}

void DialogAbout::SetDescription()
{
    textCtrlAbout->SetValue("Ollamawx\n\n"
    "This project is an application interface built with wxWidgets for chatting with your local LLMs via ollama.ai (https://ollama.ai/) using the ollama API (https://github.com/ollama/ollama/blob/main/docs/api.md).\n\n"
    "For more information, check the README.md file included in the source code.\n");
}

void DialogAbout::SetCopyright()
{
    textCtrlAbout->SetValue("Ollamawx\n"
    "Copyright (C) 2024 luckymouse0, all rights reserved.\n\n"
    "This program is free software: you can redistribute it and/or modify"
    " it under the terms of the GNU General Public License version 2 as"
    " published by the Free Software Foundation.\n\n"
    "This program is distributed in the hope that it will be useful,"
    " but WITHOUT ANY WARRANTY; without even the implied warranty of"
    " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the"
    " GNU General Public License for more details.\n\n"
    "You should have received a copy of the GNU General Public License"
    " along with this program. If not, see <https://www.gnu.org/licenses/>.\n");
}

void DialogAbout::SetLicense()
{
    if (FileUtils::CheckFileExists(FileUtils::GetExecutablePath() + "/LICENSE"))
    {
        textCtrlAbout->LoadFile(FileUtils::GetExecutablePath() + "/LICENSE");
    }
    else
    {
        SetCopyright();
    }
}

void DialogAbout::OnTextURLAbout(wxTextUrlEvent& event)
{
    if (event.GetMouseEvent().LeftDown())
    {
        wxString url = textCtrlAbout->GetRange(event.GetURLStart(), event.GetURLEnd());

        if (!url.IsEmpty())
        {
            if (!wxLaunchDefaultBrowser(url))
            {
                wxLogWarning("Could not open the URL: %s", url);
            }
        }
        else
        {
            wxLogWarning("No URL found at the clicked position.");
        }
    }
}
