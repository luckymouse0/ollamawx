///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "Views.h"

///////////////////////////////////////////////////////////////////////////

FrameMainView::FrameMainView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );
	this->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW ) );
	this->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_GRAYTEXT ) );

	wxBoxSizer* bSizerMain;
	bSizerMain = new wxBoxSizer( wxVERTICAL );

	panelMain = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerMain;
	gbSizerMain = new wxGridBagSizer( 0, 0 );
	gbSizerMain->SetFlexibleDirection( wxBOTH );
	gbSizerMain->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxBoxSizer* bSizerServerButtons;
	bSizerServerButtons = new wxBoxSizer( wxHORIZONTAL );

	bitmapStatus = new wxStaticBitmap( panelMain, wxID_ANY, wxBitmap( wxT("res/buttons/disconnected.png"), wxBITMAP_TYPE_ANY ), wxDefaultPosition, wxSize( 34,34 ), 0 );
	bitmapStatus->SetToolTip( wxT("Ollama server status: DISCONNECTED") );

	bSizerServerButtons->Add( bitmapStatus, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );

	staticlineServerButtons = new wxStaticLine( panelMain, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_VERTICAL );
	bSizerServerButtons->Add( staticlineServerButtons, 0, wxEXPAND|wxLEFT|wxRIGHT, 5 );

	bpButtonStart = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonStart->SetBitmap( wxBitmap( wxT("res/buttons/start.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonStart->SetToolTip( wxT("Start Ollama server.") );

	bSizerServerButtons->Add( bpButtonStart, 0, wxALL, 5 );

	bpButtonStop = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonStop->SetBitmap( wxBitmap( wxT("res/buttons/stop.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonStop->Enable( false );
	bpButtonStop->SetToolTip( wxT("Stop Ollama server.") );

	bSizerServerButtons->Add( bpButtonStop, 0, wxALL, 5 );

	bpButtonSettings = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonSettings->SetBitmap( wxBitmap( wxT("res/buttons/settings.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonSettings->SetToolTip( wxT("Ollama server settings.") );

	bSizerServerButtons->Add( bpButtonSettings, 0, wxALL, 5 );


	gbSizerMain->Add( bSizerServerButtons, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), 0, 5 );

	wxBoxSizer* bSizerExtraButtons;
	bSizerExtraButtons = new wxBoxSizer( wxHORIZONTAL );

	bpButtonHelp = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonHelp->SetBitmap( wxBitmap( wxT("res/buttons/help.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonHelp->SetToolTip( wxT("Ollama server settings.") );

	bSizerExtraButtons->Add( bpButtonHelp, 0, wxBOTTOM|wxLEFT|wxTOP, 5 );


	gbSizerMain->Add( bSizerExtraButtons, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	bcomboBoxModel = new wxBitmapComboBox( panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 173,30 ), 0, NULL, 0 );
	bcomboBoxModel->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
	bcomboBoxModel->SetToolTip( wxT("Select a model.") );

	gbSizerMain->Add( bcomboBoxModel, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxLEFT, 5 );

	wxBoxSizer* bSizerModelButtons;
	bSizerModelButtons = new wxBoxSizer( wxHORIZONTAL );

	bpButtonUpdateModelList = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonUpdateModelList->SetBitmap( wxBitmap( wxT("res/buttons/update-models.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonUpdateModelList->Enable( false );
	bpButtonUpdateModelList->SetToolTip( wxT("Update model list.") );

	bSizerModelButtons->Add( bpButtonUpdateModelList, 0, wxBOTTOM|wxTOP, 5 );

	bpButtonRunModel = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonRunModel->SetBitmap( wxBitmap( wxT("res/buttons/run-model.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonRunModel->Enable( false );
	bpButtonRunModel->SetToolTip( wxT("Run selected model.") );

	bSizerModelButtons->Add( bpButtonRunModel, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );

	bpButtonShowInfoModel = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonShowInfoModel->SetBitmap( wxBitmap( wxT("res/buttons/show-info-model.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonShowInfoModel->Enable( false );
	bpButtonShowInfoModel->SetToolTip( wxT("Show info of selected model.") );

	bSizerModelButtons->Add( bpButtonShowInfoModel, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );

	bpButtonDeleteModel = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonDeleteModel->SetBitmap( wxBitmap( wxT("res/buttons/delete-model.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonDeleteModel->Enable( false );
	bpButtonDeleteModel->SetToolTip( wxT("Delete selected model.") );

	bSizerModelButtons->Add( bpButtonDeleteModel, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );


	gbSizerMain->Add( bSizerModelButtons, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), 0, 5 );

	wxBoxSizer* bSizerConversationsButtons;
	bSizerConversationsButtons = new wxBoxSizer( wxHORIZONTAL );

	bpButtonLoadConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonLoadConversation->SetBitmap( wxBitmap( wxT("res/buttons/load-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonLoadConversation->Enable( false );
	bpButtonLoadConversation->SetToolTip( wxT("Load selected conversation.") );

	bSizerConversationsButtons->Add( bpButtonLoadConversation, 0, wxTOP|wxLEFT, 5 );

	bpButtonRenameConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonRenameConversation->SetBitmap( wxBitmap( wxT("res/buttons/rename-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonRenameConversation->Enable( false );
	bpButtonRenameConversation->SetToolTip( wxT("Rename selected conversation.") );

	bSizerConversationsButtons->Add( bpButtonRenameConversation, 0, wxTOP|wxBOTTOM|wxLEFT, 5 );

	bpButtonDeleteConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxPoint( -1,-1 ), wxSize( 30,30 ), wxBU_AUTODRAW|0 );

	bpButtonDeleteConversation->SetBitmap( wxBitmap( wxT("res/buttons/delete-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonDeleteConversation->Enable( false );
	bpButtonDeleteConversation->SetToolTip( wxT("Delete selected conversation.") );

	bSizerConversationsButtons->Add( bpButtonDeleteConversation, 0, wxTOP|wxRIGHT|wxLEFT, 5 );

	searchCtrlFilter = new wxSearchCtrl( panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 203,30 ), 0 );
	#ifndef __WXMAC__
	searchCtrlFilter->ShowSearchButton( true );
	#endif
	searchCtrlFilter->ShowCancelButton( true );
	searchCtrlFilter->SetFont( wxFont( 10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, wxEmptyString ) );
	searchCtrlFilter->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT ) );
	searchCtrlFilter->Enable( false );

	bSizerConversationsButtons->Add( searchCtrlFilter, 0, wxTOP|wxLEFT, 5 );


	gbSizerMain->Add( bSizerConversationsButtons, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxEXPAND, 5 );

	listBoxConversation = new wxListBox( panelMain, wxID_ANY, wxDefaultPosition, wxSize( -1,361 ), 0, NULL, wxLB_NEEDED_SB|wxLB_SINGLE|wxBORDER_SUNKEN );
	listBoxConversation->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_HIGHLIGHTTEXT ) );
	listBoxConversation->Enable( false );
	listBoxConversation->SetToolTip( wxT("Saved conversations.") );

	gbSizerMain->Add( listBoxConversation, wxGBPosition( 3, 0 ), wxGBSpan( 4, 2 ), wxBOTTOM|wxEXPAND|wxLEFT|wxTOP, 5 );

	richTextChatBox = new wxRichTextCtrl( panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 603,370 ), wxTE_AUTO_URL|wxTE_READONLY|wxVSCROLL );
	richTextChatBox->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INFOTEXT ) );

	gbSizerMain->Add( richTextChatBox, wxGBPosition( 0, 3 ), wxGBSpan( 4, 2 ), wxEXPAND|wxALL, 5 );

	textCtrlChatInput = new wxTextCtrl( panelMain, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 603,70 ), wxTE_MULTILINE|wxVSCROLL );
	textCtrlChatInput->SetForegroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INACTIVECAPTIONTEXT ) );
	textCtrlChatInput->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_INACTIVECAPTION ) );
	textCtrlChatInput->Enable( false );

	gbSizerMain->Add( textCtrlChatInput, wxGBPosition( 4, 3 ), wxGBSpan( 1, 2 ), wxALL|wxEXPAND, 5 );

	wxBoxSizer* bSizerConversationButtons;
	bSizerConversationButtons = new wxBoxSizer( wxHORIZONTAL );

	bpButtonCloseConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonCloseConversation->SetBitmap( wxBitmap( wxT("res/buttons/close-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonCloseConversation->Enable( false );
	bpButtonCloseConversation->SetToolTip( wxT("Close conversation.") );

	bSizerConversationButtons->Add( bpButtonCloseConversation, 0, wxALL, 5 );

	bpButtonSaveCloseConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonSaveCloseConversation->SetBitmap( wxBitmap( wxT("res/buttons/save-close-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonSaveCloseConversation->Enable( false );
	bpButtonSaveCloseConversation->SetToolTip( wxT("Save and Close conversation.") );

	bSizerConversationButtons->Add( bpButtonSaveCloseConversation, 0, wxALL, 5 );

	bpButtonSaveConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonSaveConversation->SetBitmap( wxBitmap( wxT("res/buttons/save-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonSaveConversation->Enable( false );
	bpButtonSaveConversation->SetToolTip( wxT("Save conversation.") );

	bSizerConversationButtons->Add( bpButtonSaveConversation, 0, wxALL, 5 );

	bpButtonEditSession = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonEditSession->SetBitmap( wxBitmap( wxT("res/buttons/edit-session.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonEditSession->Enable( false );
	bpButtonEditSession->SetToolTip( wxT("Edit conversation session.") );

	bSizerConversationButtons->Add( bpButtonEditSession, 0, wxALL, 5 );

	bpButtonClearConversation = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonClearConversation->SetBitmap( wxBitmap( wxT("res/buttons/clear-conversation.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonClearConversation->Enable( false );
	bpButtonClearConversation->SetToolTip( wxT("Clear conversation and chat history.") );

	bSizerConversationButtons->Add( bpButtonClearConversation, 0, wxALL, 5 );


	gbSizerMain->Add( bSizerConversationButtons, wxGBPosition( 5, 3 ), wxGBSpan( 1, 1 ), 0, 5 );

	wxBoxSizer* bSizerConversationSendButtons;
	bSizerConversationSendButtons = new wxBoxSizer( wxHORIZONTAL );

	bpButtonAttach = new wxBitmapButton( panelMain, wxID_ANY, wxNullBitmap, wxDefaultPosition, wxSize( 34,34 ), wxBU_AUTODRAW|0 );

	bpButtonAttach->SetBitmap( wxBitmap( wxT("res/buttons/attach.png"), wxBITMAP_TYPE_ANY ) );
	bpButtonAttach->Enable( false );
	bpButtonAttach->SetToolTip( wxT("Attach to conversation.") );

	bSizerConversationSendButtons->Add( bpButtonAttach, 0, wxALL, 5 );

	buttonSend = new wxButton( panelMain, wxID_ANY, wxT("Send"), wxDefaultPosition, wxSize( 200,34 ), 0 );

	buttonSend->SetBitmap( wxBitmap( wxT("res/buttons/send.png"), wxBITMAP_TYPE_ANY ) );
	buttonSend->Enable( false );
	buttonSend->SetToolTip( wxT("Send message to conversation.") );

	bSizerConversationSendButtons->Add( buttonSend, 0, wxALL, 5 );


	gbSizerMain->Add( bSizerConversationSendButtons, wxGBPosition( 5, 4 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );


	gbSizerMain->AddGrowableCol( 4 );
	gbSizerMain->AddGrowableRow( 3 );
	gbSizerMain->AddGrowableRow( 4 );

	panelMain->SetSizer( gbSizerMain );
	panelMain->Layout();
	gbSizerMain->Fit( panelMain );
	bSizerMain->Add( panelMain, 1, wxEXPAND|wxALL, 5 );

	wxBoxSizer* bSizerMenuHelp;
	bSizerMenuHelp = new wxBoxSizer( wxVERTICAL );

	panelMenuHelp = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	panelMenuHelp->Enable( false );
	panelMenuHelp->Hide();

	menuHelp = new wxMenu();
	wxMenuItem* menuItemAbout;
	menuItemAbout = new wxMenuItem( menuHelp, wxID_ANY, wxString( wxT("About...") ) , wxT("Show about dialog."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	menuItemAbout->SetBitmaps( wxBitmap( wxT("res/buttons/about.png"), wxBITMAP_TYPE_ANY ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	menuItemAbout->SetBitmap( wxBitmap( wxT("res/buttons/about.png"), wxBITMAP_TYPE_ANY ) );
	#endif
	menuHelp->Append( menuItemAbout );

	panelMenuHelp->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( FrameMainView::panelMenuHelpOnContextMenu ), NULL, this );

	bSizerMenuHelp->Add( panelMenuHelp, 1, wxEXPAND | wxALL, 5 );


	bSizerMain->Add( bSizerMenuHelp, 1, wxEXPAND, 5 );

	wxBoxSizer* bSizerMenuAttach;
	bSizerMenuAttach = new wxBoxSizer( wxVERTICAL );

	panelMenuAttach = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	menuAttach = new wxMenu();
	wxMenuItem* menuItemAttachImage;
	menuItemAttachImage = new wxMenuItem( menuAttach, wxID_ANY, wxString( wxT("Attach image") ) , wxT("Attach image to conversation."), wxITEM_NORMAL );
	#ifdef __WXMSW__
	menuItemAttachImage->SetBitmaps( wxBitmap( wxT("res/buttons/attach-image.png"), wxBITMAP_TYPE_ANY ) );
	#elif (defined( __WXGTK__ ) || defined( __WXOSX__ ))
	menuItemAttachImage->SetBitmap( wxBitmap( wxT("res/buttons/attach-image.png"), wxBITMAP_TYPE_ANY ) );
	#endif
	menuAttach->Append( menuItemAttachImage );
	menuItemAttachImage->Enable( false );

	panelMenuAttach->Connect( wxEVT_RIGHT_DOWN, wxMouseEventHandler( FrameMainView::panelMenuAttachOnContextMenu ), NULL, this );

	bSizerMenuAttach->Add( panelMenuAttach, 1, wxEXPAND | wxALL, 5 );


	bSizerMain->Add( bSizerMenuAttach, 1, wxEXPAND, 5 );


	this->SetSizer( bSizerMain );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameMainView::OnClose ) );
	bpButtonStart->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnStartClick ), NULL, this );
	bpButtonStop->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnStopClick ), NULL, this );
	bpButtonSettings->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSettingsClick ), NULL, this );
	bpButtonHelp->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnHelpClick ), NULL, this );
	bcomboBoxModel->Connect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameMainView::OnComboBoxModel ), NULL, this );
	bpButtonUpdateModelList->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnUpdateModelListClick ), NULL, this );
	bpButtonRunModel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnRunModelClick ), NULL, this );
	bpButtonShowInfoModel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnShowInfoModelClick ), NULL, this );
	bpButtonDeleteModel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnDeleteModelClick ), NULL, this );
	bpButtonLoadConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnLoadConversationClick ), NULL, this );
	bpButtonRenameConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnRenameConversationClick ), NULL, this );
	bpButtonDeleteConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnDeleteConversationClick ), NULL, this );
	searchCtrlFilter->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameMainView::OnSearchFilter ), NULL, this );
	textCtrlChatInput->Connect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameMainView::OnChatInputKeyDown ), NULL, this );
	bpButtonCloseConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnCloseConversationClick ), NULL, this );
	bpButtonSaveCloseConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSaveCloseConversationClick ), NULL, this );
	bpButtonSaveConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSaveConversationClick ), NULL, this );
	bpButtonEditSession->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnEditSessionClick ), NULL, this );
	bpButtonClearConversation->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnClearConversationClick ), NULL, this );
	bpButtonAttach->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnAttachClick ), NULL, this );
	buttonSend->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSendClick ), NULL, this );
	menuHelp->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnAboutMenuSelection ), this, menuItemAbout->GetId());
	menuAttach->Bind(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler( FrameMainView::OnAttachImage ), this, menuItemAttachImage->GetId());
}

FrameMainView::~FrameMainView()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( FrameMainView::OnClose ) );
	bpButtonStart->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnStartClick ), NULL, this );
	bpButtonStop->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnStopClick ), NULL, this );
	bpButtonSettings->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSettingsClick ), NULL, this );
	bpButtonHelp->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnHelpClick ), NULL, this );
	bcomboBoxModel->Disconnect( wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler( FrameMainView::OnComboBoxModel ), NULL, this );
	bpButtonUpdateModelList->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnUpdateModelListClick ), NULL, this );
	bpButtonRunModel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnRunModelClick ), NULL, this );
	bpButtonShowInfoModel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnShowInfoModelClick ), NULL, this );
	bpButtonDeleteModel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnDeleteModelClick ), NULL, this );
	bpButtonLoadConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnLoadConversationClick ), NULL, this );
	bpButtonRenameConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnRenameConversationClick ), NULL, this );
	bpButtonDeleteConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnDeleteConversationClick ), NULL, this );
	searchCtrlFilter->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( FrameMainView::OnSearchFilter ), NULL, this );
	textCtrlChatInput->Disconnect( wxEVT_KEY_DOWN, wxKeyEventHandler( FrameMainView::OnChatInputKeyDown ), NULL, this );
	bpButtonCloseConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnCloseConversationClick ), NULL, this );
	bpButtonSaveCloseConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSaveCloseConversationClick ), NULL, this );
	bpButtonSaveConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSaveConversationClick ), NULL, this );
	bpButtonEditSession->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnEditSessionClick ), NULL, this );
	bpButtonClearConversation->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnClearConversationClick ), NULL, this );
	bpButtonAttach->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnAttachClick ), NULL, this );
	buttonSend->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( FrameMainView::OnSendClick ), NULL, this );

	delete menuHelp;
	delete menuAttach;
}

DialogServerSettingsView::DialogServerSettingsView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerServerSettings;
	bSizerServerSettings = new wxBoxSizer( wxVERTICAL );

	panelServerSettings = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerServerSettings;
	gbSizerServerSettings = new wxGridBagSizer( 0, 0 );
	gbSizerServerSettings->SetFlexibleDirection( wxBOTH );
	gbSizerServerSettings->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextPath = new wxStaticText( panelServerSettings, wxID_ANY, wxT("Ollama path:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextPath->Wrap( -1 );
	gbSizerServerSettings->Add( staticTextPath, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	filePickerOllama = new wxFilePickerCtrl( panelServerSettings, wxID_ANY, wxT("ollama"), wxT("Select ollama file"), wxT("*"), wxDefaultPosition, wxSize( 200,-1 ), wxFLP_DEFAULT_STYLE|wxFLP_SMALL|wxFLP_USE_TEXTCTRL );
	gbSizerServerSettings->Add( filePickerOllama, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	staticTextHost = new wxStaticText( panelServerSettings, wxID_ANY, wxT("Host:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextHost->Wrap( -1 );
	gbSizerServerSettings->Add( staticTextHost, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlHost = new wxTextCtrl( panelServerSettings, wxID_ANY, wxT("http://localhost:11434"), wxDefaultPosition, wxSize( 200,-1 ), 0 );
	textCtrlHost->SetToolTip( wxT("Server or machine where the Ollama container is deployed or where the Ollama Web UI is accessed.") );

	gbSizerServerSettings->Add( textCtrlHost, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	staticTextKeepAlive = new wxStaticText( panelServerSettings, wxID_ANY, wxT("Keep alive minutes:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextKeepAlive->Wrap( -1 );
	gbSizerServerSettings->Add( staticTextKeepAlive, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	spinCtrlKeepAlive = new wxSpinCtrl( panelServerSettings, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 999, 20 );
	spinCtrlKeepAlive->SetToolTip( wxT("How long a model will be stored in memory, in minutes. 0 = models won't stay loaded, -1 = models stay loaded indefinitely.") );

	gbSizerServerSettings->Add( spinCtrlKeepAlive, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	staticTextTimeout = new wxStaticText( panelServerSettings, wxID_ANY, wxT("Timeout seconds:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTimeout->Wrap( -1 );
	gbSizerServerSettings->Add( staticTextTimeout, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	spinCtrlTimeout = new wxSpinCtrl( panelServerSettings, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 999, 120 );
	spinCtrlTimeout->SetToolTip( wxT("Timeout for server responses, in seconds. 0 = no timeout.") );

	gbSizerServerSettings->Add( spinCtrlTimeout, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	checkBoxStreaming = new wxCheckBox( panelServerSettings, wxID_ANY, wxT("Streaming"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	checkBoxStreaming->SetValue(true);
	checkBoxStreaming->SetToolTip( wxT("If checked, text output will be generated and displayed token by token in real-time. If unchecked, the entire response will be displayed only after it is fully generated.") );

	gbSizerServerSettings->Add( checkBoxStreaming, wxGBPosition( 4, 0 ), wxGBSpan( 1, 2 ), wxALIGN_CENTER_HORIZONTAL|wxALL, 5 );

	sdbSizerServerSettings = new wxStdDialogButtonSizer();
	sdbSizerServerSettingsOK = new wxButton( panelServerSettings, wxID_OK );
	sdbSizerServerSettings->AddButton( sdbSizerServerSettingsOK );
	sdbSizerServerSettingsCancel = new wxButton( panelServerSettings, wxID_CANCEL );
	sdbSizerServerSettings->AddButton( sdbSizerServerSettingsCancel );
	sdbSizerServerSettings->Realize();

	gbSizerServerSettings->Add( sdbSizerServerSettings, wxGBPosition( 5, 1 ), wxGBSpan( 1, 2 ), wxTOP, 10 );

	buttonDefault = new wxButton( panelServerSettings, wxID_ANY, wxT("Default"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerServerSettings->Add( buttonDefault, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxLEFT|wxTOP, 10 );


	panelServerSettings->SetSizer( gbSizerServerSettings );
	panelServerSettings->Layout();
	gbSizerServerSettings->Fit( panelServerSettings );
	bSizerServerSettings->Add( panelServerSettings, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerServerSettings );
	this->Layout();
	bSizerServerSettings->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	sdbSizerServerSettingsOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogServerSettingsView::OnServerSettingsOKClick ), NULL, this );
	buttonDefault->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogServerSettingsView::OnDefaultClick ), NULL, this );
}

DialogServerSettingsView::~DialogServerSettingsView()
{
	// Disconnect Events
	sdbSizerServerSettingsOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogServerSettingsView::OnServerSettingsOKClick ), NULL, this );
	buttonDefault->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogServerSettingsView::OnDefaultClick ), NULL, this );

}

DialogModelInformationView::DialogModelInformationView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerModelInformation;
	bSizerModelInformation = new wxBoxSizer( wxVERTICAL );

	panelModelInformation = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerModelInformation;
	gbSizerModelInformation = new wxGridBagSizer( 0, 0 );
	gbSizerModelInformation->SetFlexibleDirection( wxBOTH );
	gbSizerModelInformation->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextModelDetails = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Model details"), wxDefaultPosition, wxSize( 92,-1 ), 0 );
	staticTextModelDetails->Wrap( -1 );
	staticTextModelDetails->SetFont( wxFont( wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD, false, wxEmptyString ) );

	gbSizerModelInformation->Add( staticTextModelDetails, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER|wxALL, 5 );

	wxGridSizer* gSizerModelDetails;
	gSizerModelDetails = new wxGridSizer( 5, 2, 0, 0 );

	staticTextFormat = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Format:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFormat->Wrap( -1 );
	gSizerModelDetails->Add( staticTextFormat, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlFormat = new wxTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxTE_READONLY );
	gSizerModelDetails->Add( textCtrlFormat, 0, wxALL|wxEXPAND, 5 );

	staticTextFamily = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Family:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFamily->Wrap( -1 );
	gSizerModelDetails->Add( staticTextFamily, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlFamily = new wxTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	gSizerModelDetails->Add( textCtrlFamily, 0, wxALL|wxEXPAND, 5 );

	staticTextFamilies = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Families:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFamilies->Wrap( -1 );
	gSizerModelDetails->Add( staticTextFamilies, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlFamilies = new wxTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	gSizerModelDetails->Add( textCtrlFamilies, 0, wxALL|wxEXPAND, 5 );

	staticTextParameterSize = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Parameter Size:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextParameterSize->Wrap( -1 );
	gSizerModelDetails->Add( staticTextParameterSize, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlParameterSize = new wxTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	gSizerModelDetails->Add( textCtrlParameterSize, 0, wxALL|wxEXPAND, 5 );

	staticTextQuantizationLevel = new wxStaticText( panelModelInformation, wxID_ANY, wxT("Quantization Level:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextQuantizationLevel->Wrap( -1 );
	gSizerModelDetails->Add( staticTextQuantizationLevel, 0, wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlQuantizationLevel = new wxTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY );
	gSizerModelDetails->Add( textCtrlQuantizationLevel, 0, wxALL|wxEXPAND, 5 );


	gbSizerModelInformation->Add( gSizerModelDetails, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	staticlineModelInformation = new wxStaticLine( panelModelInformation, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	gbSizerModelInformation->Add( staticlineModelInformation, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxEXPAND | wxALL, 5 );

	wxGridBagSizer* gbSizerModelData;
	gbSizerModelData = new wxGridBagSizer( 0, 0 );
	gbSizerModelData->SetFlexibleDirection( wxBOTH );
	gbSizerModelData->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	richTextModelInformation = new wxRichTextCtrl( panelModelInformation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 300,-1 ), wxTE_AUTO_URL|wxTE_READONLY|wxVSCROLL|wxWANTS_CHARS );
	gbSizerModelData->Add( richTextModelInformation, wxGBPosition( 0, 0 ), wxGBSpan( 5, 1 ), wxALL|wxEXPAND, 5 );

	buttonModelfile = new wxButton( panelModelInformation, wxID_ANY, wxT("Modelfile"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerModelData->Add( buttonModelfile, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

	buttonLicense = new wxButton( panelModelInformation, wxID_ANY, wxT("License"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerModelData->Add( buttonLicense, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

	buttonParameters = new wxButton( panelModelInformation, wxID_ANY, wxT("Parameters"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerModelData->Add( buttonParameters, wxGBPosition( 2, 1 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );

	buttonSystemMessage = new wxButton( panelModelInformation, wxID_ANY, wxT("System Message"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerModelData->Add( buttonSystemMessage, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	buttonPromptTemplate = new wxButton( panelModelInformation, wxID_ANY, wxT("Prompt Template"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerModelData->Add( buttonPromptTemplate, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );


	gbSizerModelInformation->Add( gbSizerModelData, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sdbSizerModelInformation = new wxStdDialogButtonSizer();
	sdbSizerModelInformationOK = new wxButton( panelModelInformation, wxID_OK );
	sdbSizerModelInformation->AddButton( sdbSizerModelInformationOK );
	sdbSizerModelInformation->Realize();

	gbSizerModelInformation->Add( sdbSizerModelInformation, wxGBPosition( 4, 0 ), wxGBSpan( 1, 2 ), wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 10 );


	panelModelInformation->SetSizer( gbSizerModelInformation );
	panelModelInformation->Layout();
	gbSizerModelInformation->Fit( panelModelInformation );
	bSizerModelInformation->Add( panelModelInformation, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerModelInformation );
	this->Layout();
	bSizerModelInformation->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	buttonModelfile->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnModelfileClick ), NULL, this );
	buttonLicense->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnLicenseClick ), NULL, this );
	buttonParameters->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnParametersClick ), NULL, this );
	buttonSystemMessage->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnSystemMessageClick ), NULL, this );
	buttonPromptTemplate->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnPromptTemplateClick ), NULL, this );
	sdbSizerModelInformationOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnModelInformationOKClick ), NULL, this );
}

DialogModelInformationView::~DialogModelInformationView()
{
	// Disconnect Events
	buttonModelfile->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnModelfileClick ), NULL, this );
	buttonLicense->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnLicenseClick ), NULL, this );
	buttonParameters->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnParametersClick ), NULL, this );
	buttonSystemMessage->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnSystemMessageClick ), NULL, this );
	buttonPromptTemplate->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnPromptTemplateClick ), NULL, this );
	sdbSizerModelInformationOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogModelInformationView::OnModelInformationOKClick ), NULL, this );

}

DialogEditSessionView::DialogEditSessionView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerEditSession;
	bSizerEditSession = new wxBoxSizer( wxVERTICAL );

	panelEditSession = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerEditSession;
	gbSizerEditSession = new wxGridBagSizer( 0, 0 );
	gbSizerEditSession->SetFlexibleDirection( wxBOTH );
	gbSizerEditSession->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxBoxSizer* bSizerSystemMessage;
	bSizerSystemMessage = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableSystemMessage = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSystemMessage->Add( checkBoxEnableSystemMessage, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextSystemMessage = new wxStaticText( panelEditSession, wxID_ANY, wxT("System Message:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextSystemMessage->Wrap( -1 );
	bSizerSystemMessage->Add( staticTextSystemMessage, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	textCtrlSystemMessage = new wxTextCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE );
	textCtrlSystemMessage->Enable( false );
	textCtrlSystemMessage->SetToolTip( wxT("Sets a custom system message to specify the behavior of the chat assistant.") );

	bSizerSystemMessage->Add( textCtrlSystemMessage, 1, wxALL|wxEXPAND, 5 );


	gbSizerEditSession->Add( bSizerSystemMessage, wxGBPosition( 0, 0 ), wxGBSpan( 1, 2 ), wxEXPAND, 5 );

	wxBoxSizer* bSizerTemplate;
	bSizerTemplate = new wxBoxSizer( wxHORIZONTAL );

	bSizerTemplate->SetMinSize( wxSize( -1,70 ) );
	checkBoxEnableTemplate = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTemplate->Add( checkBoxEnableTemplate, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTemplate = new wxStaticText( panelEditSession, wxID_ANY, wxT("Template:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTemplate->Wrap( -1 );
	bSizerTemplate->Add( staticTextTemplate, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	textCtrlTemplate = new wxTextCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( -1,-1 ), wxTE_MULTILINE );
	textCtrlTemplate->Enable( false );
	textCtrlTemplate->SetToolTip( wxT("The full prompt template to be passed into the model. It may include (optionally) a system message, a user message and the response from the model. Note: syntax may be model specific.") );

	bSizerTemplate->Add( textCtrlTemplate, 1, wxALL|wxEXPAND, 5 );


	gbSizerEditSession->Add( bSizerTemplate, wxGBPosition( 1, 0 ), wxGBSpan( 1, 2 ), wxEXPAND, 5 );

	staticlineConversationParameters = new wxStaticLine( panelEditSession, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	gbSizerEditSession->Add( staticlineConversationParameters, wxGBPosition( 2, 0 ), wxGBSpan( 1, 2 ), wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizerSeed;
	bSizerSeed = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableSeed = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerSeed->Add( checkBoxEnableSeed, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextSeed = new wxStaticText( panelEditSession, wxID_ANY, wxT("Seed:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextSeed->Wrap( -1 );
	bSizerSeed->Add( staticTextSeed, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlSeed = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 2147483647, -1 );
	spinCtrlSeed->Enable( false );
	spinCtrlSeed->SetToolTip( wxT("Sets the random number seed to use for generation. Setting this to a specific number will make the model generate the same text for the same prompt.") );

	bSizerSeed->Add( spinCtrlSeed, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerSeed, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerTemperature;
	bSizerTemperature = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableTemperature = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTemperature->Add( checkBoxEnableTemperature, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTemperature = new wxStaticText( panelEditSession, wxID_ANY, wxT("Temperature:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTemperature->Wrap( -1 );
	bSizerTemperature->Add( staticTextTemperature, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleTemperature = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 0.8, 0.01 );
	spinCtrlDoubleTemperature->SetDigits( 2 );
	spinCtrlDoubleTemperature->Enable( false );
	spinCtrlDoubleTemperature->SetToolTip( wxT("The lower the value, the more deterministic the end result. On the other hand, a higher value leads to more randomness, hence more diverse and creative output.") );

	bSizerTemperature->Add( spinCtrlDoubleTemperature, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerTemperature, wxGBPosition( 4, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerContextSize;
	bSizerContextSize = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableContextSize = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerContextSize->Add( checkBoxEnableContextSize, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextContextSize = new wxStaticText( panelEditSession, wxID_ANY, wxT("Context Size:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextContextSize->Wrap( -1 );
	bSizerContextSize->Add( staticTextContextSize, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlContextSize = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 2147483647, 2048 );
	spinCtrlContextSize->Enable( false );
	spinCtrlContextSize->SetToolTip( wxT("Sets the size of the context window used to generate the next token.") );

	bSizerContextSize->Add( spinCtrlContextSize, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerContextSize, wxGBPosition( 5, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerPredictTokens;
	bSizerPredictTokens = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnablePredictTokens = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerPredictTokens->Add( checkBoxEnablePredictTokens, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextPredictTokens = new wxStaticText( panelEditSession, wxID_ANY, wxT("Predict Tokens:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextPredictTokens->Wrap( -1 );
	bSizerPredictTokens->Add( staticTextPredictTokens, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlPredictTokens = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -2, 2147483647, -1 );
	spinCtrlPredictTokens->Enable( false );
	spinCtrlPredictTokens->SetToolTip( wxT("Maximum number of tokens to predict when generating text. (-1 = infinite generation, -2 = fill context)") );

	bSizerPredictTokens->Add( spinCtrlPredictTokens, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerPredictTokens, wxGBPosition( 6, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerRepeatLast;
	bSizerRepeatLast = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableRepeatLast = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerRepeatLast->Add( checkBoxEnableRepeatLast, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextRepeatLast = new wxStaticText( panelEditSession, wxID_ANY, wxT("Repeat Last:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextRepeatLast->Wrap( -1 );
	bSizerRepeatLast->Add( staticTextRepeatLast, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlRepeatLast = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 2147483647, 64 );
	spinCtrlRepeatLast->Enable( false );
	spinCtrlRepeatLast->SetToolTip( wxT("How far back the model checks to prevent repetition. (0 = disabled, -1 = context size)") );

	bSizerRepeatLast->Add( spinCtrlRepeatLast, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerRepeatLast, wxGBPosition( 7, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerRepeatPenalty;
	bSizerRepeatPenalty = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableRepeatPenalty = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerRepeatPenalty->Add( checkBoxEnableRepeatPenalty, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextRepeatPenalty = new wxStaticText( panelEditSession, wxID_ANY, wxT("Repeat Penalty:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextRepeatPenalty->Wrap( -1 );
	bSizerRepeatPenalty->Add( staticTextRepeatPenalty, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleRepeatPenalty = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 2, 1.1, 0.01 );
	spinCtrlDoubleRepeatPenalty->SetDigits( 2 );
	spinCtrlDoubleRepeatPenalty->Enable( false );
	spinCtrlDoubleRepeatPenalty->SetToolTip( wxT("Penalizes repetitions. Higher values increase the penalty.") );

	bSizerRepeatPenalty->Add( spinCtrlDoubleRepeatPenalty, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerRepeatPenalty, wxGBPosition( 8, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerTopK;
	bSizerTopK = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableTopK = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTopK->Add( checkBoxEnableTopK, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTopK = new wxStaticText( panelEditSession, wxID_ANY, wxT("Top K:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTopK->Wrap( -1 );
	bSizerTopK->Add( staticTextTopK, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlTopK = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, -1, 100, 40 );
	spinCtrlTopK->Enable( false );
	spinCtrlTopK->SetToolTip( wxT("Limits the likelihood of less probable responses. Higher values allow more diversity.") );

	bSizerTopK->Add( spinCtrlTopK, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerTopK, wxGBPosition( 3, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerTopP;
	bSizerTopP = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableTopP = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTopP->Add( checkBoxEnableTopP, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTopP = new wxStaticText( panelEditSession, wxID_ANY, wxT("Top P:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTopP->Wrap( -1 );
	bSizerTopP->Add( staticTextTopP, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleTopP = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 0.9, 0.01 );
	spinCtrlDoubleTopP->SetDigits( 2 );
	spinCtrlDoubleTopP->Enable( false );
	spinCtrlDoubleTopP->SetToolTip( wxT("Works with \"Top K\" to manage diversity of responses. Higher values lead to more diversity.") );

	bSizerTopP->Add( spinCtrlDoubleTopP, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerTopP, wxGBPosition( 4, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerTFSZ;
	bSizerTFSZ = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableTFSZ = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTFSZ->Add( checkBoxEnableTFSZ, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTFSZ = new wxStaticText( panelEditSession, wxID_ANY, wxT("TFS Z:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTFSZ->Wrap( -1 );
	bSizerTFSZ->Add( staticTextTFSZ, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleTFSZ = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 2, 1, 0.01 );
	spinCtrlDoubleTFSZ->SetDigits( 2 );
	spinCtrlDoubleTFSZ->Enable( false );
	spinCtrlDoubleTFSZ->SetToolTip( wxT("Tail-free sampling is used to reduce the impact of less probable tokens from the output. A higher value (e.g., 2.0) will reduce the impact more, while a value of 1.0 disables this setting.") );

	bSizerTFSZ->Add( spinCtrlDoubleTFSZ, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerTFSZ, wxGBPosition( 5, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerMirostat;
	bSizerMirostat = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableMirostat = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerMirostat->Add( checkBoxEnableMirostat, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextMirostat = new wxStaticText( panelEditSession, wxID_ANY, wxT("Mirostat:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextMirostat->Wrap( -1 );
	bSizerMirostat->Add( staticTextMirostat, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	wxString choiceMirostatChoices[] = { wxT("Disabled"), wxT("Mirostat"), wxT("Mirostat 2.0") };
	int choiceMirostatNChoices = sizeof( choiceMirostatChoices ) / sizeof( wxString );
	choiceMirostat = new wxChoice( panelEditSession, wxID_ANY, wxDefaultPosition, wxDefaultSize, choiceMirostatNChoices, choiceMirostatChoices, 0 );
	choiceMirostat->SetSelection( 0 );
	choiceMirostat->Enable( false );
	choiceMirostat->SetToolTip( wxT("Enables Mirostat sampling to control perplexity.") );

	bSizerMirostat->Add( choiceMirostat, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerMirostat, wxGBPosition( 6, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerMirostatTau;
	bSizerMirostatTau = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableMirostatTau = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerMirostatTau->Add( checkBoxEnableMirostatTau, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextMirostatTau = new wxStaticText( panelEditSession, wxID_ANY, wxT("Mirostat Tau:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextMirostatTau->Wrap( -1 );
	bSizerMirostatTau->Add( staticTextMirostatTau, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleMirostatTau = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 10, 5, 0.01 );
	spinCtrlDoubleMirostatTau->SetDigits( 2 );
	spinCtrlDoubleMirostatTau->Enable( false );
	spinCtrlDoubleMirostatTau->SetToolTip( wxT("Balances between coherence and diversity of output. Lower values yield more coherence.") );

	bSizerMirostatTau->Add( spinCtrlDoubleMirostatTau, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerMirostatTau, wxGBPosition( 7, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerMirostatEta;
	bSizerMirostatEta = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableMirostatEta = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerMirostatEta->Add( checkBoxEnableMirostatEta, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextMirostatEta = new wxStaticText( panelEditSession, wxID_ANY, wxT("Mirostat Eta:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextMirostatEta->Wrap( -1 );
	bSizerMirostatEta->Add( staticTextMirostatEta, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleMirostatEta = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 0.1, 0.01 );
	spinCtrlDoubleMirostatEta->SetDigits( 2 );
	spinCtrlDoubleMirostatEta->Enable( false );
	spinCtrlDoubleMirostatEta->SetToolTip( wxT("Influences response speed to feedback in text generation. Higher rates mean quicker adjustments.") );

	bSizerMirostatEta->Add( spinCtrlDoubleMirostatEta, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerMirostatEta, wxGBPosition( 8, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	staticlineParameters = new wxStaticLine( panelEditSession, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	gbSizerEditSession->Add( staticlineParameters, wxGBPosition( 9, 0 ), wxGBSpan( 1, 2 ), wxEXPAND | wxALL, 5 );

	wxBoxSizer* bSizerKeepTokens;
	bSizerKeepTokens = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableKeepTokens = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerKeepTokens->Add( checkBoxEnableKeepTokens, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextKeepTokens = new wxStaticText( panelEditSession, wxID_ANY, wxT("Keep Tokens:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextKeepTokens->Wrap( -1 );
	bSizerKeepTokens->Add( staticTextKeepTokens, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlKeepTokens = new wxSpinCtrl( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 2147483647, 4 );
	spinCtrlKeepTokens->Enable( false );
	spinCtrlKeepTokens->SetToolTip( wxT("Number of tokens to keep from the prompt, i.e. number of tokens to keep unchanged at the beginning of generated text.") );

	bSizerKeepTokens->Add( spinCtrlKeepTokens, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerKeepTokens, wxGBPosition( 10, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerTypicalP;
	bSizerTypicalP = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableTypicalP = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerTypicalP->Add( checkBoxEnableTypicalP, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextTypicalP = new wxStaticText( panelEditSession, wxID_ANY, wxT("Typical P:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTypicalP->Wrap( -1 );
	bSizerTypicalP->Add( staticTextTypicalP, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleTypicalP = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 1, 0.01 );
	spinCtrlDoubleTypicalP->SetDigits( 2 );
	spinCtrlDoubleTypicalP->Enable( false );
	spinCtrlDoubleTypicalP->SetToolTip( wxT("Sets a minimum likelihood threshold for considering a token.") );

	bSizerTypicalP->Add( spinCtrlDoubleTypicalP, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerTypicalP, wxGBPosition( 11, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerPenalizeNewlines;
	bSizerPenalizeNewlines = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnablePenalizeNewlines = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerPenalizeNewlines->Add( checkBoxEnablePenalizeNewlines, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	checkBoxPenalizeNewlines = new wxCheckBox( panelEditSession, wxID_ANY, wxT("Penalize New Lines:"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT );
	checkBoxPenalizeNewlines->SetValue(true);
	checkBoxPenalizeNewlines->Enable( false );
	checkBoxPenalizeNewlines->SetToolTip( wxT("Whether to penalize the generation of new lines.") );

	bSizerPenalizeNewlines->Add( checkBoxPenalizeNewlines, 0, wxALIGN_CENTER_VERTICAL|wxALL, 7 );


	gbSizerEditSession->Add( bSizerPenalizeNewlines, wxGBPosition( 12, 0 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerPresencePenalty;
	bSizerPresencePenalty = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnablePresencePenalty = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerPresencePenalty->Add( checkBoxEnablePresencePenalty, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextPresencePenalty = new wxStaticText( panelEditSession, wxID_ANY, wxT("Presence Penalty:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextPresencePenalty->Wrap( -1 );
	bSizerPresencePenalty->Add( staticTextPresencePenalty, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoublePresencePenalty = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 0, 0.01 );
	spinCtrlDoublePresencePenalty->SetDigits( 2 );
	spinCtrlDoublePresencePenalty->Enable( false );
	spinCtrlDoublePresencePenalty->SetToolTip( wxT("Penalizes new tokens based on their presence so far.") );

	bSizerPresencePenalty->Add( spinCtrlDoublePresencePenalty, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerPresencePenalty, wxGBPosition( 10, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	wxBoxSizer* bSizerFrequencyPenalty;
	bSizerFrequencyPenalty = new wxBoxSizer( wxHORIZONTAL );

	checkBoxEnableFrequencyPenalty = new wxCheckBox( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizerFrequencyPenalty->Add( checkBoxEnableFrequencyPenalty, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 10 );

	staticTextFrequencyPenalty = new wxStaticText( panelEditSession, wxID_ANY, wxT("Frequency Penalty:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFrequencyPenalty->Wrap( -1 );
	bSizerFrequencyPenalty->Add( staticTextFrequencyPenalty, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	spinCtrlDoubleFrequencyPenalty = new wxSpinCtrlDouble( panelEditSession, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, 1, 0, 0.01 );
	spinCtrlDoubleFrequencyPenalty->SetDigits( 2 );
	spinCtrlDoubleFrequencyPenalty->Enable( false );
	spinCtrlDoubleFrequencyPenalty->SetToolTip( wxT("Penalizes new tokens based on their frequency so far.") );

	bSizerFrequencyPenalty->Add( spinCtrlDoubleFrequencyPenalty, 0, wxALL, 5 );


	gbSizerEditSession->Add( bSizerFrequencyPenalty, wxGBPosition( 11, 1 ), wxGBSpan( 1, 1 ), wxALIGN_RIGHT|wxEXPAND, 5 );

	staticlineExtraParameters = new wxStaticLine( panelEditSession, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	gbSizerEditSession->Add( staticlineExtraParameters, wxGBPosition( 13, 0 ), wxGBSpan( 1, 2 ), wxEXPAND | wxALL, 5 );

	sdbSizerEditSession = new wxStdDialogButtonSizer();
	sdbSizerEditSessionOK = new wxButton( panelEditSession, wxID_OK );
	sdbSizerEditSession->AddButton( sdbSizerEditSessionOK );
	sdbSizerEditSessionApply = new wxButton( panelEditSession, wxID_APPLY );
	sdbSizerEditSession->AddButton( sdbSizerEditSessionApply );
	sdbSizerEditSessionCancel = new wxButton( panelEditSession, wxID_CANCEL );
	sdbSizerEditSession->AddButton( sdbSizerEditSessionCancel );
	sdbSizerEditSession->Realize();

	gbSizerEditSession->Add( sdbSizerEditSession, wxGBPosition( 14, 1 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 10 );

	buttonDefault = new wxButton( panelEditSession, wxID_ANY, wxT("Default Values"), wxDefaultPosition, wxSize( -1,-1 ), 0 );
	gbSizerEditSession->Add( buttonDefault, wxGBPosition( 14, 0 ), wxGBSpan( 1, 1 ), wxLEFT|wxTOP, 10 );


	panelEditSession->SetSizer( gbSizerEditSession );
	panelEditSession->Layout();
	gbSizerEditSession->Fit( panelEditSession );
	bSizerEditSession->Add( panelEditSession, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerEditSession );
	this->Layout();
	bSizerEditSession->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	checkBoxEnableSystemMessage->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableSystemMessage ), NULL, this );
	textCtrlSystemMessage->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DialogEditSessionView::OnTextChanged ), NULL, this );
	checkBoxEnableTemplate->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTemplate ), NULL, this );
	textCtrlTemplate->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DialogEditSessionView::OnTextChanged ), NULL, this );
	checkBoxEnableSeed->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableSeed ), NULL, this );
	spinCtrlSeed->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTemperature->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTemperature ), NULL, this );
	spinCtrlDoubleTemperature->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableContextSize->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableContextSize ), NULL, this );
	spinCtrlContextSize->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnablePredictTokens->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePredictTokens ), NULL, this );
	spinCtrlPredictTokens->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableRepeatLast->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableRepeatLast ), NULL, this );
	spinCtrlRepeatLast->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableRepeatPenalty->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableRepeatPenalty ), NULL, this );
	spinCtrlDoubleRepeatPenalty->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableTopK->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTopK ), NULL, this );
	spinCtrlTopK->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTopP->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTopP ), NULL, this );
	spinCtrlDoubleTopP->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableTFSZ->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTFSZ ), NULL, this );
	spinCtrlDoubleTFSZ->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableMirostat->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostat ), NULL, this );
	choiceMirostat->Connect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DialogEditSessionView::OnChoiceChanged ), NULL, this );
	checkBoxEnableMirostatTau->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostatTau ), NULL, this );
	spinCtrlDoubleMirostatTau->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableMirostatEta->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostatEta ), NULL, this );
	spinCtrlDoubleMirostatEta->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableKeepTokens->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableKeepTokens ), NULL, this );
	spinCtrlKeepTokens->Connect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTypicalP->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTypicalP ), NULL, this );
	spinCtrlDoubleTypicalP->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChangedP ), NULL, this );
	checkBoxEnablePenalizeNewlines->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePenalizeNewlines ), NULL, this );
	checkBoxPenalizeNewlines->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxChanged ), NULL, this );
	checkBoxEnablePresencePenalty->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePresencePenalty ), NULL, this );
	spinCtrlDoublePresencePenalty->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableFrequencyPenalty->Connect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableFrequencyPenalty ), NULL, this );
	spinCtrlDoubleFrequencyPenalty->Connect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	sdbSizerEditSessionApply->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionApplyClick ), NULL, this );
	sdbSizerEditSessionCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionCancelClick ), NULL, this );
	sdbSizerEditSessionOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionOKClick ), NULL, this );
	buttonDefault->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionDefaultClick ), NULL, this );
}

DialogEditSessionView::~DialogEditSessionView()
{
	// Disconnect Events
	checkBoxEnableSystemMessage->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableSystemMessage ), NULL, this );
	textCtrlSystemMessage->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DialogEditSessionView::OnTextChanged ), NULL, this );
	checkBoxEnableTemplate->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTemplate ), NULL, this );
	textCtrlTemplate->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DialogEditSessionView::OnTextChanged ), NULL, this );
	checkBoxEnableSeed->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableSeed ), NULL, this );
	spinCtrlSeed->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTemperature->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTemperature ), NULL, this );
	spinCtrlDoubleTemperature->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableContextSize->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableContextSize ), NULL, this );
	spinCtrlContextSize->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnablePredictTokens->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePredictTokens ), NULL, this );
	spinCtrlPredictTokens->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableRepeatLast->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableRepeatLast ), NULL, this );
	spinCtrlRepeatLast->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableRepeatPenalty->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableRepeatPenalty ), NULL, this );
	spinCtrlDoubleRepeatPenalty->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableTopK->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTopK ), NULL, this );
	spinCtrlTopK->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTopP->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTopP ), NULL, this );
	spinCtrlDoubleTopP->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableTFSZ->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTFSZ ), NULL, this );
	spinCtrlDoubleTFSZ->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableMirostat->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostat ), NULL, this );
	choiceMirostat->Disconnect( wxEVT_COMMAND_CHOICE_SELECTED, wxCommandEventHandler( DialogEditSessionView::OnChoiceChanged ), NULL, this );
	checkBoxEnableMirostatTau->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostatTau ), NULL, this );
	spinCtrlDoubleMirostatTau->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableMirostatEta->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableMirostatEta ), NULL, this );
	spinCtrlDoubleMirostatEta->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableKeepTokens->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableKeepTokens ), NULL, this );
	spinCtrlKeepTokens->Disconnect( wxEVT_COMMAND_SPINCTRL_UPDATED, wxSpinEventHandler( DialogEditSessionView::OnSpinCtrlChanged ), NULL, this );
	checkBoxEnableTypicalP->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableTypicalP ), NULL, this );
	spinCtrlDoubleTypicalP->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChangedP ), NULL, this );
	checkBoxEnablePenalizeNewlines->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePenalizeNewlines ), NULL, this );
	checkBoxPenalizeNewlines->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxChanged ), NULL, this );
	checkBoxEnablePresencePenalty->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnablePresencePenalty ), NULL, this );
	spinCtrlDoublePresencePenalty->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	checkBoxEnableFrequencyPenalty->Disconnect( wxEVT_COMMAND_CHECKBOX_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnCheckBoxEnableFrequencyPenalty ), NULL, this );
	spinCtrlDoubleFrequencyPenalty->Disconnect( wxEVT_COMMAND_SPINCTRLDOUBLE_UPDATED, wxSpinDoubleEventHandler( DialogEditSessionView::OnSpinCtrlDoubleChanged ), NULL, this );
	sdbSizerEditSessionApply->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionApplyClick ), NULL, this );
	sdbSizerEditSessionCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionCancelClick ), NULL, this );
	sdbSizerEditSessionOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionOKClick ), NULL, this );
	buttonDefault->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogEditSessionView::OnEditSessionDefaultClick ), NULL, this );

}

DialogModelDownloadView::DialogModelDownloadView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerModelDownload;
	bSizerModelDownload = new wxBoxSizer( wxVERTICAL );

	panelModelDownload = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerModelDownload;
	gbSizerModelDownload = new wxGridBagSizer( 0, 0 );
	gbSizerModelDownload->SetFlexibleDirection( wxBOTH );
	gbSizerModelDownload->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextModelDownload = new wxStaticText( panelModelDownload, wxID_ANY, wxT("Please wait while for the download to finish..."), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextModelDownload->Wrap( -1 );
	gbSizerModelDownload->Add( staticTextModelDownload, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	gaugeModelDownload = new wxGauge( panelModelDownload, wxID_ANY, 100, wxDefaultPosition, wxSize( -1,-1 ), wxGA_HORIZONTAL );
	gaugeModelDownload->SetValue( 0 );
	gbSizerModelDownload->Add( gaugeModelDownload, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 10 );


	panelModelDownload->SetSizer( gbSizerModelDownload );
	panelModelDownload->Layout();
	gbSizerModelDownload->Fit( panelModelDownload );
	bSizerModelDownload->Add( panelModelDownload, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerModelDownload );
	this->Layout();
	bSizerModelDownload->Fit( this );
	timerModelDownload.SetOwner( this, wxID_ANY );

	this->Centre( wxBOTH );

	// Connect Events
	this->Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( DialogModelDownloadView::OnTimerModelDownload ) );
}

DialogModelDownloadView::~DialogModelDownloadView()
{
	// Disconnect Events
	this->Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler( DialogModelDownloadView::OnTimerModelDownload ) );

}

DialogSaveConversationView::DialogSaveConversationView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerSaveConversation;
	bSizerSaveConversation = new wxBoxSizer( wxVERTICAL );

	panelSaveConversation = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerSaveConversation;
	gbSizerSaveConversation = new wxGridBagSizer( 0, 0 );
	gbSizerSaveConversation->SetFlexibleDirection( wxBOTH );
	gbSizerSaveConversation->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextFilename = new wxStaticText( panelSaveConversation, wxID_ANY, wxT("Filename:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFilename->Wrap( -1 );
	gbSizerSaveConversation->Add( staticTextFilename, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlFilename = new wxTextCtrl( panelSaveConversation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	textCtrlFilename->SetToolTip( wxT("Server or machine where the Ollama container is deployed or where the Ollama Web UI is accessed.") );

	gbSizerSaveConversation->Add( textCtrlFilename, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	staticTextConversationName = new wxStaticText( panelSaveConversation, wxID_ANY, wxT("Conversation Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextConversationName->Wrap( -1 );
	gbSizerSaveConversation->Add( staticTextConversationName, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlConversationName = new wxTextCtrl( panelSaveConversation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	textCtrlConversationName->SetToolTip( wxT("Server or machine where the Ollama container is deployed or where the Ollama Web UI is accessed.") );

	gbSizerSaveConversation->Add( textCtrlConversationName, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	sdbSizerSaveConversation = new wxStdDialogButtonSizer();
	sdbSizerSaveConversationOK = new wxButton( panelSaveConversation, wxID_OK );
	sdbSizerSaveConversation->AddButton( sdbSizerSaveConversationOK );
	sdbSizerSaveConversationCancel = new wxButton( panelSaveConversation, wxID_CANCEL );
	sdbSizerSaveConversation->AddButton( sdbSizerSaveConversationCancel );
	sdbSizerSaveConversation->Realize();

	gbSizerSaveConversation->Add( sdbSizerSaveConversation, wxGBPosition( 3, 0 ), wxGBSpan( 1, 2 ), wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 10 );


	panelSaveConversation->SetSizer( gbSizerSaveConversation );
	panelSaveConversation->Layout();
	gbSizerSaveConversation->Fit( panelSaveConversation );
	bSizerSaveConversation->Add( panelSaveConversation, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerSaveConversation );
	this->Layout();
	bSizerSaveConversation->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	sdbSizerSaveConversationCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogSaveConversationView::OnSaveConversationCancelClick ), NULL, this );
	sdbSizerSaveConversationOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogSaveConversationView::OnSaveConversationOKClick ), NULL, this );
}

DialogSaveConversationView::~DialogSaveConversationView()
{
	// Disconnect Events
	sdbSizerSaveConversationCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogSaveConversationView::OnSaveConversationCancelClick ), NULL, this );
	sdbSizerSaveConversationOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogSaveConversationView::OnSaveConversationOKClick ), NULL, this );

}

DialogRenameConversationView::DialogRenameConversationView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerRenameConversation;
	bSizerRenameConversation = new wxBoxSizer( wxVERTICAL );

	panelRenameConversation = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerRenameConversation;
	gbSizerRenameConversation = new wxGridBagSizer( 0, 0 );
	gbSizerRenameConversation->SetFlexibleDirection( wxBOTH );
	gbSizerRenameConversation->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	staticTextFilename = new wxStaticText( panelRenameConversation, wxID_ANY, wxT("Filename:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextFilename->Wrap( -1 );
	gbSizerRenameConversation->Add( staticTextFilename, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlFilename = new wxTextCtrl( panelRenameConversation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	textCtrlFilename->SetToolTip( wxT("Server or machine where the Ollama container is deployed or where the Ollama Web UI is accessed.") );

	gbSizerRenameConversation->Add( textCtrlFilename, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	staticTextConversationName = new wxStaticText( panelRenameConversation, wxID_ANY, wxT("Conversation Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextConversationName->Wrap( -1 );
	gbSizerRenameConversation->Add( staticTextConversationName, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_VERTICAL|wxALIGN_RIGHT|wxALL, 5 );

	textCtrlConversationName = new wxTextCtrl( panelRenameConversation, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 200,-1 ), 0 );
	textCtrlConversationName->SetToolTip( wxT("Server or machine where the Ollama container is deployed or where the Ollama Web UI is accessed.") );

	gbSizerRenameConversation->Add( textCtrlConversationName, wxGBPosition( 1, 1 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	sdbSizerRenameConversation = new wxStdDialogButtonSizer();
	sdbSizerRenameConversationOK = new wxButton( panelRenameConversation, wxID_OK );
	sdbSizerRenameConversation->AddButton( sdbSizerRenameConversationOK );
	sdbSizerRenameConversationCancel = new wxButton( panelRenameConversation, wxID_CANCEL );
	sdbSizerRenameConversation->AddButton( sdbSizerRenameConversationCancel );
	sdbSizerRenameConversation->Realize();

	gbSizerRenameConversation->Add( sdbSizerRenameConversation, wxGBPosition( 3, 0 ), wxGBSpan( 1, 2 ), wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 10 );


	panelRenameConversation->SetSizer( gbSizerRenameConversation );
	panelRenameConversation->Layout();
	gbSizerRenameConversation->Fit( panelRenameConversation );
	bSizerRenameConversation->Add( panelRenameConversation, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerRenameConversation );
	this->Layout();
	bSizerRenameConversation->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	sdbSizerRenameConversationCancel->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogRenameConversationView::OnRenameConversationCancelClick ), NULL, this );
	sdbSizerRenameConversationOK->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogRenameConversationView::OnRenameConversationOKClick ), NULL, this );
}

DialogRenameConversationView::~DialogRenameConversationView()
{
	// Disconnect Events
	sdbSizerRenameConversationCancel->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogRenameConversationView::OnRenameConversationCancelClick ), NULL, this );
	sdbSizerRenameConversationOK->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( DialogRenameConversationView::OnRenameConversationOKClick ), NULL, this );

}

DialogAboutView::DialogAboutView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* bSizerAbout;
	bSizerAbout = new wxBoxSizer( wxVERTICAL );

	panelAbout = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxGridBagSizer* gbSizerAbout;
	gbSizerAbout = new wxGridBagSizer( 0, 0 );
	gbSizerAbout->SetFlexibleDirection( wxBOTH );
	gbSizerAbout->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	wxGridBagSizer* gbSizerAboutBanner;
	gbSizerAboutBanner = new wxGridBagSizer( 0, 0 );
	gbSizerAboutBanner->SetFlexibleDirection( wxBOTH );
	gbSizerAboutBanner->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	bitmapLogo = new wxStaticBitmap( panelAbout, wxID_ANY, wxBitmap( wxT("res/images/logo2.png"), wxBITMAP_TYPE_ANY ), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizerAboutBanner->Add( bitmapLogo, wxGBPosition( 0, 0 ), wxGBSpan( 2, 1 ), wxALL, 20 );

	staticTextTitle = new wxStaticText( panelAbout, wxID_ANY, wxT("       <b><big>Ollamawx</big></b>\n\n<tt>by luckymouse0</tt>"), wxDefaultPosition, wxDefaultSize, 0 );
	staticTextTitle->SetLabelMarkup( wxT("       <b><big>Ollamawx</big></b>\n\n<tt>by luckymouse0</tt>") );
	staticTextTitle->Wrap( -1 );
	gbSizerAboutBanner->Add( staticTextTitle, wxGBPosition( 0, 1 ), wxGBSpan( 1, 1 ), wxLEFT|wxTOP, 65 );


	gbSizerAbout->Add( gbSizerAboutBanner, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	staticlineAbout = new wxStaticLine( panelAbout, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	gbSizerAbout->Add( staticlineAbout, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxEXPAND | wxALL, 5 );

	wxGridBagSizer* gbSizerAboutInformation;
	gbSizerAboutInformation = new wxGridBagSizer( 0, 0 );
	gbSizerAboutInformation->SetFlexibleDirection( wxBOTH );
	gbSizerAboutInformation->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	notebookAbout = new wxNotebook( panelAbout, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );

	gbSizerAboutInformation->Add( notebookAbout, wxGBPosition( 0, 0 ), wxGBSpan( 1, 1 ), wxEXPAND | wxALL, 5 );

	textCtrlAbout = new wxTextCtrl( panelAbout, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 400,300 ), wxTE_AUTO_URL|wxTE_MULTILINE|wxTE_READONLY );
	gbSizerAboutInformation->Add( textCtrlAbout, wxGBPosition( 1, 0 ), wxGBSpan( 1, 1 ), wxALL|wxEXPAND, 5 );


	gbSizerAbout->Add( gbSizerAboutInformation, wxGBPosition( 2, 0 ), wxGBSpan( 1, 1 ), wxEXPAND, 5 );

	sdbSizerAbout = new wxStdDialogButtonSizer();
	sdbSizerAboutOK = new wxButton( panelAbout, wxID_OK );
	sdbSizerAbout->AddButton( sdbSizerAboutOK );
	sdbSizerAbout->Realize();

	gbSizerAbout->Add( sdbSizerAbout, wxGBPosition( 3, 0 ), wxGBSpan( 1, 1 ), wxALIGN_CENTER_HORIZONTAL|wxEXPAND|wxTOP, 10 );


	panelAbout->SetSizer( gbSizerAbout );
	panelAbout->Layout();
	gbSizerAbout->Fit( panelAbout );
	bSizerAbout->Add( panelAbout, 1, wxEXPAND | wxALL, 5 );


	this->SetSizer( bSizerAbout );
	this->Layout();
	bSizerAbout->Fit( this );

	this->Centre( wxBOTH );

	// Connect Events
	notebookAbout->Connect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( DialogAboutView::OnNotebookAboutPageChanged ), NULL, this );
	textCtrlAbout->Connect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( DialogAboutView::OnTextURLAbout ), NULL, this );
}

DialogAboutView::~DialogAboutView()
{
	// Disconnect Events
	notebookAbout->Disconnect( wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED, wxNotebookEventHandler( DialogAboutView::OnNotebookAboutPageChanged ), NULL, this );
	textCtrlAbout->Disconnect( wxEVT_COMMAND_TEXT_URL, wxTextUrlEventHandler( DialogAboutView::OnTextURLAbout ), NULL, this );

}
