/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FileUtils.h"

#include <stdexcept>
#include <filesystem>

#include <wx/filename.h>
#include <wx/stdpaths.h>
#include <wx/file.h>
#include <wx/filefn.h>

namespace FileUtils
{
    wxString GetExecutablePath()
    {
        return wxFileName(wxStandardPaths::Get().GetExecutablePath()).GetPath();
    }

    wxString GetFilePathDirectory(const wxString& path)
    {
        return wxFileName(path).GetPath();
    }

    bool CheckFileExists(const wxString& path)
    {
        return wxFileName(path).FileExists();
    }

    bool CheckDirectoryExists(const wxString& path)
    {
        return wxDirExists(path);
    }

    wxMemoryBuffer ReadFileToBuffer(const wxString& filePath)
    {
        if (CheckFileExists(filePath))
        {
            wxFile file(filePath);
            if (!file.IsOpened())
            {
                throw std::runtime_error("Error reading file: " + filePath.ToStdString());
            }

            wxFileOffset fileSize = file.Length();
            wxMemoryBuffer buffer;
            file.Read(buffer.GetWriteBuf(fileSize), fileSize);
            buffer.UngetWriteBuf(fileSize);

            return buffer;
        }
        else
        {
            throw std::runtime_error("Error file does not exists: " + filePath.ToStdString());
        }
    }

    void CreateDirectory(const wxString& dirPath)
    {
        if (!dirPath.empty() && !wxDirExists(dirPath))
        {
            std::filesystem::create_directories(dirPath.ToStdString());
        }
    }
};
