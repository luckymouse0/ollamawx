/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "FrameMain.h"
#include "Network.h"
#include "DialogServerSettings.h"
#include "DialogAbout.h"
#include "DialogModelDownload.h"
#include "DialogModelInformation.h"
#include "DialogSaveConversation.h"
#include "DialogEditSession.h"
#include "DialogRenameConversation.h"
#include "ImageUtils.h"
#include "FileUtils.h"
#include "Base64.h"

#include <stdexcept>
#include <filesystem>
#include <fstream>

#include <nlohmann/json.hpp>
#include <wx/msgdlg.h>
#include <wx/log.h>
#include <wx/utils.h>
#include <wx/gdicmn.h>
#include <wx/filedlg.h>

///////////////////////////////////////////////////////////////////////////

FrameMain::FrameMain(wxWindow* parent) : FrameMainView(parent), serverThread(nullptr), chat(nullptr), modelParams(nullptr)
{
    exePath = FileUtils::GetExecutablePath();

    SetIcon(ImageUtils::LoadIcon(exePath + "/res/icons/icon.gif"));

    SetServerStatus(false);

    LoadServerSettings();

    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &FrameMain::OnStartThread, this);
    richTextChatBox->Bind(wxEVT_TEXT_URL, &FrameMain::OnURLClick, this);
}

FrameMain::~FrameMain()
{
    StopOllamaServer();

    if (chat)
    {
        delete chat;
    }
}

void FrameMain::OnClose(wxCloseEvent& event)
{
    StopOllamaServer();

    event.Skip();
}

void FrameMain::OnStartClick(wxCommandEvent& event)
{
    StartOllamaServer();
}

void FrameMain::OnStopClick(wxCommandEvent& event)
{
    StopOllamaServer();
}

void FrameMain::OnStartThread(wxCommandEvent& event)
{
    wxString command = event.GetString();

    std::lock_guard<std::mutex> lock(mutex);

    wxProcess* process = new wxProcess(this);
    process->Redirect();
    wxLogDebug("Starting process with command: %s.", command.ToStdString());

    long pid = wxExecute(command, wxEXEC_ASYNC, process);
    if (pid != 0)
    {
        if (serverThread)
        {
            serverThread->SetPID(pid);
            serverThread->SetProcess(process);
        }

        SetServerStatus(true);
        bitmapStatus->SetToolTip(bitmapStatus->GetToolTipText() + " - pid: " + wxString::Format(wxT("%ld"), pid));
        EnableModelControls(true);
        wxLogDebug("Server with PID: %ld started.", pid);
    }
    else
    {
        wxLogError("Failed to execute command: %s.", command.ToStdString());
        delete process;
    }
}

void FrameMain::OnThreadEvent(wxThreadEvent& event)
{
    if (serverThread && serverThread->GetPendingDelete())
    {
        delete serverThread->GetProcess();
        serverThread->SetProcess(nullptr);
        serverThread->SetPendingDelete(false);
    }
}

void FrameMain::OnSettingsClick(wxCommandEvent& event)
{
    DialogServerSettings dialogServerSettings(this, &serverSettings);

    if (dialogServerSettings.ShowModal() == wxID_OK)
    {
    }
}

void FrameMain::OnHelpClick(wxCommandEvent& event)
{
    wxPoint pos = bpButtonHelp->GetPosition();
    pos.x += menuHelp->GetMenuItemCount() * 40;
    panelMain->PopupMenu(menuHelp, pos);
}

void FrameMain::OnAboutMenuSelection(wxCommandEvent& event)
{
    DialogAbout dialogAbout(this);

    if (dialogAbout.ShowModal() == wxID_OK)
    {
    }
}

void FrameMain::OnUpdateModelListClick(wxCommandEvent& event)
{
    try
    {
        wxLogDebug("Updating model list...");

        wxLogDebug("Sending HTTP GET - Host: %s Request: /api/tags.", serverSettings.GetHost());
        Network& network = Network::GetInstance();
        std::string response = network.HTTPGet(serverSettings.GetHost(), "/api/tags");
        wxLogDebug("HTTP GET Response: %s", response);

        nlohmann::json data = nlohmann::json::parse(response);
        const nlohmann::json& models = data["models"];

        wxString selectedModel = bcomboBoxModel->GetStringSelection();
        bcomboBoxModel->Clear();
        for (const auto& model : models)
        {
            bcomboBoxModel->Append(model["name"].get<std::string>());
        }

        int index = bcomboBoxModel->FindString(selectedModel);
        if (index != wxNOT_FOUND)
        {
            bcomboBoxModel->SetSelection(index);
        }

        wxLogDebug("Model list updated.");
    }
    catch (const std::exception& e)
    {
        std::string errorMsg = e.what();
        if (errorMsg.find("status: 0") != std::string::npos)
        {
           wxLogError("HTTP GET Error: Server is starting up, please wait a few moments.");
        }
        else
        {
           wxLogError("HTTP GET Error: %s", errorMsg);
        }
    }
}

void FrameMain::OnRunModelClick(wxCommandEvent& event)
{
    wxString modelName = bcomboBoxModel->GetValue();
    if (!modelName.IsEmpty())
    {
        if (bcomboBoxModel->FindString(modelName) == wxNOT_FOUND)
        {
            if (wxMessageBox(wxT("The selected model is not in the list: \n\n Model name: " + std::string(modelName.mb_str()) + "\n\nDo you want to download it?"), wxT("Download Model"), wxYES_NO | wxICON_QUESTION) == wxYES)
            {
                DialogModelDownload dialogModelDownload(this, serverSettings.GetHost(), modelName);

                if (dialogModelDownload.ShowModal() == wxID_OK)
                {
                    wxMessageBox(wxT("Model downloaded successfully."), wxT("Model download correctly."), wxICON_INFORMATION);

                    bcomboBoxModel->Append(modelName);
                }
                else
                {
                    wxLogDebug("Model could not be downloaded.");

                    return;
                }
            }
            else
            {
                wxLogDebug("User declined to download the model.");

                return;
            }
        }

        nlohmann::json requestPayload = {
            {"model", std::string(modelName.mb_str())},
            {"stream", false},
            {"keep_alive", serverSettings.GetKeepAliveMinutesString()}
        };

        try
        {
            wxLogDebug("Loading model...");

            wxLogDebug("Sending HTTP POST - Host: %s Request: /api/generate Model: %s.", serverSettings.GetHost(), std::string(modelName.mb_str()));
            Network& network = Network::GetInstance();
            std::string response = network.HTTPPost(serverSettings.GetHost(), "/api/generate", requestPayload.dump(), "application/json");
            wxLogDebug("HTTP POST Response: %s", response);

            nlohmann::json j = nlohmann::json::parse(response);
            if (j.contains("done") && !j["done"].is_null() && j["done"].get<bool>())
            {
                wxLogDebug("Model loaded.");
                loadedModel = modelName;
                bcomboBoxModel->SetItemBitmap(bcomboBoxModel->GetSelection(), ImageUtils::LoadBitmap("res/buttons/connected.png", 10, 10));
                InitializeModelParams();
                StartChat();
            }
            else
            {
                wxLogError("Error loading the model.");
                wxMessageBox(wxT("Ollama server could not load the model."), wxT("Error loading the model!"), wxICON_ERROR);
            }
        }
        catch (const std::exception& e)
        {
            wxLogError("HTTP POST Error: %s", e.what());
        }
    }
    else
    {
        wxMessageBox(wxT("Select a model before running."), wxT("No model selected!"), wxICON_WARNING);
    }
}

void FrameMain::OnShowInfoModelClick(wxCommandEvent& event)
{
    wxString modelName = bcomboBoxModel->GetStringSelection();
    if (!modelName.IsEmpty())
    {
        DialogModelInformation dialogModelInformation(this, serverSettings.GetHost(), modelName);

        if (dialogModelInformation.ShowModal() == wxID_OK)
        {
        }
    }
}

void FrameMain::OnCloseConversationClick(wxCommandEvent& event)
{
    if (!loadedModel.IsEmpty())
    {
        if (wxMessageBox(wxT("Do you want to close the conversation without saving?"), wxT("Save conversation"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            CloseConversation();
        }
    }
}

void FrameMain::OnSaveCloseConversationClick(wxCommandEvent& event)
{
    if (SaveConversation())
    {
        CloseConversation();
    }
}

void FrameMain::OnSaveConversationClick(wxCommandEvent& event)
{
    SaveConversation();
}

void FrameMain::OnDeleteModelClick(wxCommandEvent& event)
{
    wxString modelName = bcomboBoxModel->GetStringSelection();
    if (!modelName.IsEmpty())
    {
        if (wxMessageBox(wxT("Are you sure you want to delete the selected model?\n\n Model name: " + modelName), wxT("Delete Model"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            nlohmann::json requestPayload = {
                {"model", std::string(modelName.mb_str())}
            };

            try
            {
                wxLogDebug("Deleting model...");

                wxLogDebug("Sending HTTP DELETE - Host: %s Request: /api/delete Model: %s.", serverSettings.GetHost(), std::string(modelName.mb_str()));

                Network& network = Network::GetInstance();
                network.HTTPDelete(serverSettings.GetHost(), "/api/delete", requestPayload.dump(), "application/json");

                wxLogDebug("Model deleted successfully.");
                wxMessageBox(wxT("Model deleted successfully."), wxT("Model deleted."), wxICON_INFORMATION);

                bcomboBoxModel->Delete(bcomboBoxModel->GetSelection());
                bcomboBoxModel->SetSelection(wxNOT_FOUND);
            }
            catch (const std::exception& e)
            {
                wxLogError("HTTP DELETE Error: %s", e.what());
            }
        }
        else
        {
            wxLogDebug("User declined to delete the model.");
        }
    }
    else
    {
        wxMessageBox(wxT("Please select a model to delete."), wxT("No model selected!"), wxICON_WARNING);
    }
}

void FrameMain::OnEditSessionClick(wxCommandEvent& event)
{
    DialogEditSession dialogEditSession(this, modelParams, std::string(loadedSystemMessage.mb_str()), std::string(loadedTemplate.mb_str()));

    if (dialogEditSession.ShowModal() == wxID_OK)
    {
    }
}

void FrameMain::OnClearConversationClick(wxCommandEvent& event)
{
    if (wxMessageBox(wxT("Do you want to clear the chat and erase the chat history?\n\nThis action cannot be undone!"), wxT("Clear conversation"), wxYES_NO | wxICON_WARNING) == wxYES)
    {
        ClearChatBox();
        chat->ClearConversation();
        textCtrlChatInput->SetFocus();
    }
}

void FrameMain::OnSendClick(wxCommandEvent& event)
{
    try
    {
        if (chat->IsReady())
        {
            wxString userMessage = textCtrlChatInput->GetValue();
            if (!userMessage.empty())
            {
                chat->SendMessage(userMessage);
                textCtrlChatInput->Clear();
                bpButtonAttach->SetBitmap(ImageUtils::LoadBitmap(exePath + "/res/buttons/attach.png"));
            }
        }
    }
    catch (const std::exception& e)
    {
        wxLogError("Failed to send the message: %s", e.what());
    }
}

void FrameMain::OnAttachClick(wxCommandEvent& event)
{
    wxPoint pos = bpButtonAttach->GetPosition();
    pos.y -= menuAttach->GetMenuItemCount() * 40;
    panelMain->PopupMenu(menuAttach, pos);
}

void FrameMain::OnAttachImage(wxCommandEvent& event)
{
    wxFileDialog openFileDialog(this, "Open Image file", "", "", "Image Files (*.jpg;*.png;*.gif;*.bmp)|*.jpg;*.png;*.gif;*.bmp", wxFD_OPEN | wxFD_FILE_MUST_EXIST);

    if (openFileDialog.ShowModal() != wxID_CANCEL)
    {
        wxString filePath = openFileDialog.GetPath();

        wxLogDebug("Attaching file to conversation: %s", filePath);
        chat->AttachImage(filePath);

        if (chat->HasImage())
        {
            bpButtonAttach->SetBitmap(ImageUtils::ResizeBitmap(Base64::Decode(chat->GetEncodedImage()), 20, 20));
        }
        else
        {
            bpButtonAttach->SetBitmap(ImageUtils::LoadBitmap(exePath + "/res/buttons/attach-image-error.png"));
        }
    }
}

void FrameMain::OnChatInputKeyDown(wxKeyEvent& event)
{
    if (event.GetKeyCode() == WXK_RETURN && event.ShiftDown())
    {
        wxCommandEvent e;
        OnSendClick(e);
    }
    else if (event.GetKeyCode() == WXK_UP && textCtrlChatInput->GetValue().IsEmpty())
    {
        wxString lastMessage = chat->GetLastUserMessage();
        if (!lastMessage.IsEmpty())
        {
            textCtrlChatInput->SetValue(lastMessage);
            textCtrlChatInput->SetInsertionPointEnd();
        }
        else
        {
            event.Skip();
        }
    }
    else
    {
        event.Skip();
    }
}

void FrameMain::OnLoadConversationClick(wxCommandEvent& event)
{
    wxString selectedConversation = listBoxConversation->GetStringSelection();

    if (!selectedConversation.IsEmpty())
    {
        if (!richTextChatBox->IsEmpty())
        {
            if (wxMessageBox(wxT("Conversation is not empty. Do you want to overwrite it?"), wxT("Overwrite conversation"), wxYES_NO | wxICON_QUESTION) == wxNO)
            {
                return;
            }
        }

        chat->SetFilePath(conversations[selectedConversation]);
        ClearChatBox();
        chat->LoadConversation();
    }
}

void FrameMain::OnRenameConversationClick(wxCommandEvent& event)
{
    wxString selectedConversation = listBoxConversation->GetStringSelection();

    if (!selectedConversation.IsEmpty())
    {
        wxString conversationName = selectedConversation;
        wxString filePath = conversations[selectedConversation];

        DialogRenameConversation dialogRenameConversation(this, &filePath, &conversationName);

        if (dialogRenameConversation.ShowModal() == wxID_OK)
        {
            int index = listBoxConversation->GetSelection();
            listBoxConversation->Delete(index);
            listBoxConversation->Insert(conversationName, index);

            auto it = conversations.find(selectedConversation);
            if (it != conversations.end())
            {
                conversations.erase(it->first);
                conversations.insert({ conversationName, filePath });
            }
        }
    }
}

void FrameMain::OnDeleteConversationClick(wxCommandEvent& event)
{
    wxString selectedConversation = listBoxConversation->GetStringSelection();

    if (!selectedConversation.IsEmpty())
    {
        wxString filePath = conversations[selectedConversation];

        if (wxMessageBox(wxT("Are you sure you want to delete this conversation?"), wxT("Delete conversation"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            try
            {
                if (std::filesystem::remove(filePath.ToUTF8().data()))
                {
                    conversations.erase(selectedConversation);
                    listBoxConversation->Delete(listBoxConversation->GetSelection());

                    wxLogDebug("Conversation deleted successfully.");
                }
                else
                {
                    wxLogError("File not found or unable to delete.");
                }
            }
            catch (const std::filesystem::filesystem_error& e)
            {
                wxLogError("Unable to delete the file: %s.", e.what());
            }
        }
    }
}

void FrameMain::OnSearchFilter(wxCommandEvent& event)
{
    wxString searchQuery = searchCtrlFilter->GetValue().Lower();
    listBoxConversation->Clear();

    for (const auto& conversation : conversations)
    {
        if (conversation.first.Lower().Contains(searchQuery))
        {
            listBoxConversation->Append(conversation.first);
        }
    }
}

void FrameMain::OnURLClick(wxTextUrlEvent& event)
{
    wxTextAttr style;

    if (richTextChatBox->GetStyle(event.GetURLStart(), style))
    {
        wxString url = style.GetURL();

        if (!url.IsEmpty())
        {
            if (!wxLaunchDefaultBrowser(url))
            {
                wxLogWarning("Could not open the URL: %s", url);
            }
        }
        else
        {
            wxLogWarning("No URL found at the clicked position.");
        }
    }
    else
    {
        wxLogWarning("Failed to get style at the clicked position.");
    }
}

void FrameMain::SetServerStatus(bool isRunning)
{
    if (serverStatus != isRunning)
    {
        serverStatus = isRunning;
        bpButtonStart->Enable(!isRunning);
        bpButtonStop->Enable(isRunning);

        wxString tooltipText = "Ollama server status: ";
        tooltipText = tooltipText.substr(0, tooltipText.find_last_of(' ') + 1);

        if (isRunning)
        {
            bitmapStatus->SetBitmap(ImageUtils::LoadBitmap("res/buttons/connected.png"));
            tooltipText += "CONNECTED";
        }
        else
        {
            bitmapStatus->SetBitmap(ImageUtils::LoadBitmap("res/buttons/disconnected.png"));
            tooltipText += "DISCONNECTED";
        }

        bitmapStatus->SetToolTip(tooltipText);
    }
}

void FrameMain::LoadServerSettings()
{
    try
    {
        serverSettings.Load(exePath.ToStdString() + "/config/server.json");
        wxLogDebug("Server setting loaded: %s", serverSettings.GetConfigPath());
    }
    catch (const std::exception& e)
    {
        wxLogError("Cannot load server settings: %s", e.what());
    }
}

void FrameMain::StartOllamaServer()
{
    if (!serverThread)
    {
        if(FileUtils::CheckFileExists(serverSettings.GetPath()))
        {
            serverThread = new ProcessThread(this);
            serverThread->Start(serverSettings.GetPath() + " serve");
            LoadConversations();
        }
        else
        {
            wxLogDebug("The specified ollama executable was not found. Please check the path and try again.");
            wxMessageBox(wxT("The specified ollama executable was not found. Please check the path and try again."), wxT("Ollama Executable Not Found!"), wxICON_ERROR);
        }
    }
    else
    {
        wxLogDebug("Ollama executable is already running.");
        wxMessageBox(wxT("Ollama executable is already running."), wxT("Ollama Executable is running!"), wxICON_WARNING);
    }
}

void FrameMain::StopOllamaServer()
{
    if (serverThread)
    {
        serverThread->Stop();
        delete serverThread;
        serverThread = nullptr;
        SetServerStatus(false);
        EnableModelControls(false);
        EnableChatControls(false);
        EnableConversationControls(false);
        wxLogDebug("Server thread stopped.");
    }
}

void FrameMain::EnableModelControls(bool isEnabled)
{
    bcomboBoxModel->Enable(isEnabled);
    bpButtonUpdateModelList->Enable(isEnabled);
    bpButtonRunModel->Enable(isEnabled);
    bpButtonShowInfoModel->Enable(isEnabled);
    bpButtonDeleteModel->Enable(isEnabled);
}

void FrameMain::EnableChatControls(bool isEnabled)
{
    textCtrlChatInput->Enable(isEnabled);
    bpButtonCloseConversation->Enable(isEnabled);
    bpButtonSaveCloseConversation->Enable(isEnabled);
    bpButtonSaveConversation->Enable(isEnabled);
    bpButtonEditSession->Enable(isEnabled);
    buttonSend->Enable(isEnabled);
    bpButtonAttach->Enable(isEnabled);
    richTextChatBox->Clear();
    textCtrlChatInput->Clear();
}

void FrameMain::EnableConversationControls(bool isEnabled)
{
    listBoxConversation->Enable(isEnabled);
    bpButtonLoadConversation->Enable(isEnabled);
    bpButtonRenameConversation->Enable(isEnabled);
    bpButtonDeleteConversation->Enable(isEnabled);
    bpButtonClearConversation->Enable(isEnabled);
    searchCtrlFilter->Enable(isEnabled);
    listBoxConversation->Enable(isEnabled);
}

void FrameMain::EnableMenuItem(bool isEnabled, wxMenu* menu, const wxString& label)
{
    wxMenuItem* menuItem = menu->FindItem(menu->FindItem(label));
    if (menuItem)
    {
        menuItem->Enable(isEnabled);
    }
}

void FrameMain::InitializeModelParams()
{
    if (!loadedModel.empty())
    {
        if (modelParams)
        {
            delete modelParams;
        }
        modelParams = new ModelParams;

        nlohmann::json requestPayload = {
            {"name", std::string(loadedModel.mb_str())},
            {"stream", false}
        };

        try
        {
            wxLogDebug("Setting initial model parameters...");

            wxLogDebug("Sending HTTP POST - Host: %s Request: /api/show Model: %s.", serverSettings.GetHost(), std::string(loadedModel.mb_str()));
            Network& network = Network::GetInstance();
            std::string response = network.HTTPPost(serverSettings.GetHost(), "/api/show", requestPayload.dump(), "application/json");

            nlohmann::json modelInfo = nlohmann::json::parse(response);
            modelParams->SystemMessage = modelInfo.contains("system") && !modelInfo["system"].is_null() ? modelInfo["system"].get<std::string>() : "";
            loadedSystemMessage = modelParams->SystemMessage;
            modelParams->Template = modelInfo.contains("template") && !modelInfo["template"].is_null() ? modelInfo["template"].get<std::string>() : "";
            loadedTemplate = modelParams->Template;

            EnableMenuItem(false, menuAttach, "Attach image");
            if (modelInfo.contains("details") && !modelInfo["details"].is_null() && modelInfo["details"].contains("families") && !modelInfo["details"]["families"].is_null())
            {
                for (const std::string& family : modelInfo["details"]["families"].get<std::vector<std::string>>())
                {
                    if (family == "clip")
                    {
                        EnableMenuItem(true, menuAttach, "Attach image");
                        break;
                    }
                }
            }

            wxLogDebug("Model parameters initialized.");
        }
        catch (const std::exception& e)
        {
            wxLogError("HTTP POST Error: %s", e.what());
        }
    }
}

void FrameMain::StartChat()
{
    EnableChatControls(true);
    EnableConversationControls(true);
    richTextChatBox->Clear();
    chat = new Chat(serverSettings, bcomboBoxModel->GetStringSelection(), richTextChatBox, modelParams);
    textCtrlChatInput->SetFocus();
}

void FrameMain::CloseConversation()
{
    nlohmann::json requestPayload = {
        {"model", std::string(loadedModel.mb_str())},
        {"stream", false},
        {"keep_alive", 0}
    };

    try
    {
        wxLogDebug("Unloading model...");

        wxLogDebug("Sending HTTP POST - Host: %s Request: /api/generate Model: %s.", serverSettings.GetHost(), std::string(loadedModel.mb_str()));
        Network& network = Network::GetInstance();
        std::string response = network.HTTPPost(serverSettings.GetHost(), "/api/generate", requestPayload.dump(), "application/json");
        wxLogDebug("HTTP POST Response: %s", response);

        nlohmann::json j = nlohmann::json::parse(response);
        if (j.contains("done") && !j["done"].is_null() && j["done"].get<bool>())
        {
            wxLogDebug("Model unloaded and conversation closed.");
            EnableChatControls(false);
            EnableConversationControls(false);
            loadedModel = wxEmptyString;
            loadedSystemMessage = wxEmptyString;
            loadedTemplate = wxEmptyString;
            loadedConversationFilename = wxEmptyString;
            loadedConversationName = wxEmptyString;
            ClearChatBox();

            wxCommandEvent e;
            OnUpdateModelListClick(e);
        }
        else
        {
            wxLogError("Error unloading the model and closing the conversation.");
            wxMessageBox(wxT("Ollama server could not unload the model."), wxT("Error unloading the model!"), wxICON_ERROR);
        }
    }
    catch (const std::exception& e)
    {
        wxLogError("HTTP POST Error: %s", e.what());
    }
}

bool FrameMain::SaveConversation()
{
    bool correct = false;

    if (!loadedModel.empty())
    {
        bool save = !chat->GetFilePath().empty() && !chat->GetConversationName().empty();

        if (!save && loadedConversationFilename.empty() && loadedConversationName.empty())
        {
            DialogSaveConversation dialogSaveConversation(this, &loadedConversationFilename, &loadedConversationName);

            if (dialogSaveConversation.ShowModal() == wxID_OK)
            {
                save = true;
                wxString filePath = exePath + "/conversations/" + loadedConversationFilename + ".json";
                chat->SetFilePath(filePath);
                chat->SetConversationName(loadedConversationName);

                conversations.emplace(loadedConversationName, filePath);
                listBoxConversation->Append(loadedConversationName);
            }
        }

        if (save)
        {
            chat->SaveConversation();
            correct = true;
        }
    }

    return correct;
}

void FrameMain::LoadConversations()
{
    conversations.clear();
    std::filesystem::path conversationsDir = std::filesystem::u8path((exePath + "/conversations/").ToUTF8().data());

    if (std::filesystem::exists(conversationsDir) && std::filesystem::is_directory(conversationsDir))
    {
        for (const auto& entry : std::filesystem::directory_iterator(conversationsDir))
        {
            if (entry.is_regular_file() && entry.path().extension() == ".json")
            {
                std::ifstream file(entry.path().u8string());
                if (file.is_open())
                {
                    nlohmann::json conversationJson;

                    try
                    {
                        file >> conversationJson;
                        file.close();

                        wxString conversationName = wxString::FromUTF8(conversationJson["Name"].get<std::string>().c_str());
                        wxString filePath = wxString::FromUTF8(entry.path().u8string().c_str());

                        if (!conversationName.IsEmpty() || !filePath.IsEmpty())
                        {
                            conversations.emplace(conversationName, filePath);
                        }
                        else
                        {
                            wxLogError("Error reading file, empty conversation name or file name");
                        }
                    }
                    catch (const std::exception& e)
                    {
                        wxLogError("Error parsing conversation JSON file: %s error: %s", entry.path().string(), e.what());
                        continue;
                    }
                }
            }
        }

        for (const auto& conversation : conversations)
        {
            listBoxConversation->Append(conversation.first);
        }
    }
}

void FrameMain::ClearChatBox()
{
    richTextChatBox->SetFocus();

    long lastPos = richTextChatBox->GetLastPosition();
    richTextChatBox->SetInsertionPoint(lastPos);

    while (richTextChatBox->GetCaretPosition() < lastPos)
    {
        long currentChar = richTextChatBox->GetCaretPosition();
        richTextChatBox->MoveRight(100);

        if (richTextChatBox->GetCaretPosition() == currentChar)
        {
            break;
        }
    }

    richTextChatBox->SelectAll();
    richTextChatBox->DeleteSelection();
}
