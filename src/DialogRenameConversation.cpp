/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogRenameConversation.h"

#include <filesystem>
#include <fstream>
#include <string>
#include <stdexcept>

#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/log.h>

DialogRenameConversation::DialogRenameConversation(wxWindow* parent, wxString* filePath, wxString* conversationName) : DialogRenameConversationView(parent), filePath(filePath), conversationName(conversationName)
{
    filename = wxFileName(*filePath).GetName();
    textCtrlFilename->ChangeValue(filename);
    textCtrlConversationName->ChangeValue(*conversationName);
}

void DialogRenameConversation::OnRenameConversationOKClick(wxCommandEvent& event)
{
    wxString newFilename = textCtrlFilename->GetValue();
    wxString newConversationName = textCtrlConversationName->GetValue();

    if (!ValidateInput(newFilename, newConversationName))
    {
        return;
    }

    nlohmann::json conversationJson;
    if (!LoadConversationJson(conversationJson))
    {
        return;
    }

    conversationJson["Name"] = newConversationName;

    if (!SaveConversationJson(conversationJson))
    {
        return;
    }

    if (filename != newFilename && !RenameFile(newFilename))
    {
        return;
    }

    *conversationName = newConversationName;

    EndModal(wxID_OK);
}

void DialogRenameConversation::OnRenameConversationCancelClick(wxCommandEvent& event)
{
    if (*conversationName != textCtrlConversationName->GetValue() || filename != textCtrlFilename->GetValue())
    {
        if (wxMessageBox(wxT("Do you want to exit without saving?"), wxT("Save conversation"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            EndModal(wxID_CANCEL);
        }
    }
    else
    {
        EndModal(wxID_CANCEL);
    }
}

bool DialogRenameConversation::ValidateInput(const wxString& newFilename, const wxString& newConversationName) const
{
    if (newFilename.IsEmpty() || newConversationName.IsEmpty())
    {
        wxMessageBox(wxT("Fill all the required fields."), wxT("Missing values"), wxOK | wxICON_EXCLAMATION);

        return false;
    }

    if (*conversationName == newConversationName && filename == newFilename)
    {
        wxMessageBox(wxT("The conversation and/or the file name haven't changed. Nothing to rename."), wxT("Wrong values"), wxOK | wxICON_EXCLAMATION);

        return false;
    }

    return true;
}

bool DialogRenameConversation::LoadConversationJson(nlohmann::json& conversationJson) const
{
    std::ifstream fileRead(filePath->ToUTF8().data());
    if (!fileRead.is_open())
    {
        wxLogError("Unable to open file for saving conversation.");

        return false;
    }

    try
    {
        fileRead >> conversationJson;
    }
    catch (const std::exception& e)
    {
        wxLogError("Error reading file: %s", e.what());

        return false;
    }

    return true;
}

bool DialogRenameConversation::SaveConversationJson(const nlohmann::json& conversationJson) const
{
    std::ofstream fileWrite(filePath->ToUTF8().data());
    if (!fileWrite.is_open())
    {
        wxLogError("Unable to open file for saving conversation.");

        return false;
    }

    try
    {
        fileWrite << conversationJson.dump(4);
    }
    catch (const std::exception& e)
    {
        wxLogError("Error writing file: %s", e.what());

        return false;
    }

    return true;
}

bool DialogRenameConversation::RenameFile(const wxString& newFilename)
{
    std::filesystem::path parentPath = std::filesystem::u8path(filePath->ToUTF8().data()).parent_path();
    std::string newFilePath = (parentPath / (newFilename + ".json").ToUTF8().data()).string();

    try
    {
        std::filesystem::rename(filePath->ToUTF8().data(), newFilePath);
        wxLogDebug("File renamed successfully.");
    }
    catch (const std::exception& e)
    {
        wxLogError("Unable to rename the file: %s.", e.what());

        return false;
    }

    *filePath = wxString::FromUTF8(newFilePath.c_str());

    return true;
}
