/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Base64.h"
#include "FileUtils.h"

#include <wx/base64.h>
#include <wx/buffer.h>
#include <wx/mstream.h>
#include <wx/gdicmn.h>
#include <wx/log.h>

wxString Base64::Encode(const wxString& filePath)
{
    try
    {
        return wxBase64Encode(FileUtils::ReadFileToBuffer(filePath));
    }
    catch (...)
    {
        wxLogError("Cannot open file: %s", filePath);

        return wxEmptyString;
    }
}

wxImage Base64::Decode(const wxString& encodedString)
{
    wxMemoryBuffer decodedBuffer = wxBase64Decode(encodedString);
    wxMemoryInputStream inputStream(decodedBuffer.GetData(), decodedBuffer.GetDataLen());
    wxImage image(inputStream, wxBITMAP_TYPE_ANY);

    if (!image.IsOk())
    {
        wxLogError("Failed to decode image from Base64");

        return wxNullImage;
    }

    return image;
}
