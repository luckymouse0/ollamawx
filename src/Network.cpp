/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Network.h"

#include <stdexcept>
#include <thread>
#include <chrono>
#include <sstream>

Network::Network()
{
}

Network& Network::GetInstance() noexcept
{
    static Network instance;

    return instance;
}

void Network::InitializeClient(const std::string& host)
{
    if (this->host != host)
    {
        client = std::make_unique<httplib::Client>(host.c_str());
        this->host = host;
    }

    if (!client)
    {
        throw std::runtime_error("HTTP client initialization failed");
    }
}

std::string Network::HTTPGet(const std::string& host, const std::string& requestPath, int maxRetries, int delayMs)
{
    InitializeClient(host);

    for (int attempt = 1; attempt <= maxRetries; ++attempt)
    {
        try
        {
            auto res = client->Get(requestPath.c_str());

            if (res && res->status == 200)
            {
                return res->body;
            }

            throw std::runtime_error("HTTP GET request failed with status: " + std::to_string(res ? res->status : 0));
        }
        catch (const std::exception& e)
        {
            if (attempt < maxRetries)
            {
                std::cerr << "Attempt " << attempt << " failed: " << e.what() << ". Retrying in " << delayMs / 1000 << " seconds…" << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(delayMs));
            }
            else
            {
                throw;
            }
        }
    }

    throw std::runtime_error("HTTP GET request failed after maximum retries");
}

std::string Network::HTTPPost(const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType, int timeoutSec)
{
    InitializeClient(host);

    if (timeoutSec > 0)
    {
        client->set_connection_timeout(timeoutSec, 0);
        client->set_read_timeout(timeoutSec, 0);
        client->set_write_timeout(timeoutSec, 0);
    }

    auto res = client->Post(requestPath.c_str(), requestPayload.c_str(), contentType.c_str());

    if (res)
    {
        if (res->status == 200 && !res->body.empty())
        {
            return res->body;
        }
        else
        {
            std::cerr << "HTTP POST request failed with status: " << res->status << std::endl;
        }
    }
    else
    {
        std::cerr << "HTTP POST request failed with status: " << res->status << std::endl;
    }

    throw std::runtime_error("HTTP POST request failed with status: " + std::to_string(res ? res->status : 0));
}

void Network::HTTPPostStream(const std::function<void(const std::string&)>& chunkHandler, const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType, int timeoutSec)
{
    InitializeClient(host);

    if (timeoutSec > 0)
    {
        client->set_connection_timeout(timeoutSec, 0);
        client->set_read_timeout(timeoutSec, 0);
    }

    auto res = client->Post(requestPath.c_str(), requestPayload.c_str(), contentType.c_str());

    if (res && res->status == 200)
    {
        std::istringstream responseStream(res->body);
        std::string line;

        while (std::getline(responseStream, line))
        {
            if (line.empty())
            {
                continue;
            }

            try
            {
                chunkHandler(line);
            }
            catch (const std::exception& e)
            {
                std::cerr << "Error processing stream chunk: " << e.what() << std::endl;
            }
        }
    }
    else
    {
        std::cerr << "POST request failed with status: " << res->status << std::endl;
    }
}

void Network::HTTPDelete(const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType)
{
    InitializeClient(host);

    auto res = client->Delete(requestPath.c_str(), requestPayload.c_str(), contentType.c_str());

    if (!res || res->status != 200)
    {
        throw std::runtime_error("HTTP Delete request failed with status: " + std::to_string(res ? res->status : 0));
    }
}
