/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Markdown.h"
#include "Base64.h"
#include "ImageUtils.h"
#include "FileUtils.h"

#include <vector>
#include <utility>
#include <algorithm>

#include <wx/font.h>

namespace
{
    const wxString LINE_BREAK_TAG = "<br>";
    const wxString START_BOLD_TAG = "<strong>";
    const wxString END_BOLD_TAG = "</strong>";
    const wxString START_ITALIC_TAG = "<em>";
    const wxString END_ITALIC_TAG = "</em>";
    const wxString ITALIC_MARKER = "*";
    const wxString BOLD_MARKER = "**";
    const wxString BOLD_ITALIC_MARKER = "***";
    const wxString UNDERLINE_MARKER = "__";
    const wxString START_IMAGE_MARKER = "![";
    const wxString START_LINK_TEXT_MARKER = "[";
    const wxString END_LINK_TEXT_MARKER = "]";
    const wxString START_LINK_URL_MARKER = "(";
    const wxString END_LINK_URL_MARKER = ")";
    const wxString CODE_MARKER = "`";
    const wxString STRIKETHROUGH_MARKER = "~~";
    const wxString START_LIST_MARKER = "\n- ";
    const wxString LIST_MARKER = wxString::FromUTF8("\n● ");
    const wxString HEADING_MARKERS[] = {"# ", "## ", "### ", "#### ", "##### ", "###### "};
    const wxString BLOCKQUOTE_MARKER = "> ";
    const wxString TABLE_COLUMN_MARKER = "|";
    const wxString TABLE_HEADER_MARKER = "---";
    const wxString TABLE_MARKER = "<!-- TABLE -->";
    const wxString CODE_BLOCK_MARKER = "```";
}

wxArrayString Markdown::FilterEmptyElements(const wxArrayString& arrayString)
{
    wxArrayString filteredArrayString;
    for (const wxString& s : arrayString)
    {
        if (!s.IsEmpty())
        {
            filteredArrayString.Add(wxString(s).Trim(false).Trim());
        }
    }

    return filteredArrayString;
}

wxString Markdown::DetectAndMarkTables(const wxString& message)
{
    wxString output;
    wxString currentTable;
    bool inTable = false;
    wxString::size_type pos = 0;

    while (pos < message.Length())
    {
        wxString::size_type lineEnd = message.find('\n', pos);
        if (lineEnd == wxString::npos)
        {
            lineEnd = message.Length();
        }

        wxString line = message.substr(pos, lineEnd - pos).Trim(false).Trim();

        if (line.StartsWith(TABLE_COLUMN_MARKER))
        {
            if (!inTable)
            {
                inTable = true;
                currentTable.clear();
                output << TABLE_MARKER; // Mark the start of a table
            }
            currentTable << line << '\n';
        }
        else if (inTable && line.StartsWith(TABLE_HEADER_MARKER))
        {
            currentTable << line << '\n';
        }
        else
        {
            if (inTable)
            {
                inTable = false;
                output << currentTable << TABLE_MARKER; // Mark the end of a table
                currentTable.clear();
            }
            output << line << '\n';
        }
        pos = lineEnd + 1;
    }

    if (inTable)
    {
        output << currentTable << TABLE_MARKER;
    }

    return output;
}

void Markdown::SetBoldStyle(wxRichTextAttr& baseStyle)
{
    baseStyle.SetFontWeight(wxFONTWEIGHT_BOLD);
}

void Markdown::SetItalicStyle(wxRichTextAttr& baseStyle)
{
    baseStyle.SetFontStyle(wxFONTSTYLE_ITALIC);
}

void Markdown::SetUnderlineStyle(wxRichTextAttr& baseStyle)
{
    baseStyle.SetFontUnderlined(true);
}

void Markdown::SetCodeStyle(wxRichTextAttr& baseStyle)
{
    baseStyle.SetFontWeight(wxFONTWEIGHT_MEDIUM);
    baseStyle.SetBackgroundColour(baseStyle.GetBackgroundColour().ChangeLightness(60));
    baseStyle.SetTextColour(baseStyle.GetTextColour().ChangeLightness(140));
}

void Markdown::SetStrikethroughStyle(wxRichTextAttr& baseStyle)
{
    wxFont font = baseStyle.GetFont();
    font.MakeStrikethrough();
    baseStyle.SetFont(font);
}

void Markdown::SetBlockquoteStyle(wxRichTextAttr& baseStyle)
{
    baseStyle.SetBackgroundColour(baseStyle.GetBackgroundColour().ChangeLightness(50));
    baseStyle.SetTextColour(wxColour(220, 220, 220));
}

void Markdown::SetHeadingStyle(wxRichTextAttr& baseStyle, int headingLevel)
{
    wxFont font = baseStyle.GetFont();
    int pointSize = font.GetPointSize();

    switch (headingLevel)
    {
        case 1: pointSize *= 2; break;
        case 2: pointSize *= 1.7; break;
        case 3: pointSize *= 1.5; break;
        case 4: pointSize *= 1.3; break;
        case 5: pointSize *= 1.2; break;
        case 6:
        default: pointSize *= 1.1; break;
    }

    font.SetPointSize(pointSize);
    baseStyle.SetFont(font);
}

void Markdown::SetLinkStyle(wxRichTextAttr& baseStyle, wxString linkUrl)
{
    baseStyle.SetTextColour(*wxBLUE);
    baseStyle.SetFontUnderlined(true);
    baseStyle.SetURL(linkUrl);
}

wxRichTextAttr Markdown::FormatTableMarkdownStyle(wxString& styledText, const wxTextAttr& baseStyle)
{
    wxRichTextAttr textAttr = baseStyle;

    if (styledText.StartsWith(START_LINK_TEXT_MARKER) && styledText.EndsWith(END_LINK_URL_MARKER))
    {
        wxString::size_type endLinkText = styledText.find(END_LINK_TEXT_MARKER);
        wxString::size_type startLinkUrl = styledText.find(START_LINK_URL_MARKER, endLinkText);
        wxString::size_type endLinkUrl = styledText.find(END_LINK_URL_MARKER, startLinkUrl);

        if (endLinkText != wxString::npos && startLinkUrl != wxString::npos && endLinkUrl != wxString::npos)
        {
            wxString linkUrl = styledText.substr(startLinkUrl + 1, endLinkUrl - startLinkUrl - 1);

            if (linkUrl.StartsWith("http://") || linkUrl.StartsWith("https://"))
            {
                SetLinkStyle(textAttr, linkUrl);
                styledText = styledText.Mid(START_LINK_TEXT_MARKER.Length(), endLinkText - 1);
            }
        }
    }
    else
    {
        bool cont;

        do
        {
            cont = false;

            if (styledText.StartsWith(BOLD_MARKER) && styledText.EndsWith(BOLD_MARKER))
            {
                SetBoldStyle(textAttr);
                styledText = styledText.Mid(BOLD_MARKER.Length(), styledText.Length() - BOLD_MARKER.Length() * 2);
                cont = true;
            }

            if (styledText.StartsWith(ITALIC_MARKER) && styledText.EndsWith(ITALIC_MARKER))
            {
                SetItalicStyle(textAttr);
                styledText = styledText.Mid(ITALIC_MARKER.Length(), styledText.Length() - ITALIC_MARKER.Length() * 2);
                cont = true;
            }

            if (styledText.StartsWith(UNDERLINE_MARKER) && styledText.EndsWith(UNDERLINE_MARKER))
            {
                SetUnderlineStyle(textAttr);
                styledText = styledText.Mid(UNDERLINE_MARKER.Length(), styledText.Length() - UNDERLINE_MARKER.Length() * 2);
                cont = true;
            }

            if (styledText.StartsWith(CODE_MARKER) && styledText.EndsWith(CODE_MARKER))
            {
                SetCodeStyle(textAttr);
                styledText = styledText.Mid(CODE_MARKER.Length(), styledText.Length() - CODE_MARKER.Length() * 2);
                cont = true;
            }

            if (styledText.StartsWith(STRIKETHROUGH_MARKER) && styledText.EndsWith(STRIKETHROUGH_MARKER))
            {
                SetStrikethroughStyle(textAttr);
                styledText = styledText.Mid(STRIKETHROUGH_MARKER.Length(), styledText.Length() - STRIKETHROUGH_MARKER.Length() * 2);
                cont = true;
            }
        }while(cont);
    }

    return textAttr;
}

void Markdown::CreateAndInsertTable(wxRichTextCtrl* richTextChatBox, const wxString& tableData, const wxTextAttr& style)
{
    wxArrayString rows = wxSplit(tableData, '\n');
    if (rows.empty()) return;

    wxArrayString headers = wxSplit(rows[0], TABLE_COLUMN_MARKER[0]);

    headers = FilterEmptyElements(headers);

    wxString::size_type colsCount = headers.GetCount();
    wxString::size_type rowsCount = rows.GetCount() - 2; // Exclude header underline

    if (colsCount < 2)
    {
        return;
    }

    wxRichTextTable* table = richTextChatBox->WriteTable(rowsCount, colsCount);

    // Headers
    for (wxString::size_type col = 0; col < colsCount; ++col)
    {
        wxString cellText = headers[col].Trim(false).Trim();
        wxRichTextAttr cellAttr = FormatTableMarkdownStyle(cellText, style);
        cellAttr.SetAlignment(wxTEXT_ALIGNMENT_CENTRE);
        cellAttr.SetFontWeight(wxFONTWEIGHT_BOLD);
        table->GetCell(0, col)->AddParagraph(cellText, &cellAttr);
    }

    for (wxString::size_type row = 1; row < rowsCount; ++row)
    {
        wxArrayString cells = wxSplit(rows[row + 1], TABLE_COLUMN_MARKER[0]); // Skip header row

        cells = FilterEmptyElements(cells);

        if (cells.GetCount() != colsCount)
        {
            continue;
        }

        for (wxString::size_type col = 0; col < colsCount; ++col)
        {
            wxString cellText = cells[col].Trim(false).Trim();
            wxRichTextAttr cellAttr = FormatTableMarkdownStyle(cellText, style);
            cellAttr.SetAlignment(wxTEXT_ALIGNMENT_CENTRE);
            table->GetCell(row, col)->AddParagraph(cellText, &cellAttr);
        }
    }
}

wxString Markdown::ReplaceUnpaired(wchar_t character, wchar_t replaceCharacter, const wxString& message)
{
    wxString output;
    for (wxString::size_type i = 0; i < message.length(); ++i)
    {
        if (message[i] == '\n' && message[i + 1] == character)
        {
            wxString::size_type nextChar = message.find(character, i + 2);
            if (nextChar == wxString::npos || nextChar >= message.find('\n', i + 2))
            {
                output << message[i] << replaceCharacter << ' ';
                i += 1;
                continue;
            }
        }
        output << message[i];
    }

    return output;
}

wxString Markdown::FormatMessage(const wxString& message)
{
    wxString formattedMessage = message;

    formattedMessage.Replace(LINE_BREAK_TAG, "\n");
    formattedMessage.Replace(START_BOLD_TAG, BOLD_MARKER);
    formattedMessage.Replace(END_BOLD_TAG, BOLD_MARKER);
    formattedMessage.Replace(START_ITALIC_TAG, ITALIC_MARKER);
    formattedMessage.Replace(END_ITALIC_TAG, ITALIC_MARKER);
    formattedMessage.Replace(START_LIST_MARKER, LIST_MARKER);
    formattedMessage = Markdown::ReplaceUnpaired('*', LIST_MARKER[1], formattedMessage);

    formattedMessage = DetectAndMarkTables(formattedMessage);

    return formattedMessage;
}

void Markdown::HighlightSyntax(wxRichTextCtrl* richTextChatBox, const wxString& code, const wxString& language, const wxTextAttr& baseStyle)
{
    if (language == "cpp" || language == "c++" || language == "c" || language == "python" || language == "csharp" || language == "c#" || language == "java" || language == "kotlin" || language == "js" || language == "javascript" || language == "ts" || language == "typescript" || language == "php" || language == "ruby" || language == "lua" || language == "r" || language == "go" || language == "fortran" || language == "sql")
    {
        wxTextAttr keywordStyle = baseStyle;
        keywordStyle.SetTextColour(*wxBLUE);
        std::vector<wxString> keywords;

        wxString codeCopy = code;
        wxString rest = code;
        wxString::size_type pos = 0;

        if (language == "cpp" || language == "c++")
        {
            keywords = { "alignas", "alignof", "and", "and_eq", "asm", "atomic_cancel", "atomic_commit", "atomic_noexcept", "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char", "char8_t", "char16_t", "char32_t", "class", "compl", "concept", "const", "consteval", "constexpr", "constinit", "const_cast", "continue", "co_await", "co_return", "co_yield", "decltype", "default", "delete", "do", "double", "dynamic_cast", "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto", "if", "inline", "int", "long", "mutable", "namespace", "new", "noexcept", "not", "not_eq", "nullptr", "operator", "or", "or_eq", "private", "protected", "public", "reflexpr", "register", "reinterpret_cast", "requires", "return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct", "switch", "synchronized", "template", "this", "thread_local", "throw", "true", "try", "typedef", "typeid", "typename", "union", "unsigned", "using", "virtual", "void", "volatile", "wchar_t", "while", "xor", "xor_eq", "final", "override", "transaction_safe", "transaction_safe_dynamic", "import", "module", "_Pragma", "if", "elif", "else", "endif", "ifdef", "ifndef", "elifdef", "elifndef", "define", "undef", "include", "line", "error", "warning", "pragma", "defined", "__has_include", "__has_cpp_attribute", "export", "import", "module" };
        }
        else if (language == "c")
        {
            keywords = { "alignas", "alignof", "auto", "bool", "break", "case", "char", "const", "constexpr", "continue", "default", "do", "double", "else", "enum", "extern", "false", "float", "for", "goto", "if", "inline", "int", "long", "nullptr", "register", "restrict", "return", "short", "signed", "sizeof", "static", "static_assert", "struct", "switch", "thread_local", "true", "typedef", "typeof", "typeof_unqual", "union", "unsigned", "void", "volatile", "while", "_Alignas", "_Alignof", "_Atomic", "_BitInt", "_Bool", "_Complex", "_Decimal128", "_Decimal32", "_Decimal64", "_Generic", "_Imaginary", "_Noreturn", "_Static_assert", "_Thread_local", "asm", "fortran", "_Pragma", "if", "elif", "else", "endif", "ifdef", "ifndef", "elifdef", "elifndef", "define", "undef", "include", "embed", "line", "error", "warning", "pragma", "defined", "__has_include", "__has_embed", "__has_c_attribute" };
        }
        else if (language == "python")
        {
            keywords = { "and", "as", "assert", "break", "class", "continue", "def", "del", "elif", "else", "except", "exec", "finally", "for", "from", "global", "if", "import", "in", "is", "lambda", "not", "or", "pass", "print", "raise", "return", "try", "while", "with", "yield" };
        }
        else if (language == "csharp" || language == "c#")
        {
            keywords = { "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "foreach", "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator", "out", "override", "params", "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string", "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort", "using", "virtual", "void", "volatile", "while", "add", "and", "alias", "ascending", "args", "async", "await", "by", "descending", "dynamic", "equals", "file", "from", "get", "global", "group", "init", "into", "join", "let", "managed", "nameof", "nint", "not", "notnull", "nuint", "on", "or", "orderby", "partial", "partial", "record", "remove", "required", "scoped", "select", "set", "unmanaged", "unmanaged", "value", "var", "when", "where", "where", "with", "yield" };
        }
        else if (language == "java")
        {
            keywords = { "abstract", "continue", "for", "new", "switch", "assert", "default", "goto", "package", "synchronized", "boolean", "do", "if", "private", "this", "break", "double", "implements", "protected", "throw", "byte", "else", "import", "public", "throws", "case", "enum", "instanceof", "return", "transient", "catch", "extends", "int", "short", "try", "char", "final", "interface", "static", "void", "class", "finally", "long", "strictfp", "volatile", "const", "float", "native", "super", "while" };
        }
        else if (language == "kotlin")
        {
            keywords = { "as", "break", "class", "continue", "do", "else", "false", "for", "fun", "if", "in", "interface", "is", "null", "object", "package", "return", "super", "this", "throw", "true", "try", "typealias", "val", "var", "when", "while", "by", "catch", "constructor", "delegate", "dynamic", "field", "file", "finally", "get", "import", "init", "param", "property", "receiver", "set", "value", "where", "abstract", "actual", "annotation", "companion", "const", "crossinline", "data", "enum", "expect", "external", "final", "infix", "inline", "inner", "internal", "lateinit", "noinline", "open", "operator", "out", "override", "private", "protected", "public", "reified", "sealed", "suspend", "tailrec", "vararg", "field", "it" };
        }
        else if (language == "js" || language == "javascript")
        {
            keywords = { "abstract", "arguments", "await", "boolean", "break", "byte", "case", "catch", "char", "class", "const", "continue", "debugger", "default", "delete", "do", "double", "else", "enum", "eval", "export", "extends", "false", "final", "finally", "float", "for", "function", "goto", "if", "implements", "import", "in", "instanceof", "int", "interface", "let", "long", "native", "new", "null", "package", "private", "protected", "public", "return", "short", "static", "super", "switch", "synchronized", "this", "throw", "throws", "transient", "true", "try", "typeof", "var", "void", "volatile", "while", "with", "yield" };
        }
        else if (language == "ts" || language == "typescript")
        {
            keywords = { "break", "case", "catch", "class", "const", "continue", "debugger", "default", "delete", "do", "else", "enum", "export", "extends", "false", "finally", "for", "function", "if", "import", "in", "instanceOf", "new", "null", "return", "super", "switch", "this", "throw", "true", "try", "typeof", "var", "void", "while", "with", "as", "implements", "interface", "let", "package", "private", "protected", "public", "static", "yield", "any", "boolean", "constructor", "declare", "get", "module", "require", "number", "set", "string", "symbol", "type", "from", "of" };
        }
        else if (language == "php")
        {
            keywords = { "__halt_compiler", "abstract", "and", "array", "as", "break", "callable", "case", "catch", "class", "clone", "const", "continue", "declare", "default", "die", "do", "echo", "else", "elseif", "empty", "enddeclare", "endfor", "endforeach", "endif", "endswitch", "endwhile", "eval", "exit", "extends", "final", "finally", "fn", "for", "foreach", "function", "global", "goto", "if", "implements", "include", "include_once", "instanceof", "insteadof", "interface", "isset", "list", "match", "namespace", "new", "or", "print", "private", "protected", "public", "readonly", "require", "require_once", "return", "static", "switch", "throw", "trait", "try", "unset", "use", "var", "while", "xor", "yield", "yield from", "__CLASS__", "__DIR__", "__FILE__", "__FUNCTION__", "__LINE__", "__METHOD__", "__NAMESPACE__", "__TRAIT__" };
        }
        else if (language == "ruby")
        {
            keywords = { "__ENCODING__", "__LINE__", "__FILE__", "BEGIN", "END", "alias", "and", "begin", "break", "case", "class", "def", "defined?", "do", "else", "elsif", "end", "ensure", "false", "for", "if", "in", "module", "next", "nil", "not", "or", "redo", "rescue", "retry", "return", "self", "super", "then", "true", "undef", "unless", "until", "when", "while", "yield" };
        }
        else if (language == "lua")
        {
            keywords = { "and", "break", "do", "else", "elseif", "end", "false", "for", "function", "if", "in", "local", "nil", "not", "or", "repeat", "return", "then", "true", "until", "while" };
        }
        else if (language == "r")
        {
            keywords = { "if", "else", "repeat", "while", "function", "for", "next", "break", "TRUE", "FALSE", "NULL", "Inf", "NaN", "NA", "NA_integer_", "NA_real_", "NA_complex_", "NA_character_" };
        }
        else if (language == "go")
        {
            keywords = { "break", "default", "func", "interface", "select", "case", "defer", "go", "map", "struct", "chan", "else", "goto", "package", "switch", "const", "fallthrough", "if", "range", "type", "continue", "for", "import", "return", "var" };
        }
        else if (language == "fortran")
        {
            keywords = { "allocatable", "allocate", "assignment", "backspace", "call", "case", "character", "close", "complex", "contains", "cycle", "deallocate", "default", "do", "else", "else if", "elsewhere", "end do", "end function", "end if", "end interface", "end module", "end program", "end select", "end subroutine", "end type", "end where", "endfile", "exit", "format", "function", "if", "implicit", "in", "inout", "integer", "intent", "interface", "intrinsic", "inquire", "kind", "len", "logical", "module", "namelist", "nullify", "only", "open", "operator", "optional", "out", "print", "pointer", "private", "program", "public", "read", "real", "recursive", "result", "return", "rewind", "select case", "stop", "subroutine", "target", "then", "type", "use", "where", "while", "write" };
        }
        else if (language == "sql")
        {
            keywords = { "ADD", "ADD CONSTRAINT", "ALL", "ALTER", "ALTER COLUMN", "ALTER TABLE", "AND", "ANY", "AS", "ASC", "BACKUP DATABASE", "BETWEEN", "CASE", "CHECK", "COLUMN", "CONSTRAINT", "CREATE", "CREATE DATABASE", "CREATE INDEX", "CREATE OR REPLACE VIEW", "CREATE TABLE", "CREATE PROCEDURE", "CREATE UNIQUE INDEX", "CREATE VIEW", "DATABASE", "DEFAULT", "DELETE", "DESC", "DISTINCT", "DROP", "DROP COLUMN", "DROP CONSTRAINT", "DROP DATABASE", "DROP DEFAULT", "DROP INDEX", "DROP TABLE", "DROP VIEW", "EXEC", "EXISTS", "FOREIGN KEY", "FROM", "FULL OUTER JOIN", "GROUP BY", "HAVING", "IN", "INDEX", "INNER JOIN", "INSERT INTO", "INSERT INTO SELECT", "IS NULL", "IS NOT NULL", "JOIN", "LEFT JOIN", "LIKE", "LIMIT", "NOT", "NOT NULL", "OR", "ORDER BY", "OUTER JOIN", "PRIMARY KEY", "PROCEDURE", "RIGHT JOIN", "ROWNUM", "SELECT", "SELECT DISTINCT", "SELECT INTO", "SELECT TOP", "SET", "TABLE", "TOP", "TRUNCATE TABLE", "UNION", "UNION ALL", "UNIQUE", "UPDATE", "VALUES", "VIEW", "WHERE" };
        }

        while (pos != wxString::npos)
        {
            size_t minPos = wxString::npos;
            wxString keyword;
            for (const auto& kw : keywords)
            {
                size_t kwPos = rest.find(kw);
                if (kwPos != wxString::npos && (minPos == wxString::npos || kwPos < minPos))
                {
                    minPos = kwPos;
                    keyword = kw;
                }
            }

            if (minPos == wxString::npos)
            {
                break;
            }

            pos = codeCopy.Length() - rest.Length() + minPos;
            if (pos != 0)
            {
                wxChar before = codeCopy[pos - 1];
                if (wxIsalnum(before) || before == '_')
                {
                    rest = rest.substr(minPos + keyword.Length());
                    continue;
                }
            }

            wxChar after = codeCopy[pos + keyword.Length()];
            if (wxIsalnum(after) || after == '_')
            {
                rest = rest.substr(minPos + keyword.Length());
                continue;
            }

            richTextChatBox->WriteText(codeCopy.substr(0, pos));
            richTextChatBox->BeginStyle(keywordStyle);
            richTextChatBox->WriteText(keyword);
            richTextChatBox->EndStyle();
            codeCopy = codeCopy.substr(pos + keyword.Length());
            rest = rest.substr(minPos + keyword.Length());
        }

        richTextChatBox->WriteText(codeCopy);
    }
    else
    {
        richTextChatBox->WriteText(code);
    }
}

void Markdown::WriteText(wxRichTextCtrl* richTextChatBox, const wxString& message, const wxTextAttr& style)
{
    richTextChatBox->BeginStyle(style);

    wxString formattedMessage = Markdown::FormatMessage(message);

    wxString::size_type pos = 0;

    while (pos < formattedMessage.Length() && pos != wxString::npos)
    {
        wxString::size_type startBoldItalic = formattedMessage.find(BOLD_ITALIC_MARKER, pos);
        wxString::size_type startBold = formattedMessage.find(BOLD_MARKER, pos);
        wxString::size_type startItalic = formattedMessage.find(ITALIC_MARKER, pos);
        wxString::size_type startUnderline = formattedMessage.find(UNDERLINE_MARKER, pos);
        wxString::size_type startLinkText = formattedMessage.find(START_LINK_TEXT_MARKER, pos);
        wxString::size_type startImage = formattedMessage.find(START_IMAGE_MARKER, pos);
        wxString::size_type startCode = formattedMessage.find(CODE_MARKER, pos);
        wxString::size_type startStrikethrough = formattedMessage.find(STRIKETHROUGH_MARKER, pos);
        wxString::size_type startTable = formattedMessage.find(TABLE_MARKER, pos);
        wxString::size_type startCodeBlock = formattedMessage.find(CODE_BLOCK_MARKER, pos);

        wxString::size_type startHeadings[] = {
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[0] : ("\n" + HEADING_MARKERS[0]), pos),
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[1] : ("\n" + HEADING_MARKERS[1]), pos),
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[2] : ("\n" + HEADING_MARKERS[2]), pos),
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[3] : ("\n" + HEADING_MARKERS[3]), pos),
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[4] : ("\n" + HEADING_MARKERS[4]), pos),
            formattedMessage.find(pos == 0 ? HEADING_MARKERS[5] : ("\n" + HEADING_MARKERS[5]), pos)
        };

        wxString::size_type startBlockquote = formattedMessage.find(pos == 0 ? BLOCKQUOTE_MARKER : ("\n" + BLOCKQUOTE_MARKER), pos);

        std::vector<std::pair<wxString::size_type, int>> markers = {
            { startBoldItalic, 3 },
            { startBold, 2 },
            { startItalic, 1 },
            { startUnderline, 2 },
            { startCode, 1 },
            { startHeadings[5], 7 },
            { startHeadings[4], 6 },
            { startHeadings[3], 5 },
            { startHeadings[2], 4 },
            { startHeadings[1], 3 },
            { startHeadings[0], 2 },
            { startBlockquote, 2 },
            { startCodeBlock, 3 },
            { startStrikethrough, 2 },
            { startLinkText, -1 },
            { startImage, -1 },
            { startTable, 0 }
        };

        // Ignore inline code if a code block is found earlier
        if (startCodeBlock != wxString::npos && (startCode == wxString::npos || startCodeBlock <= startCode))
        {
            startCode = wxString::npos;
        }

        // Find the earliest marker
        auto earliestIt = std::min_element(markers.begin(), markers.end(), [](const auto& lhs, const auto& rhs){
            return (lhs.first != wxString::npos && (rhs.first == wxString::npos || lhs.first < rhs.first));
        });

        if (earliestIt->first != wxString::npos)
        {
            wxString::size_type markerPos = earliestIt->first;
            int markerLength = earliestIt->second;

            wxString::size_type endMarker;

            if (startBoldItalic == markerPos)
            {
                endMarker = formattedMessage.find(BOLD_ITALIC_MARKER, markerPos + markerLength);
            }
            else if (startBold == markerPos)
            {
                endMarker = formattedMessage.find(BOLD_MARKER, markerPos + markerLength);
            }
            else if (startUnderline == markerPos)
            {
                endMarker = formattedMessage.find(UNDERLINE_MARKER, markerPos + markerLength);
            }
            else if (startItalic == markerPos)
            {
                endMarker = formattedMessage.find(ITALIC_MARKER, markerPos + markerLength);
            }
            else if (startCode == markerPos)
            {
                endMarker = formattedMessage.find(CODE_MARKER, markerPos + markerLength);
            }
            else if (startStrikethrough == markerPos)
            {
                endMarker = formattedMessage.find(STRIKETHROUGH_MARKER, markerPos + markerLength);
            }
            else if (startHeadings[0] == markerPos || startHeadings[1] == markerPos || startHeadings[2] == markerPos || startHeadings[3] == markerPos || startHeadings[4] == markerPos || startHeadings[5] == markerPos || startBlockquote == markerPos)
            {
                endMarker = formattedMessage.find("\n", markerPos + markerLength);
            }
            else if (startTable == markerPos)
            {
                wxString::size_type tableEnd = formattedMessage.find(TABLE_MARKER, markerPos + TABLE_MARKER.length());
                if (tableEnd != wxString::npos)
                {
                    wxString tableData = formattedMessage.substr(markerPos + TABLE_MARKER.length(), tableEnd - (markerPos + TABLE_MARKER.length()));
                    CreateAndInsertTable(richTextChatBox, tableData, style);
                    pos = tableEnd + TABLE_MARKER.length();
                    continue;
                }
                else
                {
                    richTextChatBox->WriteText(formattedMessage.substr(pos));
                    pos = wxString::npos;
                    continue;
                }
            }
            else if (startLinkText == markerPos)
            {
                wxString::size_type endLinkText = formattedMessage.find(END_LINK_TEXT_MARKER, pos);
                wxString::size_type startLinkUrl = formattedMessage.find(START_LINK_URL_MARKER, endLinkText);

                if (endLinkText != wxString::npos && (endLinkText + 1 == startLinkUrl))
                {
                    wxString::size_type endLinkUrl = formattedMessage.find(END_LINK_URL_MARKER, startLinkUrl);

                    if (startLinkUrl != wxString::npos && endLinkUrl != wxString::npos)
                    {
                        wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                        wxString linkText = formattedMessage.substr(markerPos + 1, endLinkText - markerPos - 1);
                        wxString linkUrl = formattedMessage.substr(startLinkUrl + 1, endLinkUrl - startLinkUrl - 1);

                        if (linkUrl.StartsWith("http://") || linkUrl.StartsWith("https://"))
                        {
                            richTextChatBox->WriteText(beforeMarker);

                            wxRichTextAttr urlStyle = style;
                            SetLinkStyle(urlStyle, linkUrl);

                            richTextChatBox->BeginStyle(urlStyle);
                            richTextChatBox->WriteText(linkText);
                            richTextChatBox->EndStyle();

                            pos = endLinkUrl + 1;
                            continue;
                        }
                        else
                        {
                            wxString invalidLinkText = formattedMessage.substr(markerPos, endLinkUrl - markerPos + 1);
                            richTextChatBox->WriteText(beforeMarker + invalidLinkText);
                            pos = endLinkUrl + 1;
                            continue;
                        }
                    }
                }
                else
                {
                    wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                    wxString invalidLinkText = formattedMessage.substr(markerPos, endLinkText - markerPos + 1);
                    richTextChatBox->WriteText(beforeMarker + invalidLinkText);
                    pos = endLinkText + 1;
                    continue;
                }
            }
            else if (startImage == markerPos)
            {
                wxString::size_type endAltText = formattedMessage.find(END_LINK_TEXT_MARKER, pos);
                wxString::size_type startImageText = formattedMessage.find(START_LINK_URL_MARKER, endAltText);

                if (endAltText != wxString::npos && (endAltText + 1 == startImageText))
                {
                    wxString::size_type endImageText = formattedMessage.find(END_LINK_URL_MARKER, startImageText);

                    if (startImageText != wxString::npos && endImageText != wxString::npos)
                    {
                        wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                        wxString encodedImage = formattedMessage.substr(startImageText + 1, endImageText - startImageText - 1);

                        if (encodedImage.StartsWith("data:image;base64,"))
                        {
                            richTextChatBox->WriteText(beforeMarker);

                            wxImage image = Base64::Decode(encodedImage.Mid(18));

                            if (image.IsOk())
                            {
                                richTextChatBox->WriteImage(image);
                            }
                            else
                            {
                                wxString exePath = FileUtils::GetExecutablePath();
                                richTextChatBox->WriteImage(ImageUtils::LoadBitmap(exePath + "/res/buttons/attach-image-error.png"));
                                wxString invalidImageText = formattedMessage.substr(markerPos, endAltText - markerPos + 1);
                                richTextChatBox->WriteText(invalidImageText);
                            }

                            pos = endImageText + 1;
                            continue;
                        }
                        else
                        {
                            wxString invalidImageText = formattedMessage.substr(markerPos, endAltText - markerPos + 1);
                            richTextChatBox->WriteText(beforeMarker + invalidImageText);
                            pos = endImageText + 1;
                            continue;
                        }
                    }
                }
                else
                {
                    wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                    wxString invalidImageText = formattedMessage.substr(markerPos, endAltText - markerPos + 1);
                    richTextChatBox->WriteText(beforeMarker + invalidImageText);
                    pos = endAltText + 1;
                    continue;
                }
            }
            else if (startCodeBlock == markerPos)
            {
                wxString::size_type startLang = markerPos + 3;
                wxString::size_type endLang = formattedMessage.find('\n', startLang);
                wxString language = formattedMessage.substr(startLang, endLang - startLang).Trim().MakeLower();
                wxString::size_type codeStart = endLang + 1;
                endMarker = formattedMessage.find(CODE_BLOCK_MARKER, codeStart);

                wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                richTextChatBox->WriteText(beforeMarker);

                if (language.IsEmpty())
                {
                    pos = startLang;
                    continue;
                }
                else if (language == "markdown")
                {
                    pos = codeStart;
                    continue;
                }
                else
                {
                    if (endMarker != wxString::npos)
                    {
                        wxString codeBlockText = formattedMessage.substr(codeStart, endMarker - codeStart);
                        HighlightSyntax(richTextChatBox, codeBlockText, language, style);

                        pos = endMarker + 3;
                        continue;
                    }
                    else
                    {
                        richTextChatBox->WriteText(formattedMessage.substr(pos));

                        pos = wxString::npos;
                        continue;
                    }
                }
            }

            if (endMarker != wxString::npos)
            {
                wxString beforeMarker = formattedMessage.substr(pos, markerPos - pos);
                richTextChatBox->WriteText(beforeMarker);
                wxString styledText = formattedMessage.substr(markerPos + markerLength, endMarker - markerPos - markerLength);

                if (startBoldItalic == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetBoldStyle(textAttr);
                    SetItalicStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startBold == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetBoldStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startItalic == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetItalicStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startUnderline == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetUnderlineStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startCode == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetCodeStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startStrikethrough == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetStrikethroughStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }
                else if (startBlockquote == markerPos)
                {
                    wxRichTextAttr textAttr = style;
                    SetBlockquoteStyle(textAttr);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();

                    if (endMarker < formattedMessage.Length() && formattedMessage[endMarker] == '\n')
                    {
                        richTextChatBox->WriteText("\n");
                    }
                }
                else if (startHeadings[0] == markerPos || startHeadings[1] == markerPos || startHeadings[2] == markerPos || startHeadings[3] == markerPos || startHeadings[4] == markerPos || startHeadings[5] == markerPos)
                {
                    int headingLevel;

                    for (int i = 0; i < 6; ++i)
                    {
                        if (startHeadings[i] == markerPos)
                        {
                            headingLevel = i + 1;
                            break;
                        }
                    }

                    if (markerPos < formattedMessage.Length() && formattedMessage[markerPos - 1] != '\n')
                    {
                        richTextChatBox->WriteText("\n");
                    }

                    styledText = styledText.Trim(false).Trim();

                    richTextChatBox->WriteText("\n");

                    wxRichTextAttr textAttr = style;
                    SetHeadingStyle(textAttr, headingLevel);
                    richTextChatBox->BeginStyle(textAttr);
                    richTextChatBox->WriteText(styledText);
                    richTextChatBox->EndStyle();
                }

                if (startHeadings[0] == markerPos || startHeadings[1] == markerPos || startHeadings[2] == markerPos || startHeadings[3] == markerPos || startHeadings[4] == markerPos || startHeadings[5] == markerPos)
                {
                    pos = endMarker;
                }
                else
                {
                    pos = endMarker + markerLength;
                }
            }
            else
            {
                richTextChatBox->WriteText(formattedMessage.substr(pos));
                pos = wxString::npos;
            }
        }
        else
        {
            richTextChatBox->WriteText(formattedMessage.substr(pos));
            pos = wxString::npos;
        }
    }

    richTextChatBox->EndStyle();
}
