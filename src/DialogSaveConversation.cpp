/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogSaveConversation.h"

#include <wx/msgdlg.h>

DialogSaveConversation::DialogSaveConversation(wxWindow* parent, wxString* filename, wxString* conversationName) : DialogSaveConversationView(parent), filename(filename), conversationName(conversationName)
{
}

void DialogSaveConversation::OnSaveConversationOKClick(wxCommandEvent& event)
{
    if (!textCtrlFilename->GetValue().IsEmpty() && !textCtrlConversationName->GetValue().IsEmpty())
    {
        *filename = textCtrlFilename->GetValue();
        *conversationName = textCtrlConversationName->GetValue();

        EndModal(wxID_OK);
    }
    else
    {
        wxMessageBox(wxT("Fill all the required fields."), wxT("Missing values"), wxOK | wxICON_EXCLAMATION);
    }
}

void DialogSaveConversation::OnSaveConversationCancelClick(wxCommandEvent& event)
{
    if (!textCtrlFilename->GetValue().IsEmpty() && !textCtrlConversationName->GetValue().IsEmpty())
    {
        if (wxMessageBox(wxT("Do you want to exit without saving?"), wxT("Save conversation"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            EndModal(wxID_CANCEL);
        }
    }
    else
    {
        EndModal(wxID_CANCEL);
    }
}
