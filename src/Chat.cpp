/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Chat.h"
#include "Network.h"
#include "Markdown.h"
#include "Base64.h"
#include "FileUtils.h"

#include <thread>
#include <stdexcept>
#include <filesystem>
#include <fstream>

#include <wx/log.h>

const wxEventTypeTag<wxCommandEvent> Chat::wxEVT_COMMAND_TEXT_REFORMAT(wxNewEventType());

Chat::Chat(const ServerSettings& serverSettings, const wxString& modelName, wxRichTextCtrl* richTextChatBox, ModelParams* modelParams, const wxString& conversationName, const wxString& filePath) : serverSettings(serverSettings), modelName(modelName), richTextChatBox(richTextChatBox), ready(true), modelParams(modelParams), conversationName(conversationName), streamingStartPos(-1), encodedImage(wxEmptyString)
{
    Bind(wxEVT_COMMAND_TEXT_UPDATED, &Chat::OnStreamingText, this);
    Bind(wxEVT_COMMAND_TEXT_REFORMAT, &Chat::OnTextReformat, this);
}

void Chat::SendMessage(const wxString& userMessage)
{
    if (!EndsWithNewline())
    {
        AppendMessage("\n", "assistant");
    }

    nlohmann::json chatEntry = {
        {"role", "user"},
        {"content", std::string(userMessage.utf8_str())}
    };

    if (HasImage())
    {
        AppendImage(encodedImage);

        std::vector<std::string> images;
        images.push_back(encodedImage.ToStdString());

        chatEntry["images"] = images;

        encodedImage = wxEmptyString;
    }

    AppendMessage(userMessage, "user");

    chatHistory.push_back(chatEntry);

    nlohmann::json requestBody = MakeRequestBody(userMessage);

    wxLogDebug("Constructed request body: %s", wxString::FromUTF8(requestBody.dump().c_str()));

    ready = false;

    std::thread([this, requestBody]{
        try
        {
            Network& network = Network::GetInstance();

            if (serverSettings.GetStreaming())
            {
                if (!EndsWithNewline())
                {
                    AppendMessage("\n", "user");
                }

                std::string fullResponse;

                network.HTTPPostStream([this, &fullResponse](const std::string& responseChunk){
                    wxLogDebug("Received chunk: %s", wxString::FromUTF8(responseChunk.c_str()));

                    nlohmann::json jsonChunk = nlohmann::json::parse(responseChunk);
                    if (jsonChunk.contains("message") && jsonChunk["message"].contains("content") && !jsonChunk["message"]["content"].is_null())
                    {
                        std::string content = jsonChunk["message"]["content"].get<std::string>();

                        wxCommandEvent event(wxEVT_COMMAND_TEXT_UPDATED);
                        event.SetString(wxString::FromUTF8(content.c_str()));
                        wxPostEvent(this, event);

                        fullResponse += content;

                        if (jsonChunk["done"].get<bool>())
                        {
                            if (!fullResponse.empty())
                            {
                                wxLogDebug("AI message content: %s", wxString::FromUTF8(fullResponse.c_str()));
                                chatHistory.push_back({
                                    {"role", "assistant"},
                                    {"content", fullResponse}
                                });

                                wxCommandEvent reformatEvent(wxEVT_COMMAND_TEXT_REFORMAT);
                                reformatEvent.SetString(wxString::FromUTF8(fullResponse.c_str()));
                                wxPostEvent(this, reformatEvent);
                            }
                            else
                            {
                                wxLogDebug("Received empty response from server");
                            }
                        }
                    }
                }, serverSettings.GetHost(), "/api/chat", requestBody.dump(), "application/json", serverSettings.GetTimeoutSeconds());
            }
            else
            {
                std::string response = network.HTTPPost(serverSettings.GetHost(), "/api/chat", requestBody.dump(), "application/json", serverSettings.GetTimeoutSeconds());

                wxLogDebug("Raw response: %s", wxString::FromUTF8(response.c_str()));

                if (!response.empty())
                {
                    nlohmann::json j = nlohmann::json::parse(response);

                    wxLogDebug("Parsed JSON: %s", wxString::FromUTF8(j.dump().c_str()));

                    if (j.contains("message") && j["message"].contains("content") && !j["message"]["content"].is_null())
                    {
                        std::string aiMessage = j["message"]["content"].get<std::string>();
                        wxLogDebug("AI message content: %s", wxString::FromUTF8(aiMessage.c_str()));
                        AppendMessage(wxString::FromUTF8(aiMessage.c_str()), "assistant");
                        chatHistory.push_back({
                            {"role", "assistant"},
                            {"content", aiMessage}
                        });
                    }
                    else
                    {
                        wxLogDebug("Parsed JSON does not contain required fields.");
                    }
                }
                else
                {
                    wxLogDebug("Received empty response from server");
                }
            }
        }
        catch (const std::exception& e)
        {
            wxLogError("Failed to communicate with the server: %s", e.what());
        }
        ready = true;
    }).detach();
}

void Chat::AttachImage(const wxString& filePath)
{
    if (!filePath.IsEmpty())
    {
        encodedImage = Base64::Encode(filePath);
    }
}

wxString Chat::GetLastUserMessage() const
{
    wxString message = wxEmptyString;

    for (auto it = chatHistory.rbegin(); it != chatHistory.rend(); ++it)
    {
        if ((*it)["role"] == "user")
        {
            message = wxString::FromUTF8((*it)["content"].get<std::string>().c_str());
            break;
        }
    }

    return message;
}

bool Chat::IsReady() const
{
    return ready;
}

wxString Chat::GetConversationName() const
{
    return conversationName;
}

void Chat::SetConversationName(const wxString& conversationName)
{
    this->conversationName = conversationName;
}

wxString Chat::GetFilePath() const
{
    return filePath;
}

void Chat::SetFilePath(const wxString& filePath)
{
    this->filePath = filePath;
}

void Chat::SaveConversation()
{
    nlohmann::json conversationJson;

    // Store conversation parameters
    conversationJson["Name"] = conversationName.ToUTF8().data();
    conversationJson["chatHistory"] = chatHistory;

    // Store model parameters
    nlohmann::json modelParamsJson = {
        {"SystemMessageEnabled", modelParams->SystemMessageEnabled},
        {"SystemMessage", modelParams->SystemMessage},
        {"TemplateEnabled", modelParams->TemplateEnabled},
        {"Template", modelParams->Template},
        {"SeedEnabled", modelParams->SeedEnabled},
        {"Seed", modelParams->Seed},
        {"TemperatureEnabled", modelParams->TemperatureEnabled},
        {"Temperature", modelParams->Temperature},
        {"ContextSizeEnabled", modelParams->ContextSizeEnabled},
        {"ContextSize", modelParams->ContextSize},
        {"PredictTokensEnabled", modelParams->PredictTokensEnabled},
        {"PredictTokens", modelParams->PredictTokens},
        {"RepeatLastEnabled", modelParams->RepeatLastEnabled},
        {"RepeatLast", modelParams->RepeatLast},
        {"RepeatPenaltyEnabled", modelParams->RepeatPenaltyEnabled},
        {"RepeatPenalty", modelParams->RepeatPenalty},
        {"TopKEnabled", modelParams->TopKEnabled},
        {"TopK", modelParams->TopK},
        {"TopPEnabled", modelParams->TopPEnabled},
        {"TopP", modelParams->TopP},
        {"TFSZEnabled", modelParams->TFSZEnabled},
        {"TFSZ", modelParams->TFSZ},
        {"MirostatEnabled", modelParams->MirostatEnabled},
        {"Mirostat", modelParams->Mirostat},
        {"MirostatTauEnabled", modelParams->MirostatTauEnabled},
        {"MirostatTau", modelParams->MirostatTau},
        {"MirostatEtaEnabled", modelParams->MirostatEtaEnabled},
        {"MirostatEta", modelParams->MirostatEta},
        {"KeepTokensEnabled", modelParams->KeepTokensEnabled},
        {"KeepTokens", modelParams->KeepTokens},
        {"TypicalPEnabled", modelParams->TypicalPEnabled},
        {"TypicalP", modelParams->TypicalP},
        {"PresencePenaltyEnabled", modelParams->PresencePenaltyEnabled},
        {"PresencePenalty", modelParams->PresencePenalty},
        {"FrequencyPenaltyEnabled", modelParams->FrequencyPenaltyEnabled},
        {"FrequencyPenalty", modelParams->FrequencyPenalty},
        {"PenalizeNewLinesEnabled", modelParams->PenalizeNewLinesEnabled},
        {"PenalizeNewLines", modelParams->PenalizeNewLines}
    };

    conversationJson["modelParams"] = modelParamsJson;

    FileUtils::CreateDirectory(wxString::FromUTF8(std::filesystem::path(filePath.ToStdString()).parent_path().u8string().c_str()));

    std::ofstream file(filePath.ToUTF8().data());
    if (file.is_open())
    {
        file << conversationJson.dump(4);
        file.close();
    }
    else
    {
        wxLogError("Unable to open file for saving conversation.");
    }
}

void Chat::LoadConversation()
{
    std::ifstream file(filePath.ToUTF8().data());
    if (file.is_open())
    {
        nlohmann::json conversationJson;
        file >> conversationJson;
        file.close();

        // Load conversation parameters
        conversationName = wxString::FromUTF8(conversationJson["Name"].get<std::string>().c_str());
        chatHistory = conversationJson["chatHistory"].get<std::vector<nlohmann::json>>();

        // Load model parameters
        modelParams->SystemMessageEnabled = conversationJson["modelParams"]["SystemMessageEnabled"].get<bool>();
        modelParams->SystemMessage = conversationJson["modelParams"]["SystemMessage"].get<std::string>();
        modelParams->TemplateEnabled = conversationJson["modelParams"]["TemplateEnabled"].get<bool>();
        modelParams->Template = conversationJson["modelParams"]["Template"].get<std::string>();
        modelParams->SeedEnabled = conversationJson["modelParams"]["SeedEnabled"].get<bool>();
        modelParams->Seed = conversationJson["modelParams"]["Seed"].get<int>();
        modelParams->TemperatureEnabled = conversationJson["modelParams"]["TemperatureEnabled"].get<bool>();
        modelParams->Temperature = conversationJson["modelParams"]["Temperature"].get<float>();
        modelParams->ContextSizeEnabled = conversationJson["modelParams"]["ContextSizeEnabled"].get<bool>();
        modelParams->ContextSize = conversationJson["modelParams"]["ContextSize"].get<int>();
        modelParams->PredictTokensEnabled = conversationJson["modelParams"]["PredictTokensEnabled"].get<bool>();
        modelParams->PredictTokens = conversationJson["modelParams"]["PredictTokens"].get<int>();
        modelParams->RepeatLastEnabled = conversationJson["modelParams"]["RepeatLastEnabled"].get<bool>();
        modelParams->RepeatLast = conversationJson["modelParams"]["RepeatLast"].get<int>();
        modelParams->RepeatPenaltyEnabled = conversationJson["modelParams"]["RepeatPenaltyEnabled"].get<bool>();
        modelParams->RepeatPenalty = conversationJson["modelParams"]["RepeatPenalty"].get<float>();
        modelParams->TopKEnabled = conversationJson["modelParams"]["TopKEnabled"].get<bool>();
        modelParams->TopK = conversationJson["modelParams"]["TopK"].get<int>();
        modelParams->TopPEnabled = conversationJson["modelParams"]["TopPEnabled"].get<bool>();
        modelParams->TopP = conversationJson["modelParams"]["TopP"].get<float>();
        modelParams->TFSZEnabled = conversationJson["modelParams"]["TFSZEnabled"].get<bool>();
        modelParams->TFSZ = conversationJson["modelParams"]["TFSZ"].get<float>();
        modelParams->MirostatEnabled = conversationJson["modelParams"]["MirostatEnabled"].get<bool>();
        modelParams->Mirostat = conversationJson["modelParams"]["Mirostat"].get<int>();
        modelParams->MirostatTauEnabled = conversationJson["modelParams"]["MirostatTauEnabled"].get<bool>();
        modelParams->MirostatTau = conversationJson["modelParams"]["MirostatTau"].get<float>();
        modelParams->MirostatEtaEnabled = conversationJson["modelParams"]["MirostatEtaEnabled"].get<bool>();
        modelParams->MirostatEta = conversationJson["modelParams"]["MirostatEta"].get<float>();
        modelParams->KeepTokensEnabled = conversationJson["modelParams"]["KeepTokensEnabled"].get<bool>();
        modelParams->KeepTokens = conversationJson["modelParams"]["KeepTokens"].get<int>();
        modelParams->TypicalPEnabled = conversationJson["modelParams"]["TypicalPEnabled"].get<bool>();
        modelParams->TypicalP = conversationJson["modelParams"]["TypicalP"].get<float>();
        modelParams->PresencePenaltyEnabled = conversationJson["modelParams"]["PresencePenaltyEnabled"].get<bool>();
        modelParams->PresencePenalty = conversationJson["modelParams"]["PresencePenalty"].get<float>();
        modelParams->FrequencyPenaltyEnabled = conversationJson["modelParams"]["FrequencyPenaltyEnabled"].get<bool>();
        modelParams->FrequencyPenalty = conversationJson["modelParams"]["FrequencyPenalty"].get<float>();
        modelParams->PenalizeNewLinesEnabled = conversationJson["modelParams"]["PenalizeNewLinesEnabled"].get<bool>();
        modelParams->PenalizeNewLines = conversationJson["modelParams"]["PenalizeNewLines"].get<bool>();

        RewriteChatHistory();
    }
    else
    {
        wxLogError("Unable to open file for loading conversation.");
    }
}

void Chat::ClearConversation()
{
    richTextChatBox->Clear();
    chatHistory.clear();
}

bool Chat::HasImage()
{
    return !encodedImage.IsEmpty();
}

wxString Chat::GetEncodedImage()
{
    return encodedImage;
}

nlohmann::json Chat::MakeRequestBody(const wxString& userMessage) const
{
    nlohmann::json requestBody = {
        {"model", modelName.ToStdString()},
        {"messages", chatHistory},
        {"stream", serverSettings.GetStreaming()},
        {"keep_alive", serverSettings.GetKeepAliveMinutesString()}
    };

    if (modelParams->SystemMessageEnabled)
    {
        nlohmann::json systemMessage = {
            {"role", "system"},
            {"content", modelParams->SystemMessage}
        };
        requestBody["messages"].insert(requestBody["messages"].begin(), systemMessage);
    }

    if (modelParams->TemplateEnabled)
    {
        requestBody["template"] = modelParams->Template;
    }

    nlohmann::json options;

    if (modelParams->SeedEnabled)
    {
        options["seed"] = modelParams->Seed;
    }

    if (modelParams->TemperatureEnabled)
    {
        options["temperature"] = modelParams->Temperature;
    }

    if (modelParams->ContextSizeEnabled)
    {
        options["num_ctx"] = modelParams->ContextSize;
    }

    if (modelParams->PredictTokensEnabled)
    {
        options["num_predict"] = modelParams->PredictTokens;
    }

    if (modelParams->RepeatLastEnabled)
    {
        options["repeat_last_n"] = modelParams->RepeatLast;
    }

    if (modelParams->RepeatPenaltyEnabled)
    {
        options["repeat_penalty"] = modelParams->RepeatPenalty;
    }

    if (modelParams->TopKEnabled)
    {
        options["top_k"] = modelParams->TopK;
    }

    if (modelParams->TopPEnabled)
    {
        options["top_p"] = modelParams->TopP;
    }

    if (modelParams->TFSZEnabled)
    {
        options["tfs_z"] = modelParams->TFSZ;
    }

    if (modelParams->MirostatEnabled)
    {
        options["mirostat"] = modelParams->Mirostat;
    }

    if (modelParams->MirostatTauEnabled)
    {
        options["mirostat_tau"] = modelParams->MirostatTau;
    }

    if (modelParams->MirostatEtaEnabled)
    {
        options["mirostat_eta"] = modelParams->MirostatEta;
    }

    if (modelParams->KeepTokensEnabled)
    {
        options["num_keep"] = modelParams->KeepTokens;
    }

    if (modelParams->TypicalPEnabled)
    {
        options["typical_p"] = modelParams->TypicalP;
    }

    if (modelParams->PresencePenaltyEnabled)
    {
        options["presence_penalty"] = modelParams->PresencePenalty;
    }

    if (modelParams->FrequencyPenaltyEnabled)
    {
        options["frequency_penalty"] = modelParams->FrequencyPenalty;
    }

    if (modelParams->PenalizeNewLinesEnabled)
    {
        options["penalize_newline"] = modelParams->PenalizeNewLines;
    }

    requestBody["options"] = options;

    return requestBody;
}

void Chat::AppendMessage(const wxString& message, const wxString& role, bool reformat)
{
    wxMutexGuiEnter();

    wxTextAttr style;
    style.SetFont(richTextChatBox->GetFont());

    if (role == "user")
    {
        style.SetAlignment(wxTEXT_ALIGNMENT_LEFT);
        style.SetBackgroundColour(wxColour("#4A67CF"));
        style.SetTextColour(wxColour("#FFFFFF"));
        style.SetLeftIndent(20);
        style.SetRightIndent(5);
        style.SetParagraphSpacingBefore(20);
        style.SetParagraphSpacingAfter(15);
    }
    else
    {
        style.SetAlignment(wxTEXT_ALIGNMENT_LEFT);
        style.SetBackgroundColour(wxColour("#2B651A"));
        style.SetTextColour(wxColour("#FFFFFF"));
        style.SetLeftIndent(5);
        style.SetRightIndent(20);
    }

    richTextChatBox->Freeze();

    richTextChatBox->SetInsertionPointEnd();

    if (reformat)
    {
        long startPos = streamingStartPos != -1 ? streamingStartPos : richTextChatBox->GetLastPosition();
        richTextChatBox->Delete(wxRichTextRange(startPos, richTextChatBox->GetLastPosition()));

        wxString mes = FormatMessageString(message);
        Markdown::WriteText(richTextChatBox, mes.IsEmpty() || mes.Last() == '\n' ? mes : mes + "\n", style);
        streamingStartPos = -1;
    }
    else
    {
        if (serverSettings.GetStreaming())
        {
            richTextChatBox->BeginStyle(style);
            richTextChatBox->WriteText(message);
            richTextChatBox->EndStyle();
        }
        else
        {
            wxString mes = FormatMessageString(message);
            Markdown::WriteText(richTextChatBox, mes.IsEmpty() || mes.Last() == '\n' ? mes : mes + "\n", style);
        }
    }

    richTextChatBox->SetInsertionPointEnd();
    richTextChatBox->ShowPosition(richTextChatBox->GetLastPosition());

    richTextChatBox->Thaw();

    wxMutexGuiLeave();
}

void Chat::AppendImage(const wxString& encodedImage)
{
    richTextChatBox->Newline();
    richTextChatBox->WriteImage(Base64::Decode(encodedImage));
    richTextChatBox->Newline();
}

void Chat::OnStreamingText(wxCommandEvent& event)
{
    if (streamingStartPos == -1)
    {
        streamingStartPos = richTextChatBox->GetLastPosition();
    }

    AppendMessage(event.GetString(), "assistant");
}

void Chat::OnTextReformat(wxCommandEvent& event)
{
    AppendMessage(event.GetString(), "assistant", true);
}

bool Chat::EndsWithNewline()
{
    bool endsWithNewline = chatHistory.empty() || richTextChatBox->IsEmpty();

    if (richTextChatBox->GetLastPosition() > 0)
    {
        wxString lastChar = richTextChatBox->GetRange(richTextChatBox->GetLastPosition() - 1, richTextChatBox->GetLastPosition());
        endsWithNewline = (lastChar == "\n");
    }

    return endsWithNewline;
}

wxString Chat::TrimTrailingWhitespace(const wxString& str)
{
    size_t endpos = str.find_last_not_of(" \t\n\r");
    if (wxString::npos != endpos)
    {
        return str.substr(0, endpos + 1);
    }

    return str;
}

wxString Chat::FormatMessageString(const wxString& message)
{
    wxString formattedMessage = message;

    if (!serverSettings.GetStreaming())
    {
        bool endsWithNewline = EndsWithNewline();

        formattedMessage = TrimTrailingWhitespace(formattedMessage);

        if (!endsWithNewline)
        {
            richTextChatBox->WriteText("\n");
        }
    }

    return formattedMessage;
}

void Chat::RewriteChatHistory()
{
    richTextChatBox->Clear();

    for (const auto& messageJson : chatHistory)
    {
        if (messageJson.contains("images") && !messageJson["images"].is_null())
        {
            for (const auto& image : messageJson["images"].get<std::vector<std::string>>())
            {
                AppendImage(wxString::FromUTF8(image));
            }
        }

        wxString mes = wxString::FromUTF8(messageJson["content"].get<std::string>().c_str());
        AppendMessage(mes.IsEmpty() || mes.Last() == '\n' ? mes : mes + "\n", wxString::FromUTF8(messageJson["role"].get<std::string>().c_str()));
    }
}
