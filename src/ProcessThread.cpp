/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ProcessThread.h"

#include <wx/log.h>

#ifdef __WXGTK__
    #include <wx/filename.h>    // wxDirExists
#elif __WXMSW__
    #include <windows.h>        // Windows process APIs
#elif __WXOSX__
    #include <unistd.h>         // access()
    #include <sys/types.h>      // pid_t
    #include <signal.h>         // kill()
    #include <errno.h>          // errno
#endif

ProcessThread::ProcessThread(wxEvtHandler* parent) : stopThread(false), parent(parent), process(nullptr), pid(0), pendingDelete(false)
{
}

ProcessThread::~ProcessThread()
{
    Stop();
}

void ProcessThread::Start(const wxString& command)
{
    stopThread.store(false);
    pid.store(0);
    thread = std::thread(&ProcessThread::ProcessHandler, this);

    wxCommandEvent evt(wxEVT_COMMAND_BUTTON_CLICKED, wxID_ANY);
    evt.SetString(command);
    wxQueueEvent(parent, evt.Clone());
}

void ProcessThread::Stop()
{
    stopThread.store(true);
    cv.notify_all();

    if (thread.joinable())
    {
        thread.join();
    }

    std::unique_lock<std::mutex> lock(mutex);

    if (process && !pendingDelete)
    {
        wxLogDebug("Queue stop. Terminating process...");

        TerminateProcess();

        wxLogDebug("Detaching and deleting process...");
        process->Detach();
        pendingDelete = true;
        wxQueueEvent(parent, new wxThreadEvent(wxEVT_THREAD, wxID_ANY));
        pid.store(0);
    }

    wxLogDebug("ProcessThread::Stop completed.");
}

long ProcessThread::GetPID() const
{
    return pid.load();
}

void ProcessThread::SetPID(long pid)
{
    this->pid.store(pid);
}

wxProcess* ProcessThread::GetProcess()
{
    return process;
}

void ProcessThread::SetProcess(wxProcess* process)
{
    std::lock_guard<std::mutex> lock(mutex);
    this->process = process;
}

bool ProcessThread::GetPendingDelete()
{
    return pendingDelete;
}

void ProcessThread::SetPendingDelete(bool pendingDelete)
{
    this->pendingDelete = pendingDelete;
}

void ProcessThread::ProcessHandler()
{
    wxLogDebug("ProcessHandler started.");

    while (pid.load() == 0 && !stopThread.load())
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    if (stopThread.load())
    {
        wxLogDebug("ProcessHandler exiting early due to stop request.");
        return;
    }

    while (!stopThread.load())
    {
        if (!CheckProcessExists())
        {
            wxLogWarning("Process terminated unexpectedly.");
            break;
        }

        std::unique_lock<std::mutex> lock(mutex);
        cv.wait_for(lock, std::chrono::milliseconds(100));
    }

    if (stopThread.load())
    {
        std::lock_guard<std::mutex> lock(mutex);

        TerminateProcess();
    }

    wxLogDebug("ProcessHandler completed.");
}

bool ProcessThread::CheckProcessExists()
{
    bool exists = false;

    #ifdef __WXGTK__     // Linux-specific code
        wxString procPath = wxString::Format(wxT("/proc/%ld"), pid.load());
        exists = wxDirExists(procPath);
    #elif __WXMSW__     // Windows-specific code
        DWORD processID = (DWORD)pid.load();
        HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, processID);
        if (hProcess != NULL)
        {
            DWORD exitCode;
            if (GetExitCodeProcess(hProcess, &exitCode) && exitCode == STILL_ACTIVE)
            {
                exists = true;
            }
            CloseHandle(hProcess);
        }
    #elif __WXOSX__     // macOS-specific code
        pid_t processID = (pid_t)pid.load();
        if (kill(processID, 0) == 0 || errno != ESRCH)
        {
            exists = true;
        }
    #else               // Unsupported platform
        wxLogError("Process existence check is not supported on this platform.");
    #endif

    return exists;
}

void ProcessThread::TerminateProcess()
{
    long localPID = pid.load();
    if (localPID > 0 && CheckProcessExists())
    {
        wxKillError status;

        wxLogDebug("Stopping process with PID: %ld.", localPID);

        if (wxKill(localPID, wxSIGTERM, &status) == wxKILL_OK)
        {
            wxLogDebug("Successfully sent SIGTERM to process with PID: %ld, status: %s.", localPID, GetStatus(status).ToStdString());
        }
        else
        {
            wxLogWarning("Failed to send SIGTERM to process with PID: %ld, status: %s.", localPID, GetStatus(status).ToStdString());
        }

        std::this_thread::sleep_for(std::chrono::seconds(2)); // Allow graceful shutdown

        if (CheckProcessExists())
        {
            if (wxKill(localPID, wxSIGKILL, &status) == wxKILL_OK)
            {
                wxLogDebug("Successfully sent SIGKILL to process with PID: %ld, status: %s.", localPID, GetStatus(status).ToStdString());
            }
            else
            {
                wxLogWarning("Failed to send SIGKILL to process with PID: %ld, status: %s.", localPID, GetStatus(status).ToStdString());
            }
        }

        pid.store(0);
    }
}

wxString ProcessThread::GetStatus(wxKillError status)
{
    wxString statusString;

    switch (status)
    {
        case wxKILL_OK: statusString = "OK"; break;
        case wxKILL_BAD_SIGNAL: statusString = "Bad signal"; break;
        case wxKILL_ACCESS_DENIED: statusString = "Permission denied"; break;
        case wxKILL_NO_PROCESS: statusString = "No such process"; break;
        case wxKILL_ERROR: statusString = "Unspecified error"; break;
        default: statusString = "Unknown status";
    }

    return statusString;
}
