/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogServerSettings.h"

#include <wx/log.h>

DialogServerSettings::DialogServerSettings(wxWindow* parent, ServerSettings* serverSettings) : DialogServerSettingsView(parent), serverSettings(serverSettings)
{
    filePickerOllama->SetPath(serverSettings->GetPath());
    textCtrlHost->ChangeValue(serverSettings->GetHost());
    spinCtrlKeepAlive->SetValue(serverSettings->GetKeepAliveMinutes());
    spinCtrlTimeout->SetValue(serverSettings->GetTimeoutSeconds());
    checkBoxStreaming->SetValue(serverSettings->GetStreaming());

    initialSettings = *serverSettings;
}

DialogServerSettings::~DialogServerSettings()
{
}

void DialogServerSettings::OnServerSettingsOKClick(wxCommandEvent& event)
{
    serverSettings->SetPath(filePickerOllama->GetPath().ToUTF8().data());
    serverSettings->SetHost(textCtrlHost->GetValue().ToUTF8().data());
    serverSettings->SetKeepAliveMinutes(spinCtrlKeepAlive->GetValue());
    serverSettings->SetTimeoutSeconds(spinCtrlTimeout->GetValue());
    serverSettings->SetStreaming(checkBoxStreaming->IsChecked());

    if(initialSettings != *serverSettings)
    {
        try
        {
            serverSettings->Save();
            wxLogDebug("Server setting saved at: %s", serverSettings->GetConfigPath());
        }
        catch (const std::exception& e)
        {
            wxLogError("Cannot save server settings: %s", e.what());
        }
    }

    Close();
}

void DialogServerSettings::OnDefaultClick(wxCommandEvent& event)
{
    filePickerOllama->SetPath("ollama");
    textCtrlHost->ChangeValue("http://localhost:11434");
    spinCtrlKeepAlive->SetValue(20);
    spinCtrlTimeout->SetValue(120);
    checkBoxStreaming->SetValue(true);
}
