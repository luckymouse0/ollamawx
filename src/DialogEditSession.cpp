/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DialogEditSession.h"

#include <wx/msgdlg.h>

DialogEditSession::DialogEditSession(wxWindow* parent, ModelParams* modelParams, std::string originalSystemMessage, std::string originalTemplate) : DialogEditSessionView(parent), modelParams(modelParams), originalSystemMessage(originalSystemMessage), originalTemplate(originalTemplate), hasChanged(false)
{
    checkBoxEnableSystemMessage->SetValue(modelParams->SystemMessageEnabled);
    textCtrlSystemMessage->ChangeValue(modelParams->SystemMessage);
    checkBoxEnableTemplate->SetValue(modelParams->TemplateEnabled);
    textCtrlTemplate->ChangeValue(modelParams->Template);
    checkBoxEnableSeed->SetValue(modelParams->SeedEnabled);
    spinCtrlSeed->SetValue(modelParams->Seed);
    checkBoxEnableTemperature->SetValue(modelParams->TemperatureEnabled);
    spinCtrlDoubleTemperature->SetValue(modelParams->Temperature);
    checkBoxEnableContextSize->SetValue(modelParams->ContextSizeEnabled);
    spinCtrlContextSize->SetValue(modelParams->ContextSize);
    checkBoxEnablePredictTokens->SetValue(modelParams->PredictTokensEnabled);
    spinCtrlPredictTokens->SetValue(modelParams->PredictTokens);
    checkBoxEnableRepeatLast->SetValue(modelParams->RepeatLastEnabled);
    spinCtrlRepeatLast->SetValue(modelParams->RepeatLast);
    checkBoxEnableRepeatPenalty->SetValue(modelParams->RepeatPenaltyEnabled);
    spinCtrlDoubleRepeatPenalty->SetValue(modelParams->RepeatPenalty);
    checkBoxEnableTopK->SetValue(modelParams->TopKEnabled);
    spinCtrlTopK->SetValue(modelParams->TopK);
    checkBoxEnableTopP->SetValue(modelParams->TopPEnabled);
    spinCtrlDoubleTopP->SetValue(modelParams->TopP);
    checkBoxEnableTFSZ->SetValue(modelParams->TFSZEnabled);
    spinCtrlDoubleTFSZ->SetValue(modelParams->TFSZ);
    checkBoxEnableMirostat->SetValue(modelParams->MirostatEnabled);
    choiceMirostat->SetSelection(modelParams->Mirostat);
    checkBoxEnableMirostatTau->SetValue(modelParams->MirostatTauEnabled);
    spinCtrlDoubleMirostatTau->SetValue(modelParams->MirostatTau);
    checkBoxEnableMirostatEta->SetValue(modelParams->MirostatEtaEnabled);
    spinCtrlDoubleMirostatEta->SetValue(modelParams->MirostatEta);
    checkBoxEnableKeepTokens->SetValue(modelParams->KeepTokensEnabled);
    spinCtrlKeepTokens->SetValue(modelParams->KeepTokens);
    checkBoxEnableTypicalP->SetValue(modelParams->TypicalPEnabled);
    spinCtrlDoubleTypicalP->SetValue(modelParams->TypicalP);
    checkBoxEnablePenalizeNewlines->SetValue(modelParams->PenalizeNewLinesEnabled);
    checkBoxPenalizeNewlines->SetValue(modelParams->PenalizeNewLines);
    checkBoxEnablePresencePenalty->SetValue(modelParams->PresencePenaltyEnabled);
    spinCtrlDoublePresencePenalty->SetValue(modelParams->PresencePenalty);
    checkBoxEnableFrequencyPenalty->SetValue(modelParams->FrequencyPenaltyEnabled);
    spinCtrlDoubleFrequencyPenalty->SetValue(modelParams->FrequencyPenalty);

    EnableControls();
}

void DialogEditSession::OnEditSessionOKClick(wxCommandEvent& event)
{
    if (hasChanged)
    {
        if (wxMessageBox(wxT("Do you want to save the changes and exit?"), wxT("Save parameters"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            SaveParams();
            EndModal(wxID_OK);
        }
    }
    else
    {
        EndModal(wxID_OK);
    }
}

void DialogEditSession::OnEditSessionApplyClick(wxCommandEvent& event)
{
    if (hasChanged)
    {
        if (wxMessageBox(wxT("Do you want to save the changes?"), wxT("Save parameters"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            SaveParams();
            hasChanged = false;
        }
    }
}

void DialogEditSession::OnEditSessionCancelClick(wxCommandEvent& event)
{
    if (hasChanged)
    {
        if (wxMessageBox(wxT("Do you want to exit without saving?"), wxT("Save parameters"), wxYES_NO | wxICON_QUESTION) == wxYES)
        {
            EndModal(wxID_CANCEL);
        }
    }
    else
    {
        EndModal(wxID_CANCEL);
    }
}

void DialogEditSession::OnEditSessionDefaultClick(wxCommandEvent& event)
{
    if (wxMessageBox(wxT("Do you want to change values to default and lose current changes?"), wxT("Default parameters"), wxYES_NO | wxICON_QUESTION) == wxYES)
    {
        hasChanged = true;

        ModelParams defaultModelParams;
        defaultModelParams.SystemMessage = originalSystemMessage;
        defaultModelParams.Template = originalTemplate;

        checkBoxEnableSystemMessage->SetValue(defaultModelParams.SystemMessageEnabled);
        textCtrlSystemMessage->ChangeValue(defaultModelParams.SystemMessage);
        checkBoxEnableTemplate->SetValue(defaultModelParams.TemplateEnabled);
        textCtrlTemplate->ChangeValue(defaultModelParams.Template);
        checkBoxEnableSeed->SetValue(defaultModelParams.SeedEnabled);
        spinCtrlSeed->SetValue(defaultModelParams.Seed);
        checkBoxEnableTemperature->SetValue(defaultModelParams.TemperatureEnabled);
        spinCtrlDoubleTemperature->SetValue(defaultModelParams.Temperature);
        checkBoxEnableContextSize->SetValue(defaultModelParams.ContextSizeEnabled);
        spinCtrlContextSize->SetValue(defaultModelParams.ContextSize);
        checkBoxEnablePredictTokens->SetValue(defaultModelParams.PredictTokensEnabled);
        spinCtrlPredictTokens->SetValue(defaultModelParams.PredictTokens);
        checkBoxEnableRepeatLast->SetValue(defaultModelParams.RepeatLastEnabled);
        spinCtrlRepeatLast->SetValue(defaultModelParams.RepeatLast);
        checkBoxEnableRepeatPenalty->SetValue(defaultModelParams.RepeatPenaltyEnabled);
        spinCtrlDoubleRepeatPenalty->SetValue(defaultModelParams.RepeatPenalty);
        checkBoxEnableTopK->SetValue(defaultModelParams.TopKEnabled);
        spinCtrlTopK->SetValue(defaultModelParams.TopK);
        checkBoxEnableTopP->SetValue(defaultModelParams.TopPEnabled);
        spinCtrlDoubleTopP->SetValue(defaultModelParams.TopP);
        checkBoxEnableTFSZ->SetValue(defaultModelParams.TFSZEnabled);
        spinCtrlDoubleTFSZ->SetValue(defaultModelParams.TFSZ);
        checkBoxEnableMirostat->SetValue(defaultModelParams.MirostatEnabled);
        choiceMirostat->SetSelection(defaultModelParams.Mirostat);
        checkBoxEnableMirostatTau->SetValue(defaultModelParams.MirostatTauEnabled);
        spinCtrlDoubleMirostatTau->SetValue(defaultModelParams.MirostatTau);
        checkBoxEnableMirostatEta->SetValue(defaultModelParams.MirostatEtaEnabled);
        spinCtrlDoubleMirostatEta->SetValue(defaultModelParams.MirostatEta);
        checkBoxEnableKeepTokens->SetValue(defaultModelParams.KeepTokensEnabled);
        spinCtrlKeepTokens->SetValue(defaultModelParams.KeepTokens);
        checkBoxEnableTypicalP->SetValue(defaultModelParams.TypicalPEnabled);
        spinCtrlDoubleTypicalP->SetValue(defaultModelParams.TypicalP);
        checkBoxEnablePenalizeNewlines->SetValue(defaultModelParams.PenalizeNewLinesEnabled);
        checkBoxPenalizeNewlines->SetValue(defaultModelParams.PenalizeNewLines);
        checkBoxEnablePresencePenalty->SetValue(defaultModelParams.PresencePenaltyEnabled);
        spinCtrlDoublePresencePenalty->SetValue(defaultModelParams.PresencePenalty);
        checkBoxEnableFrequencyPenalty->SetValue(defaultModelParams.FrequencyPenaltyEnabled);
        spinCtrlDoubleFrequencyPenalty->SetValue(defaultModelParams.FrequencyPenalty);

        EnableControls();
    }
}

void DialogEditSession::OnTextChanged(wxCommandEvent& event)
{
    hasChanged = true;
}

void DialogEditSession::OnSpinCtrlChanged(wxSpinEvent& event)
{
    hasChanged = true;
}

void DialogEditSession::OnSpinCtrlDoubleChanged(wxSpinDoubleEvent& event)
{
    hasChanged = true;
}

void DialogEditSession::OnChoiceChanged(wxCommandEvent& event)
{
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxChanged(wxCommandEvent& event)
{
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableSystemMessage(wxCommandEvent& event)
{
    textCtrlSystemMessage->Enable(checkBoxEnableSystemMessage->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTemplate(wxCommandEvent& event)
{
    textCtrlTemplate->Enable(checkBoxEnableTemplate->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableSeed(wxCommandEvent& event)
{
    spinCtrlSeed->Enable(checkBoxEnableSeed->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTemperature(wxCommandEvent& event)
{
    spinCtrlDoubleTemperature->Enable(checkBoxEnableTemperature->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableContextSize(wxCommandEvent& event)
{
    spinCtrlContextSize->Enable(checkBoxEnableContextSize->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnablePredictTokens(wxCommandEvent& event)
{
    spinCtrlPredictTokens->Enable(checkBoxEnablePredictTokens->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableRepeatLast(wxCommandEvent& event)
{
    spinCtrlRepeatLast->Enable(checkBoxEnableRepeatLast->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableRepeatPenalty(wxCommandEvent& event)
{
    spinCtrlDoubleRepeatPenalty->Enable(checkBoxEnableRepeatPenalty->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTopK(wxCommandEvent& event)
{
    spinCtrlTopK->Enable(checkBoxEnableTopK->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTopP(wxCommandEvent& event)
{
    spinCtrlDoubleTopP->Enable(checkBoxEnableTopP->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTFSZ(wxCommandEvent& event)
{
    spinCtrlDoubleTFSZ->Enable(checkBoxEnableTFSZ->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableMirostat(wxCommandEvent& event)
{
    choiceMirostat->Enable(checkBoxEnableMirostat->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableMirostatTau(wxCommandEvent& event)
{
    spinCtrlDoubleMirostatTau->Enable(checkBoxEnableMirostatTau->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableMirostatEta(wxCommandEvent& event)
{
    spinCtrlDoubleMirostatEta->Enable(checkBoxEnableMirostatEta->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableKeepTokens(wxCommandEvent& event)
{
    spinCtrlKeepTokens->Enable(checkBoxEnableKeepTokens->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableTypicalP(wxCommandEvent& event)
{
    spinCtrlDoubleTypicalP->Enable(checkBoxEnableTypicalP->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnablePenalizeNewlines(wxCommandEvent& event)
{
    checkBoxPenalizeNewlines->Enable(checkBoxEnablePenalizeNewlines->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnablePresencePenalty(wxCommandEvent& event)
{
    spinCtrlDoublePresencePenalty->Enable(checkBoxEnablePresencePenalty->IsChecked());
    hasChanged = true;
}

void DialogEditSession::OnCheckBoxEnableFrequencyPenalty(wxCommandEvent& event)
{
    spinCtrlDoubleFrequencyPenalty->Enable(checkBoxEnableFrequencyPenalty->IsChecked());
    hasChanged = true;
}

void DialogEditSession::SaveParams()
{
    modelParams->SystemMessageEnabled = checkBoxEnableSystemMessage->IsChecked();
    if (textCtrlSystemMessage->IsEnabled())
    {
        modelParams->SystemMessage = textCtrlSystemMessage->GetValue();
    }

    modelParams->TemplateEnabled = checkBoxEnableTemplate->IsChecked();
    if (textCtrlTemplate->IsEnabled())
    {
        modelParams->Template = textCtrlTemplate->GetValue();
    }

    modelParams->SeedEnabled = checkBoxEnableSeed->IsChecked();
    if (spinCtrlSeed->IsEnabled())
    {
        modelParams->Seed = spinCtrlSeed->GetValue();
    }

    modelParams->TemperatureEnabled = checkBoxEnableTemperature->IsChecked();
    if (spinCtrlDoubleTemperature->IsEnabled())
    {
        modelParams->Temperature = spinCtrlDoubleTemperature->GetValue();
    }

    modelParams->ContextSizeEnabled = checkBoxEnableContextSize->IsChecked();
    if (spinCtrlContextSize->IsEnabled())
    {
        modelParams->ContextSize = spinCtrlContextSize->GetValue();
    }

    modelParams->PredictTokensEnabled = checkBoxEnablePredictTokens->IsChecked();
    if (spinCtrlPredictTokens->IsEnabled())
    {
        modelParams->PredictTokens = spinCtrlPredictTokens->GetValue();
    }

    modelParams->RepeatLastEnabled = checkBoxEnableRepeatLast->IsChecked();
    if (spinCtrlRepeatLast->IsEnabled())
    {
        modelParams->RepeatLast = spinCtrlRepeatLast->GetValue();
    }

    modelParams->RepeatPenaltyEnabled = checkBoxEnableRepeatPenalty->IsChecked();
    if (spinCtrlDoubleRepeatPenalty->IsEnabled())
    {
        modelParams->RepeatPenalty = spinCtrlDoubleRepeatPenalty->GetValue();
    }

    modelParams->TopKEnabled = checkBoxEnableTopK->IsChecked();
    if (spinCtrlTopK->IsEnabled())
    {
        modelParams->TopK = spinCtrlTopK->GetValue();
    }

    modelParams->TopPEnabled = checkBoxEnableTopP->IsChecked();
    if (spinCtrlDoubleTopP->IsEnabled())
    {
        modelParams->TopP = spinCtrlDoubleTopP->GetValue();
    }

    modelParams->TFSZEnabled = checkBoxEnableTFSZ->IsChecked();
    if (spinCtrlDoubleTFSZ->IsEnabled())
    {
        modelParams->TFSZ = spinCtrlDoubleTFSZ->GetValue();
    }

    modelParams->MirostatEnabled = checkBoxEnableMirostat->IsChecked();
    if (choiceMirostat->IsEnabled())
    {
        modelParams->Mirostat = choiceMirostat->GetSelection();
    }

    modelParams->MirostatTauEnabled = checkBoxEnableMirostatTau->IsChecked();
    if (spinCtrlDoubleMirostatTau->IsEnabled())
    {
        modelParams->MirostatTau = spinCtrlDoubleMirostatTau->GetValue();
    }

    modelParams->MirostatEtaEnabled = checkBoxEnableMirostatEta->IsChecked();
    if (spinCtrlDoubleMirostatEta->IsEnabled())
    {
        modelParams->MirostatEta = spinCtrlDoubleMirostatEta->GetValue();
    }

    modelParams->KeepTokensEnabled = checkBoxEnableKeepTokens->IsChecked();
    if (spinCtrlKeepTokens->IsEnabled())
    {
        modelParams->KeepTokens = spinCtrlKeepTokens->GetValue();
    }

    modelParams->TypicalPEnabled = checkBoxEnableTypicalP->IsChecked();
    if (spinCtrlDoubleTypicalP->IsEnabled())
    {
        modelParams->TypicalP = spinCtrlDoubleTypicalP->GetValue();
    }

    modelParams->PenalizeNewLinesEnabled = checkBoxEnablePenalizeNewlines->IsChecked();
    if (checkBoxPenalizeNewlines->IsEnabled())
    {
        modelParams->PenalizeNewLines = checkBoxPenalizeNewlines->IsEnabled();
    }

    modelParams->PresencePenaltyEnabled = checkBoxEnablePresencePenalty->IsChecked();
    if (spinCtrlDoublePresencePenalty->IsEnabled())
    {
        modelParams->PresencePenalty = spinCtrlDoublePresencePenalty->GetValue();
    }

    modelParams->FrequencyPenaltyEnabled = checkBoxEnableFrequencyPenalty->IsChecked();
    if (spinCtrlDoubleFrequencyPenalty->IsEnabled())
    {
        modelParams->FrequencyPenalty = spinCtrlDoubleFrequencyPenalty->GetValue();
    }
}

void DialogEditSession::EnableControls()
{
    textCtrlSystemMessage->Enable(checkBoxEnableSystemMessage->IsChecked());
    textCtrlTemplate->Enable(checkBoxEnableTemplate->IsChecked());
    spinCtrlSeed->Enable(checkBoxEnableSeed->IsChecked());
    spinCtrlDoubleTemperature->Enable(checkBoxEnableTemperature->IsChecked());
    spinCtrlContextSize->Enable(checkBoxEnableContextSize->IsChecked());
    spinCtrlPredictTokens->Enable(checkBoxEnablePredictTokens->IsChecked());
    spinCtrlRepeatLast->Enable(checkBoxEnableRepeatLast->IsChecked());
    spinCtrlDoubleRepeatPenalty->Enable(checkBoxEnableRepeatPenalty->IsChecked());
    spinCtrlTopK->Enable(checkBoxEnableTopK->IsChecked());
    spinCtrlDoubleTopP->Enable(checkBoxEnableTopP->IsChecked());
    spinCtrlDoubleTFSZ->Enable(checkBoxEnableTFSZ->IsChecked());
    choiceMirostat->Enable(checkBoxEnableMirostat->IsChecked());
    spinCtrlDoubleMirostatTau->Enable(checkBoxEnableMirostatTau->IsChecked());
    spinCtrlDoubleMirostatEta->Enable(checkBoxEnableMirostatEta->IsChecked());
    spinCtrlKeepTokens->Enable(checkBoxEnableKeepTokens->IsChecked());
    spinCtrlDoubleTypicalP->Enable(checkBoxEnableTypicalP->IsChecked());
    checkBoxPenalizeNewlines->Enable(checkBoxEnablePenalizeNewlines->IsChecked());
    spinCtrlDoublePresencePenalty->Enable(checkBoxEnablePresencePenalty->IsChecked());
    spinCtrlDoubleFrequencyPenalty->Enable(checkBoxEnableFrequencyPenalty->IsChecked());
}
