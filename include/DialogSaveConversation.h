/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGSAVECONVERSATION_H
#define DIALOGSAVECONVERSATION_H

#include "Views.h"

#include <wx/string.h>

class DialogSaveConversation : public DialogSaveConversationView
{
    public:
        DialogSaveConversation(wxWindow* parent, wxString* filename, wxString* conversationName);
        ~DialogSaveConversation() = default;
        void OnSaveConversationOKClick(wxCommandEvent& event) override;
        void OnSaveConversationCancelClick(wxCommandEvent& event) override;

    private:
        wxString* filename;
        wxString* conversationName;
};

#endif // DIALOGSAVECONVERSATION_H
