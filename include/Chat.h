/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef CHAT_H
#define CHAT_H

#include <cstdint>
#include <vector>

#include "ServerSettings.h"
#include "ModelParams.h"

#include <wx/wx.h>
#include <wx/event.h>
#include <wx/string.h>
#include <wx/image.h>
#include <wx/richtext/richtextctrl.h>
#include <nlohmann/json.hpp>

class Chat : public wxEvtHandler
{
    public:
        Chat(const ServerSettings& serverSettings, const wxString& modelName, wxRichTextCtrl* richTextChatBox, ModelParams* modelParams, const wxString& conversationName = "", const wxString& filePath = "");
        ~Chat() = default;
        void SendMessage(const wxString& userMessage);
        void AttachImage(const wxString& filePath);
        wxString GetLastUserMessage() const;
        bool IsReady() const;
        wxString GetConversationName() const;
        void SetConversationName(const wxString& conversationName);
        wxString GetFilePath() const;
        void SetFilePath(const wxString& filePath);
        void SaveConversation();
        void LoadConversation();
        void ClearConversation();
        bool HasImage();
        wxString GetEncodedImage();

    private:
        nlohmann::json MakeRequestBody(const wxString& userMessage) const;
        void AppendMessage(const wxString& message, const wxString& role, bool reformat = false);
        void AppendImage(const wxString& encodedImage);
        void OnStreamingText(wxCommandEvent& event);
        void OnTextReformat(wxCommandEvent& event);
        bool EndsWithNewline();
        wxString TrimTrailingWhitespace(const wxString& str);
        wxString FormatMessageString(const wxString& message);
        void RewriteChatHistory();

        static const wxEventTypeTag<wxCommandEvent> wxEVT_COMMAND_TEXT_REFORMAT;

        ServerSettings serverSettings;
        wxString modelName;
        wxRichTextCtrl* richTextChatBox;
        std::vector<nlohmann::json> chatHistory;
        bool ready;
        ModelParams* modelParams;
        wxString conversationName;
        wxString filePath;
        long streamingStartPos;
        wxString encodedImage;
};

#endif // CHAT_H
