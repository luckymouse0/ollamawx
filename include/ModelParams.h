/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef MODELPARAMS_H
#define MODELPARAMS_H

#include <string>

struct ModelParams
{
    bool SystemMessageEnabled = false;
    std::string SystemMessage;
    bool TemplateEnabled = false;
    std::string Template;
    bool SeedEnabled = false;
    int Seed = -1;
    bool TemperatureEnabled = false;
    float Temperature = 0.80;
    bool ContextSizeEnabled = false;
    int ContextSize = 2048;
    bool PredictTokensEnabled = false;
    int PredictTokens = -1;
    bool RepeatLastEnabled = false;
    int RepeatLast = 64;
    bool RepeatPenaltyEnabled = false;
    float RepeatPenalty = 1.10;
    bool TopKEnabled = false;
    int TopK = 40;
    bool TopPEnabled = false;
    float TopP = 0.90;
    bool TFSZEnabled = false;
    float TFSZ = 1.00;
    bool MirostatEnabled = false;
    int Mirostat = 0;
    bool MirostatTauEnabled = false;
    float MirostatTau = 5.00;
    bool MirostatEtaEnabled = false;
    float MirostatEta = 0.10;
    bool KeepTokensEnabled = false;
    int KeepTokens = 4;
    bool TypicalPEnabled = false;
    float TypicalP = 1.00;
    bool PresencePenaltyEnabled = false;
    float PresencePenalty = 0.00;
    bool FrequencyPenaltyEnabled = false;
    float FrequencyPenalty = 0.00;
    bool PenalizeNewLinesEnabled = false;
    bool PenalizeNewLines = true;
};

#endif // MODELPARAMS_H
