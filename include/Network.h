/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef NETWORK_H
#define NETWORK_H

#include <string>
#include <functional>
#include <memory>

#include <cpp-httplib/httplib.h>
#include <wx/event.h>

class Network
{
    public:
        ~Network() = default;
        Network(const Network&) = delete;
        Network operator=(const Network&) = delete;
        Network(Network&&) noexcept = delete;
        Network& operator=(Network&&) noexcept = delete;
        static Network& GetInstance() noexcept;
        void InitializeClient(const std::string& host);
        std::string HTTPGet(const std::string& host, const std::string& requestPath, int maxRetries = 5, int delayMs = 1000);
        std::string HTTPPost(const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType, int timeoutSec = -1);
        void HTTPPostStream(const std::function<void(const std::string&)>& chunkHandler, const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType, int timeoutSec = -1);
        void HTTPDelete(const std::string& host, const std::string& requestPath, const std::string& requestPayload, const std::string& contentType);

    private:
        Network();

        std::string host;
        std::unique_ptr<httplib::Client> client;
};

#endif // NETWORK_H
