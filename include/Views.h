///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/statbmp.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/statline.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/sizer.h>
#include <wx/bmpcbox.h>
#include <wx/srchctrl.h>
#include <wx/listbox.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/textctrl.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/menu.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/filepicker.h>
#include <wx/spinctrl.h>
#include <wx/checkbox.h>
#include <wx/dialog.h>
#include <wx/choice.h>
#include <wx/gauge.h>
#include <wx/timer.h>
#include <wx/notebook.h>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class FrameMainView
///////////////////////////////////////////////////////////////////////////////
class FrameMainView : public wxFrame
{
	private:

	protected:
		wxPanel* panelMain;
		wxStaticBitmap* bitmapStatus;
		wxStaticLine* staticlineServerButtons;
		wxBitmapButton* bpButtonStart;
		wxBitmapButton* bpButtonStop;
		wxBitmapButton* bpButtonSettings;
		wxBitmapButton* bpButtonHelp;
		wxBitmapComboBox* bcomboBoxModel;
		wxBitmapButton* bpButtonUpdateModelList;
		wxBitmapButton* bpButtonRunModel;
		wxBitmapButton* bpButtonShowInfoModel;
		wxBitmapButton* bpButtonDeleteModel;
		wxBitmapButton* bpButtonLoadConversation;
		wxBitmapButton* bpButtonRenameConversation;
		wxBitmapButton* bpButtonDeleteConversation;
		wxSearchCtrl* searchCtrlFilter;
		wxListBox* listBoxConversation;
		wxRichTextCtrl* richTextChatBox;
		wxTextCtrl* textCtrlChatInput;
		wxBitmapButton* bpButtonCloseConversation;
		wxBitmapButton* bpButtonSaveCloseConversation;
		wxBitmapButton* bpButtonSaveConversation;
		wxBitmapButton* bpButtonEditSession;
		wxBitmapButton* bpButtonClearConversation;
		wxBitmapButton* bpButtonAttach;
		wxButton* buttonSend;
		wxPanel* panelMenuHelp;
		wxMenu* menuHelp;
		wxPanel* panelMenuAttach;
		wxMenu* menuAttach;

		// Virtual event handlers, override them in your derived class
		virtual void OnClose( wxCloseEvent& event ) { event.Skip(); }
		virtual void OnStartClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnStopClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSettingsClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnHelpClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnComboBoxModel( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnUpdateModelListClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRunModelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnShowInfoModelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteModelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLoadConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRenameConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDeleteConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSearchFilter( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChatInputKeyDown( wxKeyEvent& event ) { event.Skip(); }
		virtual void OnCloseConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveCloseConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditSessionClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnClearConversationClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAttachClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSendClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAboutMenuSelection( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnAttachImage( wxCommandEvent& event ) { event.Skip(); }


	public:

		FrameMainView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Ollamawx"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 1050,550 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );

		~FrameMainView();

		void panelMenuHelpOnContextMenu( wxMouseEvent &event )
		{
			panelMenuHelp->PopupMenu( menuHelp, event.GetPosition() );
		}

		void panelMenuAttachOnContextMenu( wxMouseEvent &event )
		{
			panelMenuAttach->PopupMenu( menuAttach, event.GetPosition() );
		}

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogServerSettingsView
///////////////////////////////////////////////////////////////////////////////
class DialogServerSettingsView : public wxDialog
{
	private:

	protected:
		wxPanel* panelServerSettings;
		wxStaticText* staticTextPath;
		wxFilePickerCtrl* filePickerOllama;
		wxStaticText* staticTextHost;
		wxTextCtrl* textCtrlHost;
		wxStaticText* staticTextKeepAlive;
		wxSpinCtrl* spinCtrlKeepAlive;
		wxStaticText* staticTextTimeout;
		wxSpinCtrl* spinCtrlTimeout;
		wxCheckBox* checkBoxStreaming;
		wxStdDialogButtonSizer* sdbSizerServerSettings;
		wxButton* sdbSizerServerSettingsOK;
		wxButton* sdbSizerServerSettingsCancel;
		wxButton* buttonDefault;

		// Virtual event handlers, override them in your derived class
		virtual void OnServerSettingsOKClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnDefaultClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		DialogServerSettingsView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Ollama Server Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogServerSettingsView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogModelInformationView
///////////////////////////////////////////////////////////////////////////////
class DialogModelInformationView : public wxDialog
{
	private:

	protected:
		wxPanel* panelModelInformation;
		wxStaticText* staticTextModelDetails;
		wxStaticText* staticTextFormat;
		wxTextCtrl* textCtrlFormat;
		wxStaticText* staticTextFamily;
		wxTextCtrl* textCtrlFamily;
		wxStaticText* staticTextFamilies;
		wxTextCtrl* textCtrlFamilies;
		wxStaticText* staticTextParameterSize;
		wxTextCtrl* textCtrlParameterSize;
		wxStaticText* staticTextQuantizationLevel;
		wxTextCtrl* textCtrlQuantizationLevel;
		wxStaticLine* staticlineModelInformation;
		wxRichTextCtrl* richTextModelInformation;
		wxButton* buttonModelfile;
		wxButton* buttonLicense;
		wxButton* buttonParameters;
		wxButton* buttonSystemMessage;
		wxButton* buttonPromptTemplate;
		wxStdDialogButtonSizer* sdbSizerModelInformation;
		wxButton* sdbSizerModelInformationOK;

		// Virtual event handlers, override them in your derived class
		virtual void OnModelfileClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnLicenseClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnParametersClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSystemMessageClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnPromptTemplateClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnModelInformationOKClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		DialogModelInformationView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Model Information"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogModelInformationView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogEditSessionView
///////////////////////////////////////////////////////////////////////////////
class DialogEditSessionView : public wxDialog
{
	private:

	protected:
		wxPanel* panelEditSession;
		wxCheckBox* checkBoxEnableSystemMessage;
		wxStaticText* staticTextSystemMessage;
		wxTextCtrl* textCtrlSystemMessage;
		wxCheckBox* checkBoxEnableTemplate;
		wxStaticText* staticTextTemplate;
		wxTextCtrl* textCtrlTemplate;
		wxStaticLine* staticlineConversationParameters;
		wxCheckBox* checkBoxEnableSeed;
		wxStaticText* staticTextSeed;
		wxSpinCtrl* spinCtrlSeed;
		wxCheckBox* checkBoxEnableTemperature;
		wxStaticText* staticTextTemperature;
		wxSpinCtrlDouble* spinCtrlDoubleTemperature;
		wxCheckBox* checkBoxEnableContextSize;
		wxStaticText* staticTextContextSize;
		wxSpinCtrl* spinCtrlContextSize;
		wxCheckBox* checkBoxEnablePredictTokens;
		wxStaticText* staticTextPredictTokens;
		wxSpinCtrl* spinCtrlPredictTokens;
		wxCheckBox* checkBoxEnableRepeatLast;
		wxStaticText* staticTextRepeatLast;
		wxSpinCtrl* spinCtrlRepeatLast;
		wxCheckBox* checkBoxEnableRepeatPenalty;
		wxStaticText* staticTextRepeatPenalty;
		wxSpinCtrlDouble* spinCtrlDoubleRepeatPenalty;
		wxCheckBox* checkBoxEnableTopK;
		wxStaticText* staticTextTopK;
		wxSpinCtrl* spinCtrlTopK;
		wxCheckBox* checkBoxEnableTopP;
		wxStaticText* staticTextTopP;
		wxSpinCtrlDouble* spinCtrlDoubleTopP;
		wxCheckBox* checkBoxEnableTFSZ;
		wxStaticText* staticTextTFSZ;
		wxSpinCtrlDouble* spinCtrlDoubleTFSZ;
		wxCheckBox* checkBoxEnableMirostat;
		wxStaticText* staticTextMirostat;
		wxChoice* choiceMirostat;
		wxCheckBox* checkBoxEnableMirostatTau;
		wxStaticText* staticTextMirostatTau;
		wxSpinCtrlDouble* spinCtrlDoubleMirostatTau;
		wxCheckBox* checkBoxEnableMirostatEta;
		wxStaticText* staticTextMirostatEta;
		wxSpinCtrlDouble* spinCtrlDoubleMirostatEta;
		wxStaticLine* staticlineParameters;
		wxCheckBox* checkBoxEnableKeepTokens;
		wxStaticText* staticTextKeepTokens;
		wxSpinCtrl* spinCtrlKeepTokens;
		wxCheckBox* checkBoxEnableTypicalP;
		wxStaticText* staticTextTypicalP;
		wxSpinCtrlDouble* spinCtrlDoubleTypicalP;
		wxCheckBox* checkBoxEnablePenalizeNewlines;
		wxCheckBox* checkBoxPenalizeNewlines;
		wxCheckBox* checkBoxEnablePresencePenalty;
		wxStaticText* staticTextPresencePenalty;
		wxSpinCtrlDouble* spinCtrlDoublePresencePenalty;
		wxCheckBox* checkBoxEnableFrequencyPenalty;
		wxStaticText* staticTextFrequencyPenalty;
		wxSpinCtrlDouble* spinCtrlDoubleFrequencyPenalty;
		wxStaticLine* staticlineExtraParameters;
		wxStdDialogButtonSizer* sdbSizerEditSession;
		wxButton* sdbSizerEditSessionOK;
		wxButton* sdbSizerEditSessionApply;
		wxButton* sdbSizerEditSessionCancel;
		wxButton* buttonDefault;

		// Virtual event handlers, override them in your derived class
		virtual void OnCheckBoxEnableSystemMessage( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnTextChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTemplate( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableSeed( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSpinCtrlChanged( wxSpinEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTemperature( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSpinCtrlDoubleChanged( wxSpinDoubleEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableContextSize( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnablePredictTokens( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableRepeatLast( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableRepeatPenalty( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTopK( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTopP( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTFSZ( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableMirostat( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnChoiceChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableMirostatTau( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableMirostatEta( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableKeepTokens( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableTypicalP( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSpinCtrlDoubleChangedP( wxSpinDoubleEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnablePenalizeNewlines( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxChanged( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnablePresencePenalty( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnCheckBoxEnableFrequencyPenalty( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditSessionApplyClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditSessionCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditSessionOKClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnEditSessionDefaultClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		DialogEditSessionView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Edit Conversation Session"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogEditSessionView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogModelDownloadView
///////////////////////////////////////////////////////////////////////////////
class DialogModelDownloadView : public wxDialog
{
	private:

	protected:
		wxPanel* panelModelDownload;
		wxStaticText* staticTextModelDownload;
		wxGauge* gaugeModelDownload;
		wxTimer timerModelDownload;

		// Virtual event handlers, override them in your derived class
		virtual void OnTimerModelDownload( wxTimerEvent& event ) { event.Skip(); }


	public:

		DialogModelDownloadView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Downloading model"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxCAPTION );

		~DialogModelDownloadView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogSaveConversationView
///////////////////////////////////////////////////////////////////////////////
class DialogSaveConversationView : public wxDialog
{
	private:

	protected:
		wxPanel* panelSaveConversation;
		wxStaticText* staticTextFilename;
		wxTextCtrl* textCtrlFilename;
		wxStaticText* staticTextConversationName;
		wxTextCtrl* textCtrlConversationName;
		wxStdDialogButtonSizer* sdbSizerSaveConversation;
		wxButton* sdbSizerSaveConversationOK;
		wxButton* sdbSizerSaveConversationCancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnSaveConversationCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnSaveConversationOKClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		DialogSaveConversationView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Save Conversation"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogSaveConversationView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogRenameConversationView
///////////////////////////////////////////////////////////////////////////////
class DialogRenameConversationView : public wxDialog
{
	private:

	protected:
		wxPanel* panelRenameConversation;
		wxStaticText* staticTextFilename;
		wxTextCtrl* textCtrlFilename;
		wxStaticText* staticTextConversationName;
		wxTextCtrl* textCtrlConversationName;
		wxStdDialogButtonSizer* sdbSizerRenameConversation;
		wxButton* sdbSizerRenameConversationOK;
		wxButton* sdbSizerRenameConversationCancel;

		// Virtual event handlers, override them in your derived class
		virtual void OnRenameConversationCancelClick( wxCommandEvent& event ) { event.Skip(); }
		virtual void OnRenameConversationOKClick( wxCommandEvent& event ) { event.Skip(); }


	public:

		DialogRenameConversationView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Rename Conversation"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogRenameConversationView();

};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogAboutView
///////////////////////////////////////////////////////////////////////////////
class DialogAboutView : public wxDialog
{
	private:

	protected:
		wxPanel* panelAbout;
		wxStaticBitmap* bitmapLogo;
		wxStaticText* staticTextTitle;
		wxStaticLine* staticlineAbout;
		wxNotebook* notebookAbout;
		wxTextCtrl* textCtrlAbout;
		wxStdDialogButtonSizer* sdbSizerAbout;
		wxButton* sdbSizerAboutOK;

		// Virtual event handlers, override them in your derived class
		virtual void OnNotebookAboutPageChanged( wxNotebookEvent& event ) { event.Skip(); }
		virtual void OnTextURLAbout( wxTextUrlEvent& event ) { event.Skip(); }


	public:

		DialogAboutView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("About..."), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_DIALOG_STYLE );

		~DialogAboutView();

};

