/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef MARKDOWN_H
#define MARKDOWN_H

#include <wx/arrstr.h>
#include <wx/string.h>
#include <wx/richtext/richtextbuffer.h>
#include <wx/richtext/richtextctrl.h>

namespace Markdown
{
    wxArrayString FilterEmptyElements(const wxArrayString& arrayString);
    wxString DetectAndMarkTables(const wxString& message);
    void SetBoldStyle(wxRichTextAttr& baseStyle);
    void SetItalicStyle(wxRichTextAttr& baseStyle);
    void SetUnderlineStyle(wxRichTextAttr& baseStyle);
    void SetCodeStyle(wxRichTextAttr& baseStyle);
    void SetStrikethroughStyle(wxRichTextAttr& baseStyle);
    void SetBlockquoteStyle(wxRichTextAttr& baseStyle);
    void SetHeadingStyle(wxRichTextAttr& baseStyle, int headingLevel);
    void SetLinkStyle(wxRichTextAttr& baseStyle, wxString linkUrl);
    wxRichTextAttr FormatTableMarkdownStyle(wxString& styledText, const wxTextAttr& baseStyle);
    void CreateAndInsertTable(wxRichTextCtrl* richTextChatBox, const wxString& tableData, const wxTextAttr& style);
    wxString ReplaceUnpaired(wchar_t character, wchar_t replaceCharacter, const wxString& message);
    wxString FormatMessage(const wxString& message);
    void HighlightSyntax(wxRichTextCtrl* richTextChatBox, const wxString& code, const wxString& language, const wxTextAttr& baseStyle);
    void WriteText(wxRichTextCtrl* richTextChatBox, const wxString& message, const wxTextAttr& style);
};

#endif // MARKDOWN_H
