/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef FRAMEMAIN_H
#define FRAMEMAIN_H

#include <mutex>
#include <cstdint>
#include <unordered_map>

#include "Views.h"
#include "ProcessThread.h"
#include "ServerSettings.h"
#include "Chat.h"

#include <wx/event.h>
#include <wx/string.h>
#include <wx/timer.h>
#include <wx/utils.h>
#include <wx/log.h>
#include <nlohmann/json.hpp>

///////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
/// Class FrameMain
///////////////////////////////////////////////////////////////////////////////
class FrameMain : public FrameMainView
{
    public:
        FrameMain(wxWindow* parent);
        ~FrameMain();
        void OnClose(wxCloseEvent& event) override;
        void OnStartClick(wxCommandEvent& event) override;
        void OnStopClick(wxCommandEvent& event) override;
        void OnStartThread(wxCommandEvent& event);
        void OnThreadEvent(wxThreadEvent& event);
        void OnSettingsClick(wxCommandEvent& event) override;
        void OnHelpClick(wxCommandEvent& event) override;
        void OnAboutMenuSelection(wxCommandEvent& event) override;
        void OnUpdateModelListClick(wxCommandEvent& event) override;
        void OnRunModelClick(wxCommandEvent& event) override;
        void OnShowInfoModelClick(wxCommandEvent& event) override;
        void OnDeleteModelClick(wxCommandEvent& event) override;
        void OnCloseConversationClick(wxCommandEvent& event) override;
        void OnSaveCloseConversationClick(wxCommandEvent& event) override;
        void OnSaveConversationClick(wxCommandEvent& event) override;
        void OnEditSessionClick(wxCommandEvent& event) override;
        void OnClearConversationClick(wxCommandEvent& event) override;
        void OnSendClick(wxCommandEvent& event) override;
        void OnAttachClick(wxCommandEvent& event) override;
        void OnAttachImage(wxCommandEvent& event) override;
        void OnChatInputKeyDown(wxKeyEvent& event) override;
        void OnLoadConversationClick(wxCommandEvent& event) override;
        void OnRenameConversationClick(wxCommandEvent& event) override;
        void OnDeleteConversationClick(wxCommandEvent& event) override;
        void OnSearchFilter(wxCommandEvent& event) override;
        void OnURLClick(wxTextUrlEvent& event);

    private:
        void SetServerStatus(bool isRunning);
        void LoadServerSettings();
        void StartOllamaServer();
        void StopOllamaServer();
        void EnableModelControls(bool isEnabled);
        void EnableChatControls(bool isEnabled);
        void EnableConversationControls(bool isEnabled);
        void EnableMenuItem(bool isEnabled, wxMenu* menu, const wxString& label);
        void InitializeModelParams();
        void StartChat();
        void CloseConversation();
        bool SaveConversation();
        void LoadConversations();
        void ClearChatBox();

        wxString exePath;
        ProcessThread* serverThread;
        std::mutex mutex;
        bool serverStatus;
        ServerSettings serverSettings;
        wxString loadedModel;
        wxString loadedSystemMessage;
        wxString loadedTemplate;
        wxString loadedConversationName;
        wxString loadedConversationFilename;
        Chat* chat;
        ModelParams* modelParams;
        std::unordered_map<wxString, wxString> conversations;
};

#endif // FRAMEMAIN_H
