/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef PROCESSTHREAD_H
#define PROCESSTHREAD_H

#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <chrono>

#include <wx/wx.h>
#include <wx/event.h>
#include <wx/string.h>
#include <wx/process.h>

class ProcessThread
{
    public:
        ProcessThread(wxEvtHandler* parent);
        ~ProcessThread();
        void Start(const wxString& command);
        void Stop();
        void SetPID(long pid);
        long GetPID() const;
        wxProcess* GetProcess();
        void SetProcess(wxProcess* process);
        bool GetPendingDelete();
        void SetPendingDelete(bool pendingDelete);

    private:
        void ProcessHandler();
        bool CheckProcessExists();
        void TerminateProcess();
        wxString GetStatus(wxKillError status);

        std::thread thread;
        std::mutex mutex;
        std::condition_variable cv;
        std::atomic<bool> stopThread;
        wxEvtHandler* parent;
        wxProcess* process;
        std::atomic<long> pid;
        bool pendingDelete;
};

#endif // PROCESSTHREAD_H
