/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGMODELINFORMATION_H
#define DIALOGMODELINFORMATION_H

#include "Views.h"

#include <wx/string.h>
#include <nlohmann/json.hpp>

class DialogModelInformation : public DialogModelInformationView
{
    public:
        DialogModelInformation(wxWindow* parent, const wxString& host, const wxString& modelName);
        ~DialogModelInformation() = default;
        void OnModelfileClick(wxCommandEvent& event) override;
        void OnLicenseClick(wxCommandEvent& event) override;
        void OnParametersClick(wxCommandEvent& event) override;
        void OnSystemMessageClick(wxCommandEvent& event) override;
        void OnPromptTemplateClick(wxCommandEvent& event) override;

    private:
        void BoldClickedButton(const wxString& label);

        nlohmann::json modelInfo;
};

#endif // DIALOGMODELINFORMATION_H
