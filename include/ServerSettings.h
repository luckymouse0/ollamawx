/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef SERVERSETTINGS_H
#define SERVERSETTINGS_H

#include <string>

#include <nlohmann/json.hpp>

class ServerSettings
{
    public:
        ServerSettings(std::string configJsonPath = "");
        ~ServerSettings() = default;
        bool operator==(const ServerSettings &other) const;
        bool operator!=(const ServerSettings &other) const;
        void Load(std::string newPath = "");
        void Save(std::string newPath = "");
        void SetDefaultValues();
        std::string GetConfigPath() const;
        std::string GetPath() const;
        void SetPath(std::string path);
        std::string GetHost() const;
        void SetHost(std::string host);
        int GetKeepAliveMinutes() const;
        std::string GetKeepAliveMinutesString() const;
        void SetKeepAliveMinutes(int keepAliveMinutes);
        int GetTimeoutSeconds() const;
        void SetTimeoutSeconds(int timeoutSeconds);
        bool GetStreaming() const;
        void SetStreaming(bool streaming);

    private:
        std::string configJsonPath;
        nlohmann::json configJson;
};

#endif // SERVERSETTINGS_H
