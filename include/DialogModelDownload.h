/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGMODELDOWNLOAD_H
#define DIALOGMODELDOWNLOAD_H

#include "Views.h"
#include "Network.h"

#include <thread>
#include <atomic>
#include <memory>

#include <wx/wx.h>
#include <wx/string.h>
#include <nlohmann/json.hpp>

class DialogModelDownload : public DialogModelDownloadView
{
    public:
        DialogModelDownload(wxWindow* parent, const wxString& host, const wxString& modelName);
        ~DialogModelDownload();
        void OnTimerModelDownload(wxTimerEvent& event) override;

    private:
        void DownloadModel();

        wxString host;
        wxString modelName;
        int timeoutSeconds;
        std::atomic_bool downloadInProgress;
        std::unique_ptr<std::thread> threadDownload;
};

#endif // DIALOGMODELDOWNLOAD_H
