/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGRENAMECONVERSATION_H
#define DIALOGRENAMECONVERSATION_H

#include "Views.h"

#include <wx/string.h>
#include <nlohmann/json.hpp>

class DialogRenameConversation : public DialogRenameConversationView
{
    public:
        DialogRenameConversation(wxWindow* parent, wxString* filePath, wxString* conversationName);
        ~DialogRenameConversation() = default;
        void OnRenameConversationOKClick(wxCommandEvent& event) override;
        void OnRenameConversationCancelClick(wxCommandEvent& event) override;

    private:
        bool ValidateInput(const wxString& newFilename, const wxString& newConversationName) const;
        bool LoadConversationJson(nlohmann::json& conversationJson) const;
        bool SaveConversationJson(const nlohmann::json& conversationJson) const;
        bool RenameFile(const wxString& newFilename);

        wxString* filePath;
        wxString* conversationName;
        wxString filename;
};

#endif // DIALOGRENAMECONVERSATION_H
