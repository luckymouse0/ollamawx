/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGABOUT_H
#define DIALOGABOUT_H

#include "Views.h"

#include <wx/event.h>

class DialogAbout : public DialogAboutView
{
    public:
        DialogAbout(wxWindow* parent);
        ~DialogAbout() = default;
        void OnNotebookAboutPageChanged(wxNotebookEvent& event) override;
        void OnTextURLAbout(wxTextUrlEvent& event);

    private:
        void SetDescription();
        void SetCopyright();
        void SetLicense();
};

#endif // DIALOGABOUT_H
