/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <wx/icon.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>

namespace ImageUtils
{
    wxIcon LoadIcon(const wxString& imagePath);
    wxBitmap LoadBitmap(const wxString& imagePath);
    wxBitmap LoadBitmap(const wxString& imagePath, int width, int height);
    wxImage LoadImage(const wxString& imagePath);
    wxImage ResizeImage(wxImage image, int width, int height);
    wxBitmap ResizeBitmap(const wxImage& image, int width, int height);
};

#endif // IMAGEUTILS_H
