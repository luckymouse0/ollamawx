/**
 * This file is part of Ollamawx.
 *
 * Copyright (C) 2024 luckymouse0, all rights reserved.
 *
 * Ollamawx is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Ollamawx is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Ollamawx
 * @author     luckymouse0
 * @date       2024
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef DIALOGEDITSESSION_H
#define DIALOGEDITSESSION_H

#include <string>

#include "Views.h"
#include "ModelParams.h"

class DialogEditSession : public DialogEditSessionView
{
    public:
        DialogEditSession(wxWindow* parent, ModelParams* modelParams, std::string originalSystemMessage, std::string originalTemplate);
        ~DialogEditSession() = default;
        void OnEditSessionOKClick(wxCommandEvent& event) override;
        void OnEditSessionApplyClick(wxCommandEvent& event) override;
        void OnEditSessionCancelClick(wxCommandEvent& event) override;
        void OnEditSessionDefaultClick(wxCommandEvent& event) override;
        void OnTextChanged(wxCommandEvent& event) override;
        void OnSpinCtrlChanged(wxSpinEvent& event) override;
        void OnSpinCtrlDoubleChanged(wxSpinDoubleEvent& event) override;
        void OnChoiceChanged(wxCommandEvent& event) override;
        void OnCheckBoxChanged(wxCommandEvent& event) override;
		void OnCheckBoxEnableSystemMessage(wxCommandEvent& event) override;
		void OnCheckBoxEnableTemplate(wxCommandEvent& event) override;
		void OnCheckBoxEnableSeed(wxCommandEvent& event) override;
		void OnCheckBoxEnableTemperature(wxCommandEvent& event) override;
		void OnCheckBoxEnableContextSize(wxCommandEvent& event) override;
		void OnCheckBoxEnablePredictTokens(wxCommandEvent& event) override;
		void OnCheckBoxEnableRepeatLast(wxCommandEvent& event) override;
		void OnCheckBoxEnableRepeatPenalty(wxCommandEvent& event) override;
		void OnCheckBoxEnableTopK(wxCommandEvent& event) override;
		void OnCheckBoxEnableTopP(wxCommandEvent& event) override;
		void OnCheckBoxEnableTFSZ(wxCommandEvent& event) override;
		void OnCheckBoxEnableMirostat(wxCommandEvent& event) override;
		void OnCheckBoxEnableMirostatTau(wxCommandEvent& event) override;
		void OnCheckBoxEnableMirostatEta(wxCommandEvent& event) override;
		void OnCheckBoxEnableKeepTokens(wxCommandEvent& event) override;
		void OnCheckBoxEnableTypicalP(wxCommandEvent& event) override;
		void OnCheckBoxEnablePenalizeNewlines(wxCommandEvent& event) override;
		void OnCheckBoxEnablePresencePenalty(wxCommandEvent& event) override;
		void OnCheckBoxEnableFrequencyPenalty(wxCommandEvent& event) override;

    private:
        void SaveParams();
        void EnableControls();

        ModelParams* modelParams;
        std::string originalSystemMessage;
        std::string originalTemplate;
        bool hasChanged = false;
};

#endif // DIALOGEDITSESSION_H
