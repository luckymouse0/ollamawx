# Model library

Ollamawx supports a list of models available on [ollama.com/library](https://ollama.com/library 'ollama model library')

Here are some example models that can be downloaded:

| Model              | Parameters | Size  | Download                       |
| ------------------ | ---------- | ----- | ------------------------------ |
| Falcon             | 180B       | 101GB | `falcon:180b`                  |
| Mixtral 8x22B      | 141B       | 80GB  | `mixtral:8x22b`                |
| Zephyr             | 141B       | 80GB  | `zephyr:141b`                  |
| Qwen 1.5           | 110B       | 63GB  | `qwen:110b`                    |
| Command R+         | 104B       | 59GB  | `command-r-plus`               |
| Qwen 1.5           | 72B        | 41GB  | `qwen:72b`                     |
| Llama 3            | 70B        | 40GB  | `llama3:70b`                   |
| Orca Mini          | 70B        | 39GB  | `orca-mini:70b`                |
| Code Llama         | 69B        | 39GB  | `codellama:70b`                |
| Mixtral 8x7B       | 47B        | 26GB  | `mixtral:8x7b`                 |
| Dolphin Mixtral    | 47B        | 26GB  | `dolphin-mixtral:8x7b`         |
| Falcon             | 40B        | 24GB  | `falcon:40b`                   |
| Command R          | 35B        | 20GB  | `command-r`                    |
| LLaVA              | 34B        | 20GB  | `llava:34b`                    |
| Code Llama         | 34B        | 19GB  | `codellama:34b`                |
| Wizardcoder        | 34B        | 19GB  | `wizardcoder:34b-python`       |
| Wizardcoder        | 33B        | 19GB  | `wizardcoder:33b`              |
| Qwen 1.5           | 33B        | 18GB  | `qwen:33b`                     |
| StarCoder2         | 15B        | 9.1GB | `starcoder2:15b`               |
| Qwen 1.5           | 14B        | 8.2GB | `qwen:14b`                     |
| LLaVA              | 13B        | 8.0GB | `llava:13b`                    |
| Code Llama         | 13B        | 7.4GB | `codellama:13b`                |
| Orca Mini          | 13B        | 7.4GB | `orca-mini:13b`                |
| Wizardcoder        | 13B        | 7.4GB | `wizardcoder:13b-python`       |
| Solar              | 10.7B      | 6.1GB | `solar`                        |
| CodeGemma          | 9B         | 5.0GB | `codegemma:7b`                 |
| Llama 3            | 8B         | 4.7GB | `llama3:8b`                    |
| Gemma              | 7B         | 4.8GB | `gemma:7b`                     |
| LLaVA              | 7B         | 4.5GB | `llava:7b`                     |
| Qwen 1.5           | 7B         | 4.5GB | `qwen:7b`                      |
| Falcon             | 7B         | 4.2GB | `falcon:7b`                    |
| Mistral            | 7B         | 4.1GB | `mistral`                      |
| Mistral OpenOrca   | 7B         | 4.1GB | `mistral-openorca`             |
| Neural Chat        | 7B         | 4.1GB | `neural-chat`                  |
| Starling           | 7B         | 4.1GB | `starling-lm`                  |
| Zephyr             | 7B         | 4.1GB | `zephyr:7b`                    |
| OpenHermes 2.5     | 7B         | 4.1GB | `openhermes`                   |
| Openchat 3.5       | 7B         | 4.1GB | `openchat`                     |
| StarCoder2         | 7B         | 4.0GB | `starcoder2:7b`                |
| Code Llama         | 7B         | 3.8GB | `codellama:7b`                 |
| DeepSeek Coder     | 7B         | 3.8MB | `deepseek-coder:6.7b`          |
| Llama 2 Uncensored | 7B         | 3.8GB | `llama2-uncensored`            |
| Orca Mini          | 7B         | 3.8GB | `orca-mini:7b`                 |
| Wizardcoder        | 7B         | 3.8GB | `wizardcoder:7b-python`        |
| Qwen 1.5           | 4B         | 2.3GB | `qwen:4b`                      |
| Phi-3              | 3.8B       | 2.3GB | `phi3`                         |
| Orca Mini          | 3B         | 2.0GB | `orca-mini:3b`                 |
| StarCoder2         | 3B         | 1.7GB | `starcoder2:3b`                |
| CodeGemma          | 3B         | 1.6GB | `codegemma:2b`                 |
| Stable Code 3B     | 3B         | 1.6GB | `stable-code:instruct`         |
| Phi-2              | 2.7B       | 1.6GB | `phi`                          |
| Gemma              | 2B         | 1.4GB | `gemma:2b`                     |
| Qwen 1.5           | 1.8B       | 1.1GB | `qwen:1.8b`                    |
| DeepSeek Coder     | 1.3B       | 776MB | `deepseek-coder:1.3b`          |
| TinyLlama          | 1.1B       | 638MB | `tinyllama`                    |
| TinyDolphin        | 1.1B       | 637MB | `tinydolphin`                  |
| StarCoder          | 1B         | 726GB | `starcoder:1b`                 |
| Qwen 1.5           | 0.5B       | 395MB | `qwen:0.5b`                    |

> Note: You should have at least 8 GB of RAM available to run the 7B models, 16 GB to run the 13B models, 32 GB to run the 33B models, 50-64 GB to run 70B, 64-128 GB to run 140B, and 192 GB to run 180B.