[![Ollamawx logo](docs/images/logo.png)](https://gitlab.com/luckymouse0/ollamawx)

# Ollamawx

This project is an application interface built with wxWidgets for chatting with your local LLMs via [ollama.ai](https://ollama.ai/) using the [ollama API](https://github.com/ollama/ollama/blob/main/docs/api.md).

## Features

- Cross-platform and lightweight GUI built with wxWidgets.
- Manage LLM models, adding new or deleting existing ones.
- Save, load or delete conversations.
- Support for Markdown, including syntax highlighting for code.
- Attach images to conversations for supported multimodal models.

## Compiling from source

### Paths

- Linux: It uses the default compiled-from-source wxWidgets path.
- Windows: Add a global variable in Code::Blocks named "wx" with the following:
    - base: wxWidgets installation path.
    - include: wxWidgets "include" sub-folder path.
    - lib: wxWidgets "lib" sub-folder path.

### Build Options

- Linux: Use Debug or Release build options.
- Windows: Use DebugWin or ReleaseWin build options.

## Libraries used

- cpp-httplib: [Source](https://github.com/yhirose/cpp-httplib)
- nlohmann/json: [Website](https://json.nlohmann.me/) | [Source](https://github.com/nlohmann/json)

## Tools used

- Code::Blocks 20.03: [Website](https://www.codeblocks.org/) | [Source](https://svn.code.sf.net/p/codeblocks/code/)
- wxWidgets 3.1.4: [Website](https://www.wxwidgets.org/) | [Source](https://github.com/wxWidgets/wxWidgets)
- wxFormBuilder 3.10.1: [Source](https://github.com/wxFormBuilder/wxFormBuilder)
- Ollama 0.1.32+: [Website](https://ollama.com/) | [Source](https://github.com/ollama/ollama)

## License

Ollamawx is distributed under the GPLv2.0 license, refer to the LICENSE file available in the project repository.
